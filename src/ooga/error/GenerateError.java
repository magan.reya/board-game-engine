package ooga.error;

import ooga.error.errorsAPI.GenerateErrorAPI;

import java.util.ResourceBundle;

@Deprecated
public class GenerateError implements GenerateErrorAPI {

    private String LANG_KEY = "language";//TODO: figure out this class

    public void GenerateError(ResourceBundle langResources, String message) {
        String language = langResources.getString(LANG_KEY);
        Error myError = new Error(language);
        myError.prepareError(message);
        myError.showError();
    }
}
