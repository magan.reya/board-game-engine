package ooga.error.errorsAPI;

@Deprecated
public interface ErrorAPI {

    void prepareError(String message);

    void showError();

    String getMyMessage();

}
