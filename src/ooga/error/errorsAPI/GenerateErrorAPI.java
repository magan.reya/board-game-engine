package ooga.error.errorsAPI;

import java.util.ResourceBundle;

@Deprecated
public interface GenerateErrorAPI {

    void GenerateError(ResourceBundle langResources, String message);
}
