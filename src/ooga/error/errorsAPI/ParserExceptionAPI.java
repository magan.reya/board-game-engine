package ooga.error.errorsAPI;

public interface ParserExceptionAPI {

    String getKey();

    void setKey(String key);

    String getValue();

    void setValue(String value);
}
