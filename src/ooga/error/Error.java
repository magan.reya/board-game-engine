package ooga.error;

import java.util.ResourceBundle;

import javafx.scene.control.Alert;
import ooga.error.errorsAPI.ErrorAPI;

@Deprecated
public class Error implements ErrorAPI {

    private final String KEY_UNAVAILABLE = "InvalidMessagePassed";
    //TODO: figure out this class
    private final String ERROR_RESOURCE_PATH = "cellsociety.error.resources.languages.";
    private ResourceBundle myResourceBundle;
    private String myMessage;

    public Error(String language) {
        myResourceBundle = ResourceBundle.getBundle(ERROR_RESOURCE_PATH + language);
        setMessageToDefault();
    }

    public void prepareError(String message) {
        myMessage =  (myResourceBundle.containsKey(message)) ? message : KEY_UNAVAILABLE;
    }

    //TODO: make it just front-end by using propertyObservable
    public void showError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(myMessage);
        alert.setContentText(getMyMessage());
        alert.showAndWait();
    }

    public String getMyMessage() {
        return myResourceBundle.getString(myMessage);
    }

    private void setMessageToDefault() {
        myMessage = null;
    }

}
