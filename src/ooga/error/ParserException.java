package ooga.error;

import ooga.error.errorsAPI.ParserExceptionAPI;
import static ooga.Main.LOGGER;

public class ParserException extends Exception implements ParserExceptionAPI {

  private String key;
  private String value;

  public ParserException(String key, String value, String message) {
    super(message);
    this.setKey(key);
    this.setValue(value);
    LOGGER.error(message);
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
