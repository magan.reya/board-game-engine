package ooga.components;

import ooga.components.componentAPI.CellSetup;
import ooga.resources.Config;

import java.awt.Point;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Cell class to keep track of elements within Grid (ie "pieces") and how the players interface with
 * the Grid over the course of a given game instance.
 */
public class Cell implements CellSetup {

  private Point myLocation;
  private int myState; // has to be the same with playerID
  private NeighborCells myNeighbors;

  /**
   * Constructor for Cell
   *
   * @param x     - the column of the Cell within the Grid
   * @param y     - the row of the Cell within the Grid
   * @param state - the initial state of the Cell (usually PlayerID or 0 if it's uninitialized)
   */
  public Cell(int x, int y, int state) {
    myState = state;
    myLocation = new Point(x, y);
    myNeighbors = new NeighborCells();
  }

  /**
   * @return TRUE if the Cell is empty, FALSE otherwise
   */
  public boolean isEmpty() {
    return myState == Config.EMPTY_STATE;
  }

  /**
   * @return Point location of the Cell
   */
  public Point getLocation() {
    return myLocation;
  }

  public void setState(int newState) {
    myState = newState;
  }

  public int getState() {
    return myState;
  }

  /**
   * Get the Cell's complete Cell list of its neighbors
   *
   * @return list of neighbors as Cells
   */
  public List<Cell> getCompleteNeighbors() {
    return myNeighbors.getCompleteNeighbors();
  }

  /**
   * Get solely diagonally neighboring Cells
   *
   * @return list of Cells
   */
  public List<Cell> getDiagonalNeighbors() {
    return myNeighbors.getDiagonalNeighbors();
  }

  /**
   * Get solely lower diagonal Cell neighbors
   *
   * @return list of Cells
   */
  public List<Cell> getLowerDiagonalNeighbors() {
    return myNeighbors.getLowerDiagonalNeighbors();
  }

  /**
   * Get solely upper diagonal Cell neighbors
   *
   * @return list of Cells
   */
  public List<Cell> getUpperDiagonalNeighbors() {
    return myNeighbors.getUpperDiagonalNeighbors();
  }

  /**
   * Clear the caller Cell's neighbors
   */
  public void clearNeighbors() {
    myNeighbors.clear();
  }

  /**
   * Gets the neighboring Point indexes in all directions
   *
   * @return list of Points that neighbor the caller Cell
   */
  public List<Point> getAllPossibleNeighborPoints() {
    int[] rows = {-1, -1, -1, 0, 1, 1, 1, 0};
    int[] cols = {-1, 0, 1, 1, 1, 0, -1, -1};
    List<Point> allPossibleNeighbors = new ArrayList<>();
    IntStream.range(0, rows.length)
        .forEach(i ->
            allPossibleNeighbors.add(new Point(myLocation.x + cols[i], myLocation.y + rows[i])));
    return allPossibleNeighbors;
  }

  /**
   * Adds passed Cell to the caller's neighbors
   *
   * @param cell - a neighboring Cell if the caller Cell
   */
  public void addToNeighbors(Cell cell) {
    myNeighbors.addToCompleteNeighbor(cell);
    if (isOnUpperDiagonal(cell)) {
      myNeighbors.addToUpperDiagonalNeighbor(cell);
    } else if (isOnLowerDiagonal(cell)) {
      myNeighbors.addToLowerDiagonalNeighbor(cell);
    }
  }

  private boolean isOnUpperDiagonal(Cell cell) {
    return cell.getLocation().y < myLocation.y && cell.getLocation().x != myLocation.x;
  }

  private boolean isOnLowerDiagonal(Cell cell) {
    return cell.getLocation().y > myLocation.y && cell.getLocation().x != myLocation.x;
  }

}
