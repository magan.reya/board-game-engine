package ooga.components;

import ooga.components.componentAPI.NeighborSetup;

import java.util.ArrayList;
import java.util.List;

/**
 * NeighborCells class to help process how Cells are related to one another through their location
 * on the Grid.
 */
public class NeighborCells implements NeighborSetup {

  private List<Cell> myCompleteNeighbors;
  private List<Cell> myDiagonalNeighbors;
  private List<Cell> myUpperDiagonalNeighbors;
  private List<Cell> myLowerDiagonalNeighbors;

  /**
   * Constructor for NeighborCells object
   */
  public NeighborCells() {
    myCompleteNeighbors = new ArrayList<>();
    myDiagonalNeighbors = new ArrayList<>();
    myUpperDiagonalNeighbors = new ArrayList<>();
    myLowerDiagonalNeighbors = new ArrayList<>();
  }

  /**
   * Clear all neighbor lists
   */
  public void clear() {
    myCompleteNeighbors.clear();
    myDiagonalNeighbors.clear();
    myUpperDiagonalNeighbors.clear();
    myLowerDiagonalNeighbors.clear();
  }

  /**
   * Add Cell to the Complete Neighbor list
   *
   * @param cell to be added
   */
  public void addToCompleteNeighbor(Cell cell) {
    myCompleteNeighbors.add(cell);
  }

  /**
   * Add Cell to the Upper Diagonal Neighbor list
   *
   * @param cell to be added
   */
  public void addToUpperDiagonalNeighbor(Cell cell) {
    myDiagonalNeighbors.add(cell);
    myUpperDiagonalNeighbors.add(cell);
  }

  /**
   * Add Cell to the Lower Diagonal Neighbor list
   *
   * @param cell to be added
   */
  public void addToLowerDiagonalNeighbor(Cell cell) {
    myDiagonalNeighbors.add(cell);
    myLowerDiagonalNeighbors.add(cell);
  }

  /**
   * @return the complete neighbor list of Cells
   */
  public List<Cell> getCompleteNeighbors() {
    return myCompleteNeighbors;
  }

  /**
   * @return the both Diagonal Neighbor lists
   */
  public List<Cell> getDiagonalNeighbors() {
    return myDiagonalNeighbors;
  }

  /**
   * @return the dower diagonal List of Cells
   */
  public List<Cell> getLowerDiagonalNeighbors() {
    return myLowerDiagonalNeighbors;
  }

  /**
   * @return the upper diagonal list of Cells
   */
  public List<Cell> getUpperDiagonalNeighbors() {
    return myUpperDiagonalNeighbors;
  }
}
