package ooga.components.componentAPI;

import ooga.components.Cell;

import java.util.List;

/**
 * Interface to facilitate control and analysis of a Cell's neighbors
 */
public interface NeighborSetup {

  /**
   * Clear the Cell's neighbor lists
   */
  void clear();

  /**
   * Add a given Cell to the caller's complete neighbor list of Cells
   *
   * @param cell to be added
   */
  void addToCompleteNeighbor(Cell cell);

  /**
   * Add a given Cell to the caller's upper diagonal neighbor list of Cells
   *
   * @param cell to be added
   */
  void addToUpperDiagonalNeighbor(Cell cell);

  /**
   * Add a given Cell to the caller's lower diagonal neighbor list of Cells
   *
   * @param cell to be added
   */
  void addToLowerDiagonalNeighbor(Cell cell);

  /**
   * @return the Cell's complete list of neighbors
   */
  List<Cell> getCompleteNeighbors();

  /**
   * @return the Cell's complete list of diagonal neighbors
   */
  List<Cell> getDiagonalNeighbors();

  /**
   * @return the Cell's list of lower diagonal neighbors
   */
  List<Cell> getLowerDiagonalNeighbors();

  /**
   * @return the Cell's list of upper diagonal neighbors
   */
  List<Cell> getUpperDiagonalNeighbors();
}
