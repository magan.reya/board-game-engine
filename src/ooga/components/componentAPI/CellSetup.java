package ooga.components.componentAPI;

import ooga.components.Cell;

import java.awt.Point;
import java.util.List;

/**
 * Public interface with core methods of the Cell object
 */
public interface CellSetup {

  /**
   * @return TRUE if Cell is empty, FALSE otherwise
   */
  boolean isEmpty();

  /**
   * @return the Point associated with Cell
   */
  Point getLocation();

  /**
   * Set state of the Cell
   *
   * @param newState - integer value of new state
   */
  void setState(int newState);

  /**
   * @return the state value of the Cell (usually just the PlayerID)
   */
  int getState();

  /**
   * @return Complete list of Cell's neighbors (accounts for Cell's current location)
   */
  List<Cell> getCompleteNeighbors();

  /**
   * @return List of Cell's diagonal neighbors (max=4)
   */
  List<Cell> getDiagonalNeighbors();

  /**
   * @return List of Cell's solely lower diagonal neighbors (max=2)
   */
  List<Cell> getLowerDiagonalNeighbors();

  /**
   * @return List of Cell's solely upper diagonal neighbors (max=2)
   */
  List<Cell> getUpperDiagonalNeighbors();

  /**
   * @return List of Cell's neighboring points (ignores Cell's location)
   */
  List<Point> getAllPossibleNeighborPoints();

  /**
   * Clear the Cell's neighbor lists
   */
  void clearNeighbors();

  /**
   * @param cell to be added to caller Cell's neighbor list
   */
  void addToNeighbors(Cell cell);

}
