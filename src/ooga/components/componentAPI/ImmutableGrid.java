package ooga.components.componentAPI;

import ooga.components.Cell;

import java.awt.Point;
import java.util.Set;

/**
 * Interface used obtain characteristics of the Grid without having access to change the Grid.
 */
public interface ImmutableGrid {

  /**
   * @return the number of rows of the Grid
   */
  int getNumRows();

  /**
   * @return the number of columns of the Grid
   */
  int getNumCols();

  /**
   * Determine the state of a Grid's Cell at a given location
   *
   * @param col - the Cell's column (y)
   * @param row - the Cell's row (x)
   * @return the value of the Cell's state (usually PlayerID or 0 if uninitialized)
   */
  int getState(int col, int row);

  /**
   * Determines if the Grid currently holds a given point
   *
   * @param point to be checked for in the Grid
   * @return TRUE if the Grid contains the Point, FALSE otherwise
   */
  boolean doesGridHavePiece(Point point);

  /**
   * Get the Cell at a given Point within the Grid
   *
   * @param point - the location of the Cell within the Grid
   * @return the Cell at the given Point within the Grid
   */
  Cell getCell(Point point);

  /**
   * Get all "pieces" within the Grid
   *
   * @return - Set of "pieces"
   */
  Set<Point> getAllPieces();

  /**
   * Get all Points within the Grid
   *
   * @return - Set of Points
   */
  Set<Point> getAllPoints();
}
