package ooga.components;

import ooga.components.componentAPI.ImmutableGrid;
import ooga.resources.Config;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.awt.Point;

/**
 * Grid class to abstract the "board" component of the games in this project. Each Grid contains a
 * "board" object that maps Points to Cells thereby creating an interface between the model and the
 * view.
 */
public class Grid implements ImmutableGrid {

  private Map<Point, Cell> myBoard;
  private final int myNumRows;
  private final int myNumCols;

  /**
   * Constructor for Grid
   *
   * @param states - the initial states of the Grid
   */
  public Grid(int[][] states) {
    myNumRows = states.length;
    myNumCols = states[0].length;
    myBoard = new HashMap<>();
    initializeGrid(states);
    initializeNeighbors();
  }

  private void initializeGrid(int[][] states) {
    IntStream.range(0, myNumRows).forEach(row -> IntStream.range(0, myNumCols)
        .forEach(col -> myBoard.put(new Point(col, row), new Cell(col, row, states[row][col]))));
  }


  /**
   * Get Cell in Grid given its Point
   *
   * @param point - location associated with the Cell
   * @return the Cell at a given Point
   */
  public Cell getCell(Point point) {
    if (isInsideBoard(point)) {
      return myBoard.get(point);
    }
    return null;
  }

  /**
   * Get all "pieces" on the Grid, ie Cells that belong to a player
   *
   * @return Set of "pieces"
   */
  public Set<Point> getAllPieces() {
    Set<Point> pieces = new HashSet<>();
    for (Point point : myBoard.keySet()) {
      if (!getCell(point).isEmpty()) {
        pieces.add(point);
      }
    }
    return pieces;
  }

  /**
   * @return Set of all Points in the Grid
   */
  public Set<Point> getAllPoints() {
    return myBoard.keySet();
  }

  protected void initializeNeighbors() {
    for (Point point : myBoard.keySet()) {
      Cell cell = myBoard.get(point);
      cell.clearNeighbors();
      List<Point> allPossibleNeighbors = cell.getAllPossibleNeighborPoints();
      for (Point neighbor : allPossibleNeighbors) {
        if (isInsideBoard(neighbor)) {
          cell.addToNeighbors(getCell(neighbor));
        }
      }
    }
  }

  /**
   * Determines if a Point is within the bounds of the Grid
   *
   * @param point to be checked
   * @return TRUE if the Cell is within the Grid, FALSE otherwise
   */
  public boolean isInsideBoard(Point point) {
    return myBoard.containsKey(point);
  }

  /**
   * @return the number of rows in the Grid
   */
  public int getNumRows() {
    return myNumRows;
  }

  /**
   * @return the number of cols in the Grid
   */
  public int getNumCols() {
    return myNumCols;
  }

  public int getState(int col, int row) {
    return myBoard.get(new Point(col, row)).getState();
  }

  /**
   * Determines if the Grid holds a given Point
   *
   * @param point to be checked
   * @return TRUE if the Grid has the Point, FALSE otherwise
   */
  public boolean doesGridHavePiece(Point point) {
    return isInsideBoard(point) && !myBoard.get(point).isEmpty();
  }

  /**
   * Add a "piece" to the Grid
   *
   * @param point - the location of the "piece"
   * @param ID    - the PlayerID who played the "piece"
   */
  public void addPiece(Point point, int ID) {
    myBoard.get(point).setState(ID);
  }

  /**
   * Removes a piece from the Grid
   *
   * @param point location of the Cell to be removed
   */
  public void removePiece(Point point) {
    myBoard.get(point).setState(Config.EMPTY_STATE);
  }

  /**
   * Get all PlayerID's Points
   *
   * @param ID - the Player's ID whose Points should be returned
   * @return the player's played Points in the Grid
   */
  public List<Point> getAllPointsBasedOnID(int ID) {
    return myBoard.keySet()
        .stream()
        .filter(p -> myBoard.get(p).getState() == ID)
        .collect(Collectors.toList());
  }
}
