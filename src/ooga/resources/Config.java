package ooga.resources;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javafx.scene.paint.Color;

public class Config {

  // English language (use for instantiating the type of properties file we want to load)
  public static final String ENGLISH_LANGUAGE = "English";

  public static final String MAIN_TITLE = "OOGA Setup";
  public static final int DEFAULT_WIDTH = 600;
  public static final int DEFAULT_HEIGHT = 600;
  public static final String ALERT_MESSAGE = "Error";
  public static final int VBOX_SPACING = 100;
  public static final int HBOX_SPACING = 50;
  public static final int NUM_PLAYERS = 2;
  public static final String EMPTY_STRING = "";

  /**
   * Path variables
   */
  public static final String DEFAULT_PATH_MODEL = "ooga.model.%s";
  public static final String DEFAULT_PATH_GAMEVIEW = "ooga.view.game.%s";
  public static final String DEFAULT_PATH_GRIDVIEW = "ooga.view.grid.%s";
  public static final String DEFAULT_RESOURCE_PACKAGE_PROPERTIES = "ooga.resources.%s";
  public static final String DEFAULT_RESORUCE_PACKAGE_STYLESHEETS = "/ooga.resources.%s";
  public static final String DEFAULT_PATH_PLAYER = "ooga.players.%s";
  public static final String DEFAULT_PATH_STYLESHEET = DEFAULT_RESORUCE_PACKAGE_STYLESHEETS.replace(
      ".", "/");
  public static final String DEFAULT_STYLESHEET = "Stylesheet.css";

  /**
   * Reflection and Constructor Path/Packages
   */
  public static final String DEFAULT_MODEL_REFLECTION_PATH = String.format(DEFAULT_PATH_MODEL,
      "%sModel");
  public static final String DEFAULT_PLAYER_REFLECTION_PATH = String.format(DEFAULT_PATH_PLAYER,
      "%sPlayer");
  public static final String DEFAULT_GRIDVIEW_REFLECTION_PATH = String.format(DEFAULT_PATH_GRIDVIEW,
      "%sGridView");
  public static final String DEFAULT_GAMEVIEW_CONSTRUCTOR_PATH = String.format(
      DEFAULT_PATH_GAMEVIEW, "%sGameView");

  /**
   * Parser variables
   */
  public static final String DEFAULT_BOARD_FILEPATH = "data/%s/default_board.sim";
  public static final String INVALID_FILE = "InvalidFile";
  public static final String SPACE = " ";
  public static final String ZERO_STRING = "0";
  public static final String EMPTY_NULL_STRING = "(null-empty-string)";

  /**
   * SIMParser variables
   */
  // General SIM
  public static final String SIMFILE_TYPE = ".sim";
  // SIM types
  public static final String SIMPARSER_GAME_TYPE_KEY = "GameType";
  public static final String SIMPARSER_AUTHOR_KEY = "Author";
  public static final String SIMPARSER_PLAYER_NAMES = "PlayerNames";
  public static final String SIMPARSER_PLAYER_TYPES = "PlayerTypes";
  public static final String SIMPARSER_PLAYER_TURN = "PlayerTurn";
  public static final String SIMPARSER_INITIAL_STATE = "InitialState";
  public static final Set<String> SIMPARSER_REQUIRED_KEYS =
      Set.of(SIMPARSER_GAME_TYPE_KEY, SIMPARSER_AUTHOR_KEY, SIMPARSER_PLAYER_NAMES,
          SIMPARSER_PLAYER_TYPES, SIMPARSER_PLAYER_TURN,
          SIMPARSER_INITIAL_STATE);
  // Parser Error Exceptions keys to properties file
  public static final String SIMPARSER_INVALID_KEY = "KeyDoesNotExist";
  public static final String SIMPARSER_INVALID_VALUE = "ValueDoesNotExist";
  public static final String SIMPARSER_KEY_EMPTY_VALUE = "KeyEmptyValue";
  public static final String SIMPARSER_NUM_PLAYERS_INVALID = "NumPlayersInvalid";
  public static final String SIMPARSER_KEYS_MISSING = "KeysMissing";
  public static final String SIMPARSER_MISSING_FILE = "MissingFile";
  // SIMParser regex
  public static final String SIMPARSER_LIST_REGEX = ",";
  public static final String SIMPARSER_KEY_VALUE_REGEX = "=";
  public static final String NEWLINE = "\n";
  public static final String HUMAN_TYPE = "Human";
  public static final String CPU_TYPE = "CPU";
  public static final List<String> SIMPARSER_PLAYER_TYPES_LIST = List.of(HUMAN_TYPE, CPU_TYPE);

  /**
   * CSVParser variables
   */
  // General CSV
  public static final String CSVFILE_TYPE = ".csv";
  // CSV types
  public static final String CSVPARSER_NUM_ROW_COL = "NumRowCol";
  public static final String CSVPARSER_GRID_STATES = "GridStates";
  public static final int CSVPARSER_NUMBER_ROW_COLS = 2;
  // Parser Error Exception keys to properties file
  public static final String CSVPARSER_INVALID_NUM_ELEMENTS = "InvalidNumElements";
  public static final String CSVPARSER_NON_NUMERIC = "NonNumeric";
  public static final String CSVPARSER_INVALID_KEY = "KeyDoesNotExist";
  public static final String CSVPARSER_EMPTY_LINE = "EmptyLine";
  public static final String CSVPARSER_ZERO_ELEMENTS = "ZeroElements";
  public static final String CSVPARSER_NON_VALID_STATE_ELEMENT = "NonValidStateElement";
  public static final Set<String> CSVPARSER_VALID_GRID_STATES = Set.of("0", "1", "2");

  /**
   * GameView variables
   */
  // GameView variables
  public static final int GAMEVIEW_DEFAULT_WIDTH = 800;
  public static final int GAMEVIEW_DEFAULT_HEIGHT = 600;
  public static final double GAMEVIEW_ANIMATION_SPEED_DEFAULT = 0.5;
  public static final int CHECK_IMAGE_DIM = 50;
  public static final String GAMEVIEW_ID = "GameView";
  public static final String GAMEVIEW_TOP_PANEL_ID = "GameViewTopPanel";
  public static final String GAMEVIEW_BOTTOM_PANEL_ID = "GameViewBottomPanel";
  public static final String GAMEVIEW_ANIMATION_PANEL_ID = "GameViewAnimationPanel";
  public static final String GAMEVIEW_SCOREBOARD_PANEL_ID = "GameViewScoreboard";
  public static final String GAMEVIEW_DEFAULT_AUTHOR = "DefaultAuthor";
  public static final String GAMEVIEW_SAVE_FILE_BUTTON_LABEL = "SaveFile";
  public static final String GAMEVIEW_BACK_BUTTON_LABEL = "BacktoHome";
  public static final String GAMEVIEW_START_ANIMATION_BUTTON_LABEL = "Run";
  public static final String GAMEVIEW_PAUSE_ANIMATION_BUTTON_LABEL = "Pause";
  public static final String GAMEVIEW_RESUME_ANIMATION_BUTTON_LABEL = "Resume";
  public static final String GAMEVIEW_STEP_ANIMATION_BUTTON_LABEL = "Step";
  public static final String GAMEVIEW_SAVE_CLICKED = "gameViewSaveClicked";
  public static final String GAMEVIEW_BACK_BUTTON_CLICKED = "gameViewBackClicked";
  public static final String ANIMATION_RUNNING_MESSAGE = "AnimationRunningMessage";
  public static final String ANIMATION_NOT_RUNNING_MESSAGE = "AnimationNotRunningMessage";
  public static final String ANIMATION_NOT_STARTED = "AnimationNotStarted";
  public static final String WIN_MESSAGE = "WinMessage";
  public static final String CHECK_IMAGE = "image/checked.png";


  /**
   * GridView variables
   */
  public static final int GRIDVIEW_WIDTH = 300;
  public static final int GRIDVIEW_HEIGHT = 300;
  public static final int GRIDVIEW_GAP = 3;
  public static final int GRIDVIEW_X_START = 100;
  public static final int GRIDVIEW_Y_START = 100;


  /**
   * MainView variables
   */
  public static final String CHECKERS = "Checkers";
  public static final String CONNECTFOUR = "ConnectFour";
  public static final String GOMOKU = "Gomoku";
  public static final String OTHELLO = "Othello";
  public static final List<String> GAMES = List.of(CHECKERS, CONNECTFOUR, GOMOKU, OTHELLO);
  public static final List<String> MODES = List.of("PvP", "PvC", "CvC");
  public static final List<String> GAME_MODE_METHODS = List.of("handlePlayers",
      "handlePlayerAndComputer", "handleComputers");
  public static final List<String> LANGUAGES = List.of("English", "Spanish", "French");
  public static final List<String> COLORS = List.of("Dark", "Light");
  public static final List<List<String>> MENUBARS = List.of(GAMES, MODES, LANGUAGES, COLORS);
  public static final String NEW_DATA_EXCEPTION = "You need to enter a game type and game mode, please try again.";
  public static final String INVALID_SIM_FILE = "Invalid File Input. Please select a SIM File.";
  public static final String FILE_CHOOSER_SIM = "SIM files (*.sim)";
  public static final String SIM_EXTENSION = "*.sim";
  public static final List<String> BUTTON_NAMES = List.of("Start", "Load");
  public static final String CREATE = "Create";
  public static final String CSS_ENDING = "%s.css";
  public static final List<String> MAIN_VIEW_VARIABLES = List.of("game", "matchUp", "language",
      "color");
  public static final List<String> MAIN_VIEW_MENUBARS = List.of("GameType", "GameMode", "Language",
      "Color");
  public static final List<String> STYLE_METHODS = List.of("changeLanguages", "updateSceneColors");
  public static final List<String> TITLES = List.of("Player1", "Player2",
      "Player");
  public static final List<String> HEADERS = List.of("Player1Name",
      "Player2Name", "PlayerName");
  public static final String CONTEXT_TEXT = "Name";
  public static final List<String> CPUS = List.of("CPU 1", "CPU 2", "CPU");
  public static final String NULL_NAME_INPUT = "You must input a name for the specified player. Please try again.";
  public static final int NUMBER_OF_GAME_MENUBARS = 2;
  public static final String WELCOME_MESSAGE = "Welcome! Please select a game type, mode, language and color to start the game. You can also load a file directly!";
  public static final String SPLIT_STRING = "v";
  public static final int WELCOME_SIZE = 330;
  public static final String DATA_PATH = "data";
  public static final int ZERO = 0;
  public static final int ONE = 1;
  public static final int TWO = 2;
  public static final String DEFAULT_GAME_SETUP = "defaultGameSetUp";

  /**
   * GameController variables
   */
  public static final String GRID_VIEW_CLICK_PIECE = "gridViewClickPiece";
  public static final String GAME_VIEW_CLICK_PIECE = "gameViewClickPiece";
  public static final String GRID_VIEW_ADD_PIECE = "gridViewAddPiece";
  public static final String GAME_VIEW_ADD_PIECE = "gameViewAddPiece";
  public static final String GAME_CONTROLLER_CLICK_PIECE = "gameControllerPieceClicked";
  public static final String GAME_CONTROLLER_SAVE_CLICKED = "gameControllerSaveClicked";
  public static final String GAME_CONTROLLER_STEP = "gameControllerStep";
  public static final String GAME_CONTROLLER_BACK_BUTTON_CLICKED = "backButtonClicked";
  public static final String MAIN_CONTROLLER_PARSER_EXCEPTION = "Parser Exception";
  public static final List<Color> GAME_CONTROLLER_PIECE_COLORS = Arrays.asList(Color.WHITE,
      Color.BLACK);
  public static final String PLAYER_TYPE = "P";
  public static final int DEFAULT_PLAYER_INDEX = 0;
  public static final int DEFAULT_BOARD_HEIGHT = 8;
  public static final int DEFAULT_BOARD_WIDTH = 8;
  public static final int DEFAULT_BOARD_HEIGHT_CONNECTFOUR = 6;
  public static final int DEFAULT_BOARD_WIDTH_CONNECTFOUR = 7;

  /**
   * MainController variables
   */
  public static final String CONTROLLER_PROPERTIES = "The attempted action does not exist.";
  public static final List<String> CONTROLLER_METHODS = List.of("createGame", "loadGame",
      "changeGameScene", "changeHomeScene", "changeToGameKeyPress", "parserException");
  public static final List<String> CONTROLLER_PROMPTS = List.of(CREATE, "Load", "New Game", "Back", "Game Click", MAIN_CONTROLLER_PARSER_EXCEPTION);

  /**
   * Used for View tests
   */
  public static final String VIEW_ID_SEARCH = "#%s";

  /**
   * Reflection variables
   */
  public static final String REFLECTION_FACTORY_METHOD_REFLECTION = "Method";
  public static final String REFLECTION_FACTORY_PROPERTY_REFLECTION = "PropertyChangeReflection";
  public static final String REFLECTION_FACTORY_FIELD_REFLECTION = "Field";

  /**
   * Algorithms variables
   */
  public static final int SCORE_FOR_WINNING = 10;
  public static final int CHECKERS_MINIMAX_DEPTH = 4;
  public static final int CONNECT_FOUR_MINIMAX_DEPTH = 6;
  public static final int GOMOKU_MINIMAX_DEPTH = 3;
  public static final int OTHELLO_MINIMAX_DEPTH = 3;

  /**
   * Cell State variables
   */
  public static final int EMPTY_STATE = 0;
  public static final int STALEMATE = -1;


  /**
   * Model variables
   */
  public static final int PLAYER_ONE = 1;
  public static final int PLAYER_TWO = 2;
  public static final int CONNECT_FOUR_CONNECT_NUMBER = 4;
  public static final int GOMOKU_CONNECT_NUMBER = 5;



  public static final List<String> GET_KEY_PRESSES_MAIN = List.of("CPUGomoku", "CPUConnectFour", "CPUCheckers", "CPUOthello",
          "humanCPUGomoku", "humanCPUConnectFour", "humanCPUCheckers", "humanCpuOthello", "checkersKingGame", "stalemateCondition");
  public static final List<Object> MAIN_KEYS = List.of("G", "F", "C", "O", "H", "I", "J", "L", "K", "D");
  public static final List<String> GET_KEY_PRESSES_GAME = List.of("saveGame", "backToHome", "optimalNextMove");
  public static final List<String> GAME_KEYS = List.of("S", "B", "W");
}
