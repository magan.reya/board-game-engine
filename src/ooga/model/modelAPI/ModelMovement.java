package ooga.model.modelAPI;

import ooga.components.componentAPI.ImmutableGrid;
import ooga.players.Player;

import java.awt.*;

/**
 * This is an interface that deals with all the movement in the model. Movement is defined
 * as the placing of a piece onto the grid in terms of the game logic. This interface will
 * initialize the movement, and keep track of the current player that is doing this movement.
 */
public interface ModelMovement {

    /**
     * This method returns an immutable grid object to the controller such that the view
     * does not have the ability to modify the grid, but can access the data it stores
     * @return Immutable Grid
     */
    ImmutableGrid getImmutableGrid();

    /**
     * This method will be called by the game controller to do the movement (placement) of pieces in the game.
     * It takes in the point to play and the player playing it, and throws an exception in the case
     * of an invalid move
     * @param played: a point object at which a piece is to be placed
     * @param p: the player object placing the piece
     * @throws Exception: throws a custom "Invalid Move" exception if an invalid position is selected
     */
    void move(Point played, Player p) throws  Exception;

    /**
     * A setter to set the position to move a new point to. It calls a method to check if the placement
     * is valid, and returns a boolean as to whether or not the player can make the proposed move.
     * It takes in the column and row for the point to be placed at. For example, for Othello one can
     * only palce a piece if doing so will surround pieces of opposite state and flip them. If this is not the
     * case, this method will return false.
     * @param col: column of point to place
     * @param row: row of point to place
     * @return boolean as to whether the placement can happen
     */
    boolean setPositionToBePlaced(int col, int row);

    /**
     * This method returns the position that has been chosen as the next placement. It assumes
     * that it was set up correctly and is a valid position.
     * @return a point object describing the current placement
     */
    Point getPositionToBePlaced();

    /**
     * This method sets the ID of the player that is currently playing.
     * @param ID: The ID to set for the player that will play (can be player 1 or 2)
     */
    void setCurrentPlayerID(int ID);

    /**
     * This method returns the player ID of the current player
     * @return an int of the ID of the current player
     */
    int getCurrentPlayerID();
}
