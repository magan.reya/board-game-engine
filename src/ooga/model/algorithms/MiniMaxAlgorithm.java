package ooga.model.algorithms;

import ooga.resources.Config;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/***
 * This is an interface that characterizes the minimax algorithm used
 * across all four games for the smart CPU player.
 */
public interface MiniMaxAlgorithm {

    /***
     * This method runs through the minimax algorithm recursively and search for the optimal move.
     * After the algorithm finds the optimal move, it will set mySmartPlaced (and mySmartRemoved for Checkers).
     */
    void playSmart();

    /**
     * Method determines if a game instance has been won by any player.
     *
     * @return if the game's end game condition has been satisfied.
     */
    boolean checkEndGameCondition();

    /***
     * Getter method that tells the status of the game.
     *
     * @return 0 if no one has won, otherwise return the winning player's ID.
     */
    int getGameStatus();

    /***
     * Setter method that sets the status of the game.
     *
     * @param gameStatus is 0 if no one has won, otherwise it is the winning player's ID.
     */
    void setGameStatus(int gameStatus);

    /***
     * Setter method that stores the optimal place decided by the minimax algorithm.
     *
     * @param point is the optimal place for current player.
     */
    void setSmartPlace(Point point);

    /***
     * This method returns all the possible moves for a player.
     *
     * @param possibles is a list. For Checkers, the first Object is the playerID (Integer) and
     *                  the second Object is the Point this player wants to remove.
     *                  For other three games, the only Object is the playerID (Integer).
     * @return a List of Points where the player can place the next piece.
     */
    List<Point> getAllPossiblePlaces(List<Object> possibles);

    /***
     * This method will place a piece at the location passed in, simulating the real interaction of a game.
     *
     * @param point is the location that minimax algorithm wants the player to place a piece at,
     *              so that it can record the gains and losses.
     * @param playerID is the player's ID.
     */
    void tryMiniMax(Point point, int playerID);

    /***
     * This method undos the previous execution resulted from running the minimax algorithm.
     *
     * @param point is the location where a piece was placed before only for running the minimax algorithm.
     */
    void recoverFromMiniMax(Point point);

    /***
     * This method run minimax algorithm recursively
     *
     * @param depth is the current depth of the algorithm/search
     * @param playerID is the player's ID at current depth
     * @param maxDepth is the maximum depth (where the root is located or where the algorithm started)
     * @return the score at the depth
     */
    default int recursiveMiniMax(int depth, int playerID, int maxDepth) {
        if (checkEndGameCondition()) {
            int score = getGameStatus() == 1 ?
                    Config.SCORE_FOR_WINNING + depth : -(Config.SCORE_FOR_WINNING + depth);
            setGameStatus(0);
            return score;
        }
        if (depth == 0) return 0;

        java.util.List<Point> allPossiblePlaces = getAllPossiblePlaces(List.of(playerID));
        List<Integer> allScores = new ArrayList<>();
        if (!allPossiblePlaces.isEmpty()) {
            for (Point point : allPossiblePlaces) {
                tryMiniMax(point, playerID);
                int eval = recursiveMiniMax(depth - 1, (playerID % 2) + 1, maxDepth);
                recoverFromMiniMax(point);
                allScores.add(eval);
            }
            int val = playerID == 1 ? Collections.max(allScores) : Collections.min(allScores);
            int idx = allScores.indexOf(val);
            if (depth == maxDepth) setSmartPlace(allPossiblePlaces.get(idx));
            return val;
        } else {
            return 0;
        }
    }
}


