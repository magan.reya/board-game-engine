package ooga.model;

import java.util.List;
import java.awt.Point;
import ooga.model.algorithms.MiniMaxAlgorithm;
import ooga.model.modelAPI.ModelMovement;
import ooga.players.Player;
import ooga.components.Grid;
import ooga.components.componentAPI.ImmutableGrid;

import static ooga.Main.LOGGER;

/**
 * Abstract model class that holds functionality to implement the logic of all four
 * games Othello, Checkers, Connect Four, Gomoku. It keeps track of all the current
 * states of the grid and the players playing the game.
 */
public abstract class Model implements ModelMovement, MiniMaxAlgorithm {

  private int myGameStatus;
  private int myCurrentPlayerID;
  private Point myPosToBePlaced;
  private Point mySmartPlace;
  private Grid myGrid;

  /**
   * Model constructor. It takes in a 2D array that symbolizes our grid that holds the
   * status of the game at all time
   * @param states - a 2D integer array that holds the status of all the moves in the game
   */
  public Model(int[][] states) {
    myGrid = new Grid(states);
    myGameStatus = 0;
    myCurrentPlayerID = 1;
  }

  /**
   * Method determines if a game instance has been won by any player
   *
   * @return boolean as to whether the game's end game condition has been satisfied
   */
  public abstract boolean checkEndGameCondition();

  /**
   * Determines if a clicked target is a valid move and loads it into myPosToBePlaced if valid
   *
   * @param col - the column of the clicked target
   * @param row - the column of the clicked target
   * @return TRUE if a move is valid, FALSE otherwise
   */
  public boolean setPositionToBePlaced(int col, int row) {
    Point point = new Point(col, row);
    if (isValidPlace(point)) {
      myPosToBePlaced = point;
      return true;
    }
    return false;
  }
  /**
   * Method controls move sequence given player type (CPU vs HUMAN) and check if valid move
   *
   * @param played - Point that has been added to the board
   * @param p      - the Player who's move it is
   * @throws Exception - if the move is invalid
   */
  public void move(Point played, Player p) throws Exception {
    setCurrentPlayerID(p.getID());
    if (p.isCPU()) {
      playSmart();
      execute();
    } else if (setPositionToBePlaced(played.x, played.y)) {
      execute();
    } else {
      throw new Exception("Invalid Click!");
    }
  }

  /**
   * This method is called by the game controller to increment the score of the game being played.
   * It takes in a player object and adds one point to it (as scoring for most games is done
   * on the number of turns)
   * @param players - a list of all of the players within the game that need their scores updated
   */
  public void incrementScore(List<Player> players){
    for(Player p : players){
      if(p.getID() == myCurrentPlayerID) {
        p.setScore(p.getScore() + 1);
      }
    }
  }

  /**
   * This method returns an immutable grid object to the controller such that the view
   * does not have the ability to modify the grid, but can access the data it stores
   * @return Immutable Grid
   */
  public ImmutableGrid getImmutableGrid() {
    return myGrid;
  }

  /**
   * This method returns the player ID of the current player
   * @return an int of the ID of the current player
   */
  public int getCurrentPlayerID() {
    return myCurrentPlayerID;
  }

  /**
   * This method sets the ID of the player that is currently playing.
   * @param ID: The ID to set for the player that will play (can be player 1 or 2)
   */
  public void setCurrentPlayerID(int ID) {
    myCurrentPlayerID = ID;
  }

  /***
   * Getter method that tells the status of the game
   *
   * @return 0 if no one has won, otherwise return the winning player's ID
   */
  public int getGameStatus() {
    return myGameStatus;
  }

  /***
   * Setter method that sets the status of the game
   *
   * @param status is 0 if no one has won, otherwise it is the winning player's ID
   */
  public void setGameStatus(int status) {
    myGameStatus = status;
  }

  /**
   * This method returns the position that has been chosen as the next placement. It assumes
   * that it was set up correctly and is a valid position.
   * @return a point object describing the current placement
   */
  public Point getPositionToBePlaced() {
    return myPosToBePlaced;
  }

  protected Grid getGrid() {
    return myGrid;
  }

  protected boolean isValidPlace(Point point) {
    return myGrid.isInsideBoard(point) && isSpaceInGridAvailable(point);
  }

  protected boolean isSpaceInGridAvailable(Point point) {
    return myGrid.isInsideBoard(point) && !myGrid.doesGridHavePiece(point);
  }

  protected void addPiece(Point point) {
    myGrid.addPiece(point, myCurrentPlayerID);
  }

  protected Point setUpPoint(int x, int y) {
    return new Point(x, y);
  }

  protected void execute() {
    addPiece(myPosToBePlaced);
    LOGGER.info(String.format("Player %d executed move @ (%d, %d)", myCurrentPlayerID, getPositionToBePlaced().x, getPositionToBePlaced().y));
    checkEndGameCondition();
  }

  /***
   * This method runs through the minimax algorithm recursively and search for the optimal move.
   * After the algorithm finds the optimal move, it will set mySmartPlaced (and mySmartRemoved for Checkers)
   */
  public void playSmart() {
    checkEndGameCondition();
  }

  /***
   * Setter method that stores the optimal place decided by the minimax algorithm.
   *
   * @param smartPlace is the optimal place for current player.
   */
  public void setSmartPlace(Point smartPlace){
    mySmartPlace = smartPlace;
  }

  /***
   * Getter method that returns the optimal place decided by the minimax algorithm.
   *
   * @return the optimal place for current player
   */
  protected Point getSmartPlace() {
    return mySmartPlace;
  }

  /***
   * This method will place a piece at the location passed in, simulating the real interaction of a game.
   *
   * @param pointToBePlaced is the location that minimax algorithm wants the player to place a piece at,
   *              so that it can record the gains and losses.
   * @param playerID is the player's ID.
   */
  public void tryMiniMax(Point pointToBePlaced, int playerID) {
    getGrid().addPiece(pointToBePlaced, playerID);
    myPosToBePlaced = pointToBePlaced;
  }

  /***
   * This method undos the previous execution resulted from running the minimax algorithm.
   *
   * @param pointToBeRemoved is the location where a piece was placed before only for running the minimax algorithm.
   */
  public void recoverFromMiniMax(Point pointToBeRemoved) {
    getGrid().removePiece(pointToBeRemoved);
  }

  /***
   * This method returns all the possible moves for a player.
   *
   * @param possibles is a list. For Checkers, the first Object is the playerID (Integer) and
   *                  the second Object is the Point this player wants to remove.
   *                  For other three games, the only Object is the playerID (Integer).
   * @return a List of Points where the player can place the next piece.
   */
  public abstract List<Point> getAllPossiblePlaces(List<Object> possibles);
}