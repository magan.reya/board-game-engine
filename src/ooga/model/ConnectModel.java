package ooga.model;

import java.awt.Point;

import ooga.components.Cell;

/**
 * Abstract connect model class that holds rules and mechanics of connecting games (where players
 * win by placing a number of pieces in a row). Connect Four and Gomoku are special cases of this
 * class (subclasses) where the number of pieces are 4 and 5 pieces in a row, respectively. This
 * abstract class also extends the model class to inherited fundamental APIs
 */
public abstract class ConnectModel extends Model {

  protected int myThreshold; //number of pieces in-a-row to win
  protected int myMaxRecursionDepth; //maximum recursion depth (strength) of minimax algorithm

  /**
   * Model constructor. It takes in a 2D array that symbolizes our grid that holds the status of the
   * game at all time
   *
   * @param states - a 2D integer array that holds the status of all the moves in the game
   */
  public ConnectModel(int[][] states) {
    super(states);
  }

  /**
   * This method determines if the end game condition has been met by checking if there exists N
   * in-a-row pieces in the game's current Grid.
   *
   * @return TRUE if there is a winner, FALSE otherwise
   */
  public boolean checkEndGameCondition() {
    return hasInARow(myThreshold);
  }

  public void playSmart() {
    recursiveMiniMax(myMaxRecursionDepth, getCurrentPlayerID(), myMaxRecursionDepth);
    setPositionToBePlaced(getSmartPlace().x, getSmartPlace().y);
  }

  /**
   * This method determines if there are a given number of pieces in-a-row in the current game. If
   * the threshold is met, then the game has been won by the current player, although that logic
   * exists in the super Model class.
   *
   * @param threshold to meet in terms of number of pieces in-a-row
   * @return TRUE if grid has met threshold (ie, there is a winner), FALSE otherwise
   */
  protected boolean hasInARow(int threshold) {
    if (getPositionToBePlaced() == null) {
      return false;
    }
    Cell newlyAddedPiece = getGrid().getCell(getPositionToBePlaced());
    for (Cell neighbor : newlyAddedPiece.getCompleteNeighbors()) {
      int directX = neighbor.getLocation().x - getPositionToBePlaced().x;
      int directY = neighbor.getLocation().y - getPositionToBePlaced().y;
      int counter =
          1 + countSamePiece(newlyAddedPiece, directX, directY) + countSamePiece(newlyAddedPiece,
              -directX, -directY);
      if (counter >= threshold) {
        setGameStatus(newlyAddedPiece.getState());
        return true;
      }
    }
    return false;
  }

  private int countSamePiece(Cell center, int directX, int directY) {
    int counter = 0;
    Point nextPointToFind = setUpPoint(center.getLocation().x + directX,
        center.getLocation().y + directY);
    while (true) {
      if (getGrid().isInsideBoard(nextPointToFind) &&
          getGrid().getState(nextPointToFind.x, nextPointToFind.y)
              == center.getState()) {
        counter++;
        nextPointToFind = setUpPoint(nextPointToFind.x + directX, nextPointToFind.y + directY);
      } else {
        return counter;
      }
    }
  }
}
