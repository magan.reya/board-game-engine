package ooga.model;

import ooga.resources.Config;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Extended class from the abstract ConnectModel class to implement the game of ConnectFour. It
 * keeps all the functionality provided in the abstract class and also implements more unique logic
 * specific to the ConnectFour game.
 */
public class ConnectFourModel extends ConnectModel {

  /**
   * ConnectFour model constructor. It takes in a 2D array that symbolizes our grid that holds the
   * status of the game at all time
   *
   * @param states - a 2D integer array that holds the status of all the moves in the game
   */
  public ConnectFourModel(int[][] states) {
    super(states);
    myThreshold = Config.CONNECT_FOUR_CONNECT_NUMBER;
    myMaxRecursionDepth = Config.CONNECT_FOUR_MINIMAX_DEPTH;
  }

  /**
   * Redefines a "legal" move specific to the rules of ConnectFour whereby pieces "drop" to the
   * bottom. This method is inherited from the Model super class and implements the fundamental
   * difference
   *
   * @param target location of the piece represented as a Point.
   * @return TRUE if a target location if a valid more, FALSE otherwise
   */
  @Override
  protected boolean isValidPlace(Point target) {
    Point below = new Point(target.x, target.y + 1);
    boolean targetIsValid = getGrid().isInsideBoard(target) && isSpaceInGridAvailable(target);
    boolean belowTargetIsValid = getGrid().isInsideBoard(below) && isSpaceInGridAvailable(below);
    return targetIsValid && !belowTargetIsValid;
  }

  /**
   * Determines set of possible moves given the current state of the game
   *
   * @return List of Points with invalid bottom locations (ie, a piece below it)
   */
  @Override
  public List<Point> getAllPossiblePlaces(List<Object> possibles) {
    Set<Point> possiblePlaces = new HashSet<>();
    Point temp;
    for (int col = 0; col < getGrid().getNumCols(); col++) {
      for (int row = getGrid().getNumCols() - 1; row >= 0; row--) {
        temp = new Point(col, row);

        if (isValidPlace(temp)) {
          possiblePlaces.add(temp);
          break;
        }
      }
    }
    return new ArrayList<>(possiblePlaces);
  }
}