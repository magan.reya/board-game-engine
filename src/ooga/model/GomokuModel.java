package ooga.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.awt.Point;

import ooga.components.Cell;
import ooga.resources.Config;

/**
 * Extended class from the abstract ConnectModel class to implement the game of Gomoku. It keeps all
 * the functionality provided in the abstract class and also implements more unique logic specific
 * to the Gomoku game.
 */
public class GomokuModel extends ConnectModel {

  /**
   * Gomoku model constructor. It takes in a 2D array that symbolizes our grid that holds the status of the
   * game at all time
   *
   * @param states - a 2D integer array that holds the status of all the moves in the game
   */
  public GomokuModel(int[][] states) {
    super(states);
    myThreshold = Config.GOMOKU_CONNECT_NUMBER;
    myMaxRecursionDepth = Config.GOMOKU_MINIMAX_DEPTH;
  }

  /**
   * Gomoku's specific implementation of getAllPossiblePlaces (inheriting from the MiniMaxAlgorithm
   * interface and overriding from the Model super class).
   *
   * @param possibles is a list. For Checkers, the first Object is the playerID (Integer) and the
   *                  second Object is the Point this player wants to remove. For other three games,
   *                  the only Object is the playerID (Integer).
   * @return the list of possible moves by the current player; "moves" are represented as Points.
   */
  @Override
  public List<Point> getAllPossiblePlaces(List<Object> possibles) {
    if (getGrid().getAllPieces().size()
        == 0) { // CPU player places the first piece, randomly choose a location
      Set<Point> allPoints = getGrid().getAllPoints();
      return new ArrayList<>(Arrays.asList(
          allPoints.stream().skip(new Random().nextInt(allPoints.size())).findFirst()
              .orElse(null)));
    }
    Set<Point> possiblePlaces = new HashSet<>();
    for (Point piece : getGrid().getAllPieces()) {
      for (Cell neighbor : getGrid().getCell(piece).getCompleteNeighbors()) {
        if (neighbor.isEmpty()) {
          possiblePlaces.add(neighbor.getLocation());
        }
      }
    }
    return new ArrayList<>(possiblePlaces);
  }
}
