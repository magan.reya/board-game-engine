package ooga.model;

import ooga.components.Cell;
import ooga.resources.Config;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Extended class from the abstract Model class to implement the game of Checkers.
 * It keeps all the functionality provided in the abstract class and also implements more
 * unique logic specific to the Checkers game.
 */
public class CheckersModel extends Model {
    private boolean needCapture, needUpgradeKing;
    private int capturedState;
    private Point myPosToBeCaptured;
    private Point myPosToBeRemoved;
    private int myRemovedState;
    private Point mySmartRemoved;

    /***
     * This the constructor for the Checkers game.
     * We assume that Player 1 starts from the upper edge and Player 2 starts from the lower edge
     *
     * @param states is the initial state of the grid/board of the game
     */
    public CheckersModel(int[][] states){
        super(states);
        needCapture = false;
        needUpgradeKing = false;
    }

    /**
     * Determines if a clicked target is a valid move and loads it into myPosToBeRemoved if valid
     *
     * @param col - the column of the clicked target
     * @param row - the column of the clicked target
     * @return TRUE if a move is valid, FALSE otherwise
     */
    public boolean setPositionToBeRemoved(int col, int row) {
        Point point = new Point(col, row);
        if(isValidRemove(point)) {
          myPosToBeRemoved = point;
          return true;
        }
        return false;
    }

    private boolean isValidRemove(Point point) {
        return !isSpaceInGridAvailable(point)
                && doesPieceBelongsToPlayer(getCurrentPlayerID(), getGrid().getCell(point))
                && getAllPossiblePlaces(List.of(getCurrentPlayerID(), point)).size() > 0;
    }

    @Override
    protected boolean isValidPlace (Point placed) {
        myRemovedState = getGrid().getState(myPosToBeRemoved.x, myPosToBeRemoved.y);
        Cell removedPiece = getGrid().getCell(myPosToBeRemoved);
        List<Cell> neighbors = findNeighbors(removedPiece);

        for (Cell neighbor: neighbors) {
            if(placed.equals(neighbor.getLocation())) {
                if (neighbor.isEmpty()) {
                    needCapture = false;
                    return true;
                }
                else return false;
            }
            if(!neighbor.isEmpty() && !doesPieceBelongsToPlayer(getCurrentPlayerID(), neighbor)) {
                if(captureIfPossible(neighbor, placed)) return true;
            }
        }
        return false;
    }

    @Override
    protected void execute() {
        getGrid().removePiece(myPosToBeRemoved);
        if(needCapture) {
            getGrid().removePiece(myPosToBeCaptured);
        }
        getGrid().addPiece(getPositionToBePlaced(), myRemovedState);
        needUpgradeKing = upgradeToKingIfPossible(getPositionToBePlaced());
        checkEndGameCondition();
    }

    private boolean upgradeToKingIfPossible(Point point) {
        int ID = getCurrentPlayerID();
        if((ID == 1 && point.y == getGrid().getNumRows() - 1) || (ID == 2 && point.y == 0)) {
            if(!isKing(point)) {
                getGrid().getCell(point).setState(-ID);
                return true;
            }
        }
        return false;
    }

    private List<Cell> findNeighbors (Cell removedPiece) {
        List<Cell> neighbors = new ArrayList<>();
        if (removedPiece.getState() < 0) { // if removedPiece is a King
            neighbors = removedPiece.getDiagonalNeighbors(); // consider all pieces around it on the diagonal
        }
        else if (removedPiece.getState() == 1){
            neighbors = removedPiece.getLowerDiagonalNeighbors();
        }
        else if (removedPiece.getState() == 2) {
            neighbors = removedPiece.getUpperDiagonalNeighbors();
        }
        return neighbors;
    }

    private boolean captureIfPossible(Cell neighbor, Point placed) {
        Point pointOnNextDiagonal = getCellOnTheNextDiagonal(myPosToBeRemoved, neighbor.getLocation());
        if(isSpaceInGridAvailable(pointOnNextDiagonal) && pointOnNextDiagonal.equals(placed)) {
            needCapture = true;
            capturedState = neighbor.getState();
            myPosToBeCaptured = neighbor.getLocation();
            return true;
        }
        needCapture = false;
        return false;
    }

    @Override
    public List<Point> getAllPossiblePlaces(List<Object> possibles) {
        List<Point> possibleMoves = new ArrayList<>();
        Cell removedPiece = getGrid().getCell((Point) possibles.get(1));
        int playerID = ((Integer) possibles.get(0)).intValue();
        assert(doesPieceBelongsToPlayer(playerID, removedPiece));
        List<Cell> neighbors = findNeighbors(removedPiece);
        for (Cell cell: neighbors) {
            if(cell.isEmpty()) {
                possibleMoves.add(cell.getLocation());
            }
            else if(!doesPieceBelongsToPlayer(playerID, cell)) {
                Point pointOnNextDiagonal = getCellOnTheNextDiagonal(removedPiece.getLocation(), cell.getLocation());
                if(isSpaceInGridAvailable(pointOnNextDiagonal)) {
                    possibleMoves.add(pointOnNextDiagonal);
                }
            }
        }
        return possibleMoves;
    }

    private Point getCellOnTheNextDiagonal(Point start, Point mid) {
        return new Point(2 * mid.x - start.x, 2 * mid.y - start.y);
    }

    private boolean doesPieceBelongsToPlayer(int playerID, Cell cell) {
        return cell.getState() == playerID || -cell.getState() == playerID;
    }

    private List<Point> getAllPiecesBasedOnID(int id) {
        List<Point> pieces = getGrid().getAllPointsBasedOnID(id);
        pieces.addAll(getGrid().getAllPointsBasedOnID(-id));
        return pieces;
    }

    private boolean isKing(Point point) {
        return getGrid().getState(point.x, point.y) < 0;
    }

    private boolean canMakeAnyMove(int player) {
        List<Point> currentPieces = getAllPiecesBasedOnID(player);
        for(Point piece: currentPieces) {
            if(!getAllPossiblePlaces(List.of(player, piece)).isEmpty()) {
                return true;
            }
        }
        setGameStatus(3-player);
        return false;
    }

    /**
     * Method determines if a game instance has been won by any player
     *
     * @return boolean as to whether the game's end game condition has been satisfied
     */
    public boolean checkEndGameCondition() {
        return !canMakeAnyMove(Config.PLAYER_ONE) || !canMakeAnyMove(Config.PLAYER_TWO);
    }

    @Override
    public void playSmart() {
        int id = getCurrentPlayerID();
        recursiveMiniMax(Config.CHECKERS_MINIMAX_DEPTH, getCurrentPlayerID(), Config.CHECKERS_MINIMAX_DEPTH);
        setCurrentPlayerID(id);
        setPositionToBeRemoved(mySmartRemoved.x, mySmartRemoved.y);
        setPositionToBePlaced(getSmartPlace().x, getSmartPlace().y);
    }

    @Override
    public int recursiveMiniMax(int depth, int playerID, int maxDepth) {
        setCurrentPlayerID(playerID);
        if (checkEndGameCondition()) {
            int score = getGameStatus() == 1 ?
                    Config.SCORE_FOR_WINNING + depth : -(Config.SCORE_FOR_WINNING + depth);
            setGameStatus(0);
            return score;
        }
        if (depth == 0) return 0;

        int val = getCurrentPlayerID() == 1 ? -99999 : 99999;
        List<Point> allPossibleRemoved = getAllPiecesBasedOnID(getCurrentPlayerID());
        for(Point pointToBeRemoved: allPossibleRemoved) {
            java.util.List<Point> allPossiblePlaces = getAllPossiblePlaces(List.of(getCurrentPlayerID(), pointToBeRemoved));
            for (Point pointToBePlaced : allPossiblePlaces) {
                val = minimaxSubProcedure(depth, maxDepth, pointToBeRemoved, pointToBePlaced, playerID, val);
            }
        }
        return val;
    }

    private void tryMiniMax (Point pointToBeRemoved, Point pointToBePlaced) {
        setPositionToBeRemoved(pointToBeRemoved.x, pointToBeRemoved.y);
        setPositionToBePlaced(pointToBePlaced.x, pointToBePlaced.y);
        execute();
    }

    private void recoverFromMiniMax(Point pointToBeRemoved, int removedValue, Point pointToBePlaced, Boolean ifCaptured, Point captured, int capturedStateTemp) {
        if(ifCaptured) getGrid().addPiece(captured, capturedStateTemp);
        getGrid().removePiece(pointToBePlaced);
        getGrid().addPiece(pointToBeRemoved, removedValue);
        needUpgradeKing = false;
        needCapture = false;
    }

    private int minimaxSubProcedure(int depth, int maxDepth, Point pointToBeRemoved, Point pointToBePlaced, int playerID, int val) {
        int removedValue = getGrid().getState(pointToBeRemoved.x, pointToBeRemoved.y);
        tryMiniMax(pointToBeRemoved, pointToBePlaced);
        boolean ifCaptured = needCapture;
        int capturedStateTemp = capturedState;
        Point captured = needCapture? myPosToBeCaptured: null;
        int eval = needUpgradeKing? 3:0;
        eval += needCapture? 3:0;
        eval += recursiveMiniMax(depth - 1, (playerID % 2) + 1, maxDepth);
        setCurrentPlayerID(playerID);
        recoverFromMiniMax(pointToBeRemoved, removedValue, pointToBePlaced, ifCaptured, captured, capturedStateTemp);
        if((playerID == 1 && eval > val) || (playerID == 2 && eval < val)) {
            val = eval;
            if (depth == maxDepth) {
                mySmartRemoved = pointToBeRemoved;
                setSmartPlace(pointToBePlaced);
            }
        }
        return val;
    }
}
