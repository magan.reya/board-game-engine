package ooga.model;
import ooga.components.Cell;
import ooga.players.Player;
import ooga.resources.Config;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Extended class from the abstract Model class to implement the game of Othello.
 * It keeps all the functionality provided in the abstract class and also implements more
 * unique logic specific to the Othello game.
 */
public class OthelloModel extends Model {
    private boolean canFlank = false;
    private final List<Cell> cellsToFlank = new ArrayList<>();

    /**
     * Constructor for the Othello game. It takes in the same 2D array as the model.
     * @param states - 2D int array of all the current states in the game.
     */
    public OthelloModel(int[][] states){
        super(states);
    }

    /**
     * Unlike the other three games, the Othello game's score is based on how many of each
     * players' pieces are on the grid. So at each increment call, the pieces corresponding to each
     * player respectively are added up to be displayed on screen via the game controller.
     * @param players - a list of all of the players within the game that need their scores updated
     */
    @Override
    public void incrementScore(List<Player> players){
        for(Player p : players) {
            p.setScore(getGrid().getAllPointsBasedOnID(p.getID()).size());
        }
    }

    protected boolean isValidPlace(Point loc){
        if(!super.isValidPlace(loc)) return false;
        canFlank = false;
        Cell currentPiece = getGrid().getCell(loc);
        for(Cell neighborCell : currentPiece.getCompleteNeighbors()){
            int xDirection = neighborCell.getLocation().x - loc.x;
            int yDirection = neighborCell.getLocation().y - loc.y;
            Cell nextCell = getGrid().getCell(new Point(currentPiece.getLocation().x + xDirection,
                    currentPiece.getLocation().y + yDirection));
            if(nextCell.getState() != getCurrentPlayerID() &&
                    !nextCell.isEmpty()){
                findFlankCells(nextCell, xDirection, yDirection);
            }
            if(canFlank){
                return true;
            }
        }
        cellsToFlank.clear();
        return false;
    }

    private void findFlankCells(Cell flankCell, int directionX, int directionY){
        cellsToFlank.clear();
        cellsToFlank.add(flankCell);
        while(true){
            Cell pieceToFlank = getGrid().getCell(setUpPoint(flankCell.getLocation().x + directionX,
                    flankCell.getLocation().y + directionY));
            if(pieceToFlank != null && !pieceToFlank.isEmpty()){
                if(pieceToFlank.getState() == flankCell.getState()){
                    cellsToFlank.add(pieceToFlank);
                    flankCell = pieceToFlank;
                }
                else{
                    canFlank = true;
                    return;
                }
            }
            else{
                cellsToFlank.clear();
                return;
            }
        }
    }

    private void flankCells(){
        cellsToFlank.forEach(c
                -> c.setState(getGrid().getCell(getPositionToBePlaced()).getState()));
    }

    /**
     * This method gets all the possible moves for the Othello game. It looks at all of the currently
     * empty cells within the game grid, and for each empty cell attempts to put a piece corresponding
     * to the current player. If this piece would cause the player to surround the opposing player's
     * pieces, this move is added to a list of possible moves.
     * @param possibles is a list. For Othello, the first Object is the playerID (Integer).
     * @return - a list of all possible moves for the current player
     */
    @Override
    public List<Point> getAllPossiblePlaces(List<Object> possibles){
        Set<Point> possible = new HashSet<>();
        for(Point p : getGrid().getAllPointsBasedOnID(Config.EMPTY_STATE)){
            Cell c = getGrid().getCell(p);
            isValidPlace(p);
            Integer value = (Integer) possibles.get(0);
            c.setState(value.intValue());
            if(canFlank){
                canFlank = false;
                possible.add(p);
            }
            c.setState(Config.EMPTY_STATE);
        }
        cellsToFlank.clear();
        canFlank = false;
        return new ArrayList<>(possible);
    }

    /**
     * This is the end game condition for Othello. Othello ends either when the board is completely filled,
     * or one player can no longer make a move. If either of these conditions are satisfied, the player with
     * the most pieces on the board wins. If there is a tie in number of pieces on the board,
     * a stalemate is sent out signalling this.
     * @return TRUE if game is over, FALSE if game is still ongoing
     */
    public boolean checkEndGameCondition(){
        if(getGrid().getAllPointsBasedOnID(Config.EMPTY_STATE).isEmpty() || canNoLongerFlank()) {
            setGameStatus(getGrid().getAllPointsBasedOnID(Config.PLAYER_ONE).size() >
                    getGrid().getAllPointsBasedOnID(Config.PLAYER_TWO).size() ? Config.PLAYER_ONE : Config.PLAYER_TWO);
            if(getGrid().getAllPointsBasedOnID(Config.PLAYER_ONE).size() == getGrid().getAllPointsBasedOnID(Config.PLAYER_TWO).size()){
                setGameStatus(Config.STALEMATE);
            }
        }
        return getGameStatus() != 0;
    }

    private boolean canNoLongerFlank(){
        boolean noMoreFlank = (getAllPossiblePlaces(List.of(Config.PLAYER_ONE)).isEmpty() || getAllPossiblePlaces(List.of(Config.PLAYER_TWO)).isEmpty());
        canFlank = true;
        return noMoreFlank;
    }

    @Override
    protected void execute(){
        addPiece(getPositionToBePlaced());
        flankCells();
        checkEndGameCondition();
    }

    /**
     * Play smart algorithm for Othello. It uses the mini max algorithm to run through possible moves and
     * score them based on maximizing the current player and minimizing the opponent. When the best
     * position is found, it is set as the next position to be placed.
     */
    @Override
    public void playSmart() {
        canFlank = false;
        recursiveMiniMax(Config.OTHELLO_MINIMAX_DEPTH, getCurrentPlayerID(), Config.OTHELLO_MINIMAX_DEPTH);
        setPositionToBePlaced(getSmartPlace().x, getSmartPlace().y);
    }

    /**
     * This is a helper function within the mini max function that simulates the othello game by
     * placing a point and seeing how the board changes. The best decision in this helper method
     * is set as the point to place within the mini max algorithm.
     * @param pointToBePlaced is the location that minimax algorithm wants the player to place a piece at,
     *              so that it can record the gains and losses.
     * @param playerID is the player's ID.
     */
    @Override
    public void tryMiniMax (Point pointToBePlaced, int playerID) {
        super.tryMiniMax(pointToBePlaced, playerID);
        isValidPlace(pointToBePlaced);
        flankCells();
    }

    /**
     * As the minimax algorithm simulates points, we need to clear all of the changes so that they
     * do not show on the board. This method clears any flipped cells, and restores empty cells back to
     * empty.
     * @param pointToBeRemoved is the location where a piece was placed before only for running the minimax algorithm.
     */
    @Override
    public void recoverFromMiniMax(Point pointToBeRemoved) {
        super.recoverFromMiniMax(pointToBeRemoved);
        for(Cell c : cellsToFlank){
            c.setState(3-c.getState());
        }
        cellsToFlank.clear();
    }
}