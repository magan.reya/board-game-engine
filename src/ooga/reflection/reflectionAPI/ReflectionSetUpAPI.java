package ooga.reflection.reflectionAPI;

import ooga.components.componentAPI.ImmutableGrid;
import ooga.model.Model;
import ooga.players.Player;
import ooga.view.game.GameView;

import java.util.List;

public interface ReflectionSetUpAPI {
    List<Player> setUpPlayers(List<String> playerNames, List<String> playerTypes);

    Model setUpModel(String gameType, int[][] states);

    GameView setUpGameView(String gameType, String language, String mode, ImmutableGrid grid,
                           Player currPlayer, List<Player> players, List<String> playerTypes);
}
