package ooga.reflection.reflectionAPI;

import java.beans.PropertyChangeListener;
import java.util.List;
import ooga.components.componentAPI.ImmutableGrid;
import ooga.model.Model;
import ooga.players.Player;
import ooga.view.game.GameView;

public interface ReflectionAPI {

    Object getClassReflection(String config, Class[] classParams, Object[] classValues, String type);

    void getNonClassReflection(String type, Class<?> currentClass, String name,
                               Object toActOn, Object value);

}
