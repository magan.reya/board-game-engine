package ooga.parser;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import ooga.parser.csvrules.CSVRule;
import ooga.parser.csvrules.GridStatesCSVRule;
import ooga.parser.csvrules.NumRowColCSVRule;
import ooga.parser.packet.CSVPacket;
import ooga.parser.packet.Packet;
import ooga.parser.packet.SIMPacket;
import ooga.error.ParserException;
import ooga.resources.Config;

public class CSVParser implements Parser {

  private Map<String, CSVRule> csvRuleKeyMap;
  private CSVPacket csvPacket;

  public CSVParser() {
    csvRuleKeyMap = new HashMap<>() {{
      put(Config.CSVPARSER_NUM_ROW_COL, new NumRowColCSVRule());
      put(Config.CSVPARSER_GRID_STATES, new GridStatesCSVRule());
    }};
  }

  /**
   * Given a Packet object, we obtain the file path of the SIMPacket file. We then scan the CSV file
   * line by line
   *
   * @param packet is a Packet object passed from the SIMParser
   * @throws Exception throws a ParserException to the MainView
   */
  @Override
  public void loadFile(Packet packet) throws Exception {
    SIMPacket simPacket = (SIMPacket) packet;
    String filePath = simPacket.getInitialState();

    CSVPacket csvPacket = new CSVPacket(simPacket.getInitialState());

    checkFileType(filePath, Config.CSVFILE_TYPE, Config.INVALID_FILE);

    File file = new File(filePath);
    Scanner in = new Scanner(file);

    // Parse and check the first line
    getParserKey(Config.CSVPARSER_NUM_ROW_COL).evaluateLoad(in.nextLine(), csvPacket);
    csvPacket.setUpStatesArray();

    // Parse the rest of the file
    while (in.hasNext()) {
      csvPacket.checkOutOfBounds();
      getParserKey(Config.CSVPARSER_GRID_STATES).evaluateLoad(in.nextLine(), csvPacket);
    }
    // Do final check
    csvPacket.checkCorrectRowDimensions();
    csvPacket.resetCurrRow();

    // Set csv packet
    setCsvPacket(csvPacket);
  }

  private CSVRule getParserKey(String key) throws Exception {
    if (csvRuleKeyMap.containsKey(key)) {
      return csvRuleKeyMap.get(key);
    }
    throw new ParserException(key, null, Config.CSVPARSER_INVALID_KEY);
  }

  /**
   * Given a CSVPacket, we convert it to a .csv file and save it via FileWriter
   *
   * @param packet is a Packet object passed from the GameController
   * @throws Exception throws a ParserException to the GameView
   */
  @Override
  public void saveFile(Packet packet) throws Exception {
    CSVPacket csvPacket = (CSVPacket) packet;
    String filePath = csvPacket.getFilePath() + Config.CSVFILE_TYPE;

    FileWriter fileWriter = new FileWriter(filePath);

    // Parse first line
    String firstLine = getParserKey(Config.CSVPARSER_NUM_ROW_COL).evaluateSave(
        new int[]{csvPacket.getNumRows(), csvPacket.getNumCols()}
    );
    fileWriter.write(firstLine);

    // Parser the rest of the states
    for (int i = 0; i < csvPacket.getNumRows(); i += 1) {
      String line = getParserKey(Config.CSVPARSER_GRID_STATES).evaluateSave(csvPacket.getCurrRow());
      fileWriter.write(line);
      csvPacket.incrementCurrRow();
    }
    csvPacket.resetCurrRow();
    fileWriter.close();

    // Set csvPacket
    setCsvPacket(null);
  }

  /**
   * Setter for the generated CSVPacket
   *
   * @param csvPacket
   */
  public void setCsvPacket(CSVPacket csvPacket) {
    this.csvPacket = csvPacket;
  }

  /**
   * Getter for the generated SIMPacket
   *
   * @return a SIMPacket object
   */
  public CSVPacket getCSVPacket() {
    return csvPacket;
  }
}
