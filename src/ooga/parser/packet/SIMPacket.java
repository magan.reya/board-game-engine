package ooga.parser.packet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SIMPacket extends Packet {

  private String author;
  private String gameType;
  private String initialState;

  @Deprecated
  private List<String> playerIDs;
  private List<String> playerNames;
  private List<String> playerTypes;
  private String playerTurn;
  private Set<String> keys;

  public SIMPacket() {
    keys = new HashSet<>();
  }

  public SIMPacket(Set<String> keys) {
    this.keys = keys;
  }

  public SIMPacket(Set<String> keys, String author, String gameType, String initialState,
      List<String> playerNames, List<String> playerTypes, String playerTurn) {
    this.keys = keys;
    this.author = author;
    this.gameType = gameType;
    this.initialState = initialState;
    this.playerNames = playerNames;
    this.playerTypes = playerTypes;
    this.playerTurn = playerTurn;
  }

  /**
   * Getters and setters for SIMParser packet
   * @return
   */
  public String getAuthor() {
    return author;
  }

  public String getGameType() {
    return gameType;
  }

  public String getInitialState() {
    return initialState;
  }

  @Deprecated
  public List<String> getPlayerIDs() {
    return playerIDs;
  }

  public List<String> getPlayerNames() {
    return playerNames;
  }

  public List<String> getPlayerTypes() {
    return playerTypes;
  }

  public String getPlayerTurn() {
    return playerTurn;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public void setGameType(String gameType) {
    this.gameType = gameType;
  }

  public void setInitialState(String initialState) {
    this.initialState = initialState;
  }

  @Deprecated
  public void setPlayerIDs(List<String> playerIDs) {
    this.playerIDs = playerIDs;
  }

  public void setPlayerNames(List<String> playerNames) {
    this.playerNames = playerNames;
  }

  public void setPlayerTypes(List<String> playerTypes) {
    this.playerTypes = playerTypes;
  }

  public void setPlayerTurn(String playerTurn) {
    this.playerTurn = playerTurn;
  }

  /**
   * Adds a new key into the keys hashset
   * @param key as a String
   */
  public void addKey(String key) {
    keys.add(key);
  }

  /**
   * Get keys from the set of keys
   * @return HashSet
   */
  public Set<String> getKeys() {
    return keys;
  }
}
