package ooga.parser.packet;

import ooga.components.componentAPI.ImmutableGrid;
import ooga.error.ParserException;
import ooga.resources.Config;

public class CSVPacket extends Packet {

  private int numRows;
  private int numCols;
  private int currRow;
  private int[][] states;
  private String filePath;

  public CSVPacket(String filePath) {
    numRows = 0;
    numCols = 0;
    currRow = 0;
    this.filePath = filePath;
  }

  public CSVPacket(String filePath, ImmutableGrid grid){
    numRows = grid.getNumRows();
    numCols = grid.getNumCols();
    currRow = 0;
    this.filePath = filePath;

    setUpStatesArray();
    setUpStatesArray(grid);
  }

  /**
   * Getters and setters
   */
  public void setNumRows(int rows) {
    numRows = rows;
  }

  public int getNumRows() {
    return numRows;
  }

  public void setNumCols(int cols) {
    numCols = cols;
  }

  public int getNumCols() {
    return numCols;
  }

  /**
   * Given a row of ints, add it to the 2D array of the CSVPacket
   * @param row
   */
  public void addToStates(int[] row) {
    for (int j = 0; j < row.length; j += 1) {
      states[currRow][j] = row[j];
    }
    incrementCurrRow();
  }

  /**
   * Checks whether the 2D array is full populated
   */
  public void checkOutOfBounds() {
    if (currRow >= numRows) {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * Checks whether the given 2D array has the same dimensions as the given rows
   * @throws Exception thrown when invalid
   */
  public void checkCorrectRowDimensions() throws Exception {
    if (numRows != currRow) {
      throw new ParserException(
          String.valueOf(numRows),
          String.valueOf(currRow),
          Config.CSVPARSER_INVALID_NUM_ELEMENTS);
    }
  }


  public int[][] getStates() {
    return states;
  }

  public int[] getCurrRow() {
    return states[currRow];
  }

  public void incrementCurrRow() {
    currRow += 1;
  }

  public void resetCurrRow() {
    currRow = 0;
  }

  public void setUpStatesArray() {
    states = new int[numRows][numCols];
  }

  public void setUpStatesArray(ImmutableGrid grid){
    for (int i = 0; i < numRows; i += 1){
      for (int j = 0; j < numCols; j += 1){
        states[i][j] = grid.getState(j, i);
      }
    }
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }
}
