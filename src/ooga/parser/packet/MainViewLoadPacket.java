package ooga.parser.packet;

public class MainViewLoadPacket extends Packet{
  private String filePath;

  public MainViewLoadPacket(String filePath){
    this.filePath = filePath;
  }

  /**
   * @return the file path of the input File
   */
  public String getFilePath() {
    return filePath;
  }
}
