package ooga.parser;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import ooga.parser.packet.MainViewLoadPacket;
import ooga.parser.packet.Packet;
import ooga.parser.packet.SIMPacket;
import ooga.parser.simrules.AuthorSIMRule;
import ooga.parser.simrules.GameTypeSIMRule;
import ooga.parser.simrules.InitialStateSIMRule;
import ooga.parser.simrules.PlayerNamesSIMRule;
import ooga.parser.simrules.PlayerTurnSIMRule;
import ooga.parser.simrules.PlayerTypesSIMRule;
import ooga.parser.simrules.SIMRule;
import ooga.error.ParserException;
import ooga.resources.Config;

public class SIMParser implements Parser {

  private Map<String, SIMRule> simRuleKeyMap;
  private SIMPacket simPacket;

  public SIMParser() {
    simRuleKeyMap = new HashMap<>() {{
      put(Config.SIMPARSER_GAME_TYPE_KEY, new GameTypeSIMRule());
      put(Config.SIMPARSER_AUTHOR_KEY, new AuthorSIMRule());
      put(Config.SIMPARSER_PLAYER_NAMES, new PlayerNamesSIMRule());
      put(Config.SIMPARSER_PLAYER_TYPES, new PlayerTypesSIMRule());
      put(Config.SIMPARSER_PLAYER_TURN, new PlayerTurnSIMRule());
      put(Config.SIMPARSER_INITIAL_STATE, new InitialStateSIMRule());
    }};
  }

  private SIMRule getParserKey(String key) throws Exception {
    if (simRuleKeyMap.containsKey(key)) {
      return simRuleKeyMap.get(key);
    }
    throw new ParserException(key, null, Config.SIMPARSER_INVALID_KEY);
  }

  /**
   * Given a Packet object, we obtain the file path of the .sim file. We then scan the .sim file
   * evaluate line by line
   *
   * @param packet is a Packet object passed from the MainView loading screen
   * @throws Exception throws a ParserException to the MainView
   */
  @Override
  public void loadFile(Packet packet) throws Exception {
    MainViewLoadPacket mainViewLoadPacket = (MainViewLoadPacket) packet;
    String filePath = mainViewLoadPacket.getFilePath();
    SIMPacket simPacket = new SIMPacket();

    checkFileType(filePath, Config.SIMFILE_TYPE, Config.INVALID_FILE);

    File file = new File(filePath);
    Scanner in = new Scanner(file);
    while (in.hasNextLine()) {
      String nextLine = in.nextLine();
      String key = nextLine.split(Config.SIMPARSER_KEY_VALUE_REGEX)[0];
      simPacket.addKey(key);
      getParserKey(key).evaluateLoad(nextLine, simPacket);
    }
    checkSIMKeys(simPacket);

    // Set sim packet
    setSimPacket(simPacket);
  }

  private void checkSIMKeys(SIMPacket simPacket) throws Exception {
    Set<String> differenceKeys = new HashSet<>(simPacket.getKeys());
    Set<String> missingKeys = new HashSet<>(Config.SIMPARSER_REQUIRED_KEYS);
    for (String key : differenceKeys) {
      if (!missingKeys.contains(key)) {
        throw new ParserException(key, null, Config.SIMPARSER_INVALID_KEY);
      }
      missingKeys.remove(key);
    }
    if (!missingKeys.isEmpty()) {
      throw new ParserException(missingKeys.toString(), null, Config.SIMPARSER_KEYS_MISSING);
    }
  }

  /**
   * Given a SIMPacket, we convert it to a .sim file and save it via FileWriter
   *
   * @param packet is a Packet object passed from the GameController
   * @throws Exception throws a ParserException to the GameView
   */
  @Override
  public void saveFile(Packet packet) throws Exception {
    SIMPacket simPacket = (SIMPacket) packet;
    String filePath = simPacket.getInitialState() + Config.SIMFILE_TYPE;

    FileWriter fileWriter = new FileWriter(filePath);
    for (String key : simPacket.getKeys()) {
      String line = getParserKey(key).evaluateSave(key, simPacket);
      fileWriter.write(line);
    }
    fileWriter.close();

    // Set simPacket
    setSimPacket(null);
  }

  /**
   * Setter for the generated SIMPacket
   *
   * @param simPacket
   */
  public void setSimPacket(SIMPacket simPacket) {
    this.simPacket = simPacket;
  }

  /**
   * Getter for the generated SIMPacket
   *
   * @return SIMPacket object
   */
  public SIMPacket getSimPacket() {
    return simPacket;
  }
}
