package ooga.parser;

import ooga.parser.packet.Packet;
import ooga.error.ParserException;

public interface Parser {

  // Function to load file given a string filepath
  void loadFile(Packet packet) throws Exception;

  // Function to save file given a string filepath
  void saveFile(Packet packet) throws Exception;

  // check file type
  default void checkFileType(String filePath, String fileType, String fileInvalidException) throws Exception {
    if (!filePath.endsWith(fileType)) {
      throw new ParserException(filePath, fileType, fileInvalidException);
    }
  }
}
