package ooga.parser.simrules;

import java.util.Arrays;
import java.util.List;
import ooga.parser.packet.SIMPacket;
import ooga.error.ParserException;
import ooga.resources.Config;

public interface SIMRule {

  /**
   * Method to add an array (row of elements) into the CSVPacket
   *
   * @param value     is a String object taken from the sim file
   * @param simPacket is a SIMPacket object that stores information of the SIM file
   */
  void addValueLoad(String value, SIMPacket simPacket);

  /**
   * Returns the associate value to the key in SIMPacket and return it as a String
   *
   * @param key       is a String key in the SIM file
   * @param simPacket is the SIMPacket where we find the value to return
   * @return a String to save
   */
  String convertValueSave(String key, SIMPacket simPacket);

  /**
   * Checks the validity of the key-value of the given line
   *
   * @param key   is a String key in the SIM file
   * @param value is a String value in the SIM file
   * @throws Exception is thrown if the given key value pair is not valid
   */
  void checkValidityLoad(String key, String value) throws Exception;

  /**
   * Given a line, we split the line into a key and value pair. Then we call two functions to check
   * validity and then adds the associate information into the SIMPacket
   *
   * @param line      is the String line
   * @param simPacket is the SIMPacket where we store the information
   * @throws Exception is thrown when Exception percolates up from checkValidityLoad
   */
  default void evaluateLoad(String line, SIMPacket simPacket) throws Exception {
    String key = line.split(Config.SIMPARSER_KEY_VALUE_REGEX)[0];
    String value = line.split(Config.SIMPARSER_KEY_VALUE_REGEX)[1];

    checkValidityLoad(key, value);
    addValueLoad(value, simPacket);
  }

  /**
   * Converts a key and associated value in the SIMPacket to a string to save into a sim file
   *
   * @param key       is a String key in the SIM file
   * @param simPacket is the SIMPacket where we store the information
   * @return a String
   */
  default String evaluateSave(String key, SIMPacket simPacket) {
    return convertValueSave(key, simPacket) + Config.NEWLINE;
  }

  /**
   * Checks to see if the value is empty (null)
   *
   * @param key   is a String key in the SIM file
   * @param value is a String value in the SIM file
   * @throws Exception thrown when value is null
   */
  default void isEmpty(String key, String value) throws Exception {
    if (value.isEmpty()) {
      throw new ParserException(key, null, Config.SIMPARSER_KEY_EMPTY_VALUE);
    }
  }

  /**
   * checks to see if the given list has the value passed through
   *
   * @param key   is a String key in the SIM file
   * @param value is a String value in the SIM file
   * @param list  is a list of string values
   * @throws Exception thrown if list does not contain the value
   */
  default void doesStringListContain(String key, String value, List<String> list) throws Exception {
    if (!list.contains(value)) {
      throw new ParserException(key, value, Config.SIMPARSER_INVALID_VALUE);
    }
  }

  /**
   * Checks if the number of players is equal to the other values in the sim file
   *
   * @param value is a String value in the SIM file
   * @throws Exception thrown when number of players is not correct
   */
  default void checkNumPlayers(String value) throws Exception {
    List<String> list = Arrays.stream(value.split(Config.SIMPARSER_LIST_REGEX)).toList();
    if (list.size() != Config.NUM_PLAYERS) {
      throw new ParserException(String.valueOf(Config.NUM_PLAYERS), String.valueOf(list.size()),
          Config.SIMPARSER_NUM_PLAYERS_INVALID);
    }
  }

  /**
   * checks if the elements in the list are empty
   *
   * @param key   is a String key in the SIM file
   * @param value is a String value in the SIM file
   * @throws Exception thrown if element is null
   */
  default void checkListElements(String key, String value) throws Exception {
    List<String> list = Arrays.stream(value.split(Config.SIMPARSER_LIST_REGEX)).toList();
    for (String element : list) {
      isEmpty(key, element);
    }
  }
}
