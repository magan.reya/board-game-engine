package ooga.parser.simrules;

import ooga.parser.packet.SIMPacket;
import ooga.resources.Config;

public class AuthorSIMRule implements SIMRule {

  /**
   *
   * @param value     is a String object taken from the sim file
   * @param simPacket is a SIMPacket object that stores information of the SIM file
   */
  @Override
  public void addValueLoad(String value, SIMPacket simPacket) {
    simPacket.setAuthor(value);
  }

  /**
   *
   * @param key       is a String key in the SIM file
   * @param simPacket is the SIMPacket where we find the value to return
   * @return a String for saving
   */
  @Override
  public String convertValueSave(String key, SIMPacket simPacket) {
    return String.join(Config.SIMPARSER_KEY_VALUE_REGEX, key, simPacket.getAuthor());
  }

  /**
   *
   * @param key   is a String key in the SIM file
   * @param value is a String value in the SIM file
   * @throws Exception thrown when invalid
   */
  @Override
  public void checkValidityLoad(String key, String value) throws Exception {
    isEmpty(key, value);
  }
}
