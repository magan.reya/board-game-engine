package ooga.parser.simrules;

import java.util.Arrays;
import java.util.List;
import ooga.parser.packet.SIMPacket;
import ooga.resources.Config;

public class PlayerNamesSIMRule implements SIMRule {

  /**
   *
   * @param value     is a String object taken from the sim file
   * @param simPacket is a SIMPacket object that stores information of the SIM file
   */
  @Override
  public void addValueLoad(String value, SIMPacket simPacket) {
    List<String> playerNames = Arrays.asList(value.split(Config.SIMPARSER_LIST_REGEX));
    simPacket.setPlayerNames(playerNames);
  }

  /**
   *
   * @param key       is a String key in the SIM file
   * @param simPacket is the SIMPacket where we find the value to return
   * @return
   */
  @Override
  public String convertValueSave(String key, SIMPacket simPacket) {
    List<String> list = simPacket.getPlayerNames();
    String valueJoined = String.join(Config.SIMPARSER_LIST_REGEX, list);
    return String.join(Config.SIMPARSER_KEY_VALUE_REGEX, key, valueJoined);
  }

  /**
   *
   * @param key   is a String key in the SIM file
   * @param value is a String value in the SIM file
   * @throws Exception
   */
  @Override
  public void checkValidityLoad(String key, String value) throws Exception {
    isEmpty(key, value);
    checkNumPlayers(value);
    checkListElements(key, value);
  }
}
