package ooga.parser.simrules;

import java.io.File;
import java.util.Scanner;
import ooga.parser.packet.SIMPacket;
import ooga.error.ParserException;
import ooga.resources.Config;

public class InitialStateSIMRule implements SIMRule {

  /**
   *
   * @param value     is a String object taken from the sim file
   * @param simPacket is a SIMPacket object that stores information of the SIM file
   */
  @Override
  public void addValueLoad(String value, SIMPacket simPacket) {
    simPacket.setInitialState(value);
  }

  /**
   *
   * @param key       is a String key in the SIM file
   * @param simPacket is the SIMPacket where we find the value to return
   * @return
   */
  @Override
  public String convertValueSave(String key, SIMPacket simPacket) {
    return String.join(Config.SIMPARSER_KEY_VALUE_REGEX, key, simPacket.getInitialState() + Config.CSVFILE_TYPE);
  }

  /**
   *
   * @param key   is a String key in the SIM file
   * @param value is a String value in the SIM file
   * @throws Exception
   */
  @Override
  public void checkValidityLoad(String key, String value) throws Exception {
    checkValidFile(key, value);
  }

  /**
   *
   * @param key
   * @param value
   * @throws Exception
   */
  public void checkValidFile(String key, String value) throws Exception {
    try {
      File file = new File(value);
      Scanner in = new Scanner(file);
    } catch (Exception e) {
      throw new ParserException(key, value, Config.SIMPARSER_MISSING_FILE);
    }
  }
}
