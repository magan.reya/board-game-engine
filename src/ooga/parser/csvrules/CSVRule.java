package ooga.parser.csvrules;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import ooga.parser.packet.CSVPacket;
import ooga.error.ParserException;
import ooga.resources.Config;

public interface CSVRule {

  /**
   * Method to add an array (row of elements) into the CSVPacket
   *
   * @param elements  is a String[] array of elements
   * @param csvPacket is a CSVPacket object that stores information of the CSV file
   */
  void addValueLoad(String[] elements, CSVPacket csvPacket);

  /**
   * Given an array, check if it is valid per the rules of the specific line
   *
   * @param elements  is a String array of elements
   * @param csvPacket is the packet where we store the elements if it is valid
   * @throws Exception throws an error message to the front end
   */
  void checkValidityLoad(String[] elements, CSVPacket csvPacket) throws Exception;

  /**
   * Given a line in the CSV file, we first checks the validity of the line given. If it is valid,
   * then add it to the CSV packet.
   *
   * @param line
   * @param csvPacket
   * @throws Exception
   */
  default void evaluateLoad(String line, CSVPacket csvPacket) throws Exception {
    String[] elements = line.split(Config.SIMPARSER_LIST_REGEX);
    checkValidityLoad(elements, csvPacket);
    addValueLoad(elements, csvPacket);
  }

  /**
   * Wrapper to return a convertValueSave(elements) joined into a string
   *
   * @param elements is each row in the Grid
   * @return a String of the joined array
   */
  default String evaluateSave(int[] elements) {
    return convertValueSave(elements);
  }

  /**
   * Given a int[] elements, join it all together into a string line
   *
   * @param elements is each row in the Grid
   * @return a String of the joined array
   */
  default String convertValueSave(int[] elements) {
    return IntStream.of(elements)
        .mapToObj(Integer::toString)
        .collect(Collectors.joining(Config.SIMPARSER_LIST_REGEX))
        + Config.NEWLINE;
  }

  /**
   * Given an array, checks to see if it has a size of 0, if so, throw a ParserException
   *
   * @param elements is an String[] array, which is made by splitting a line in the CSV file
   * @throws Exception (ParserException) of an empty line to the front end
   */
  default void isEmpty(String[] elements) throws Exception {
    if (elements.length == 0 || elements[0].isEmpty()) {
      throw new ParserException(null, null, Config.CSVPARSER_EMPTY_LINE);
    }
  }

  /**
   * Given an array, checks to see if all elements can be converted into a number
   *
   * @param elements is an String[] array, which is made by splitting a line in the CSV file
   * @throws Exception (ParserException) thrown when the element is non numeric or a null element
   */
  default void isNumeric(String[] elements) throws Exception {
    for (String element : elements) {
      try {
        int i = Integer.parseInt(element);
      } catch (Exception e) {
        if (element.isEmpty()) {
          throw new ParserException(Config.EMPTY_NULL_STRING, null, Config.CSVPARSER_NON_NUMERIC);
        } else {
          throw new ParserException(element, null, Config.CSVPARSER_NON_NUMERIC);
        }
      }
    }
  }
}
