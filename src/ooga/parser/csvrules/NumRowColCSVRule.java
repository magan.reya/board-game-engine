package ooga.parser.csvrules;

import ooga.parser.packet.CSVPacket;
import ooga.error.ParserException;
import ooga.resources.Config;

public class NumRowColCSVRule implements CSVRule {

  /**
   * Since this row has two elements, number of rows and number of columns. We then set the
   * csvPacket numRows and numCols to the correct states.
   *
   * @param elements  is a String[] array of elements
   * @param csvPacket is a CSVPacket object that stores information of the CSV file
   */
  @Override
  public void addValueLoad(String[] elements, CSVPacket csvPacket) {
    int numRows = Integer.parseInt(elements[0]);
    int numCols = Integer.parseInt(elements[1]);
    csvPacket.setNumRows(numRows);
    csvPacket.setNumCols(numCols);
  }

  /**
   * Checks four conditions: 1. array must not be empty 2. checks number of elements in the array 3.
   * checks to see of all elements are numeric 4. checks to see if any of the elements is zero
   *
   * @param elements  is a String array of elements
   * @param csvPacket is the packet where we store the elements if it is valid
   * @throws Exception
   */
  @Override
  public void checkValidityLoad(String[] elements, CSVPacket csvPacket) throws Exception {
    isEmpty(elements); // check null case first
    checkDefinedNumRowCol(elements); // check whether we got an actual row col
    isNumeric(elements); // then check if the strings are numbers
    checkZeroElements(elements);
  }

  /**
   * Makes sure non of the elements is zero
   *
   * @param elements is an String[] array
   * @throws Exception to thrown when elements are zero
   */
  public void checkZeroElements(String[] elements) throws Exception {
    for (String element : elements) {
      int i = Integer.parseInt(element);
      if (i == 0) {
        throw new ParserException(String.join(Config.SIMPARSER_LIST_REGEX, elements), null,
            Config.CSVPARSER_ZERO_ELEMENTS);
      }
    }
  }

  /**
   * Checks to see if the number of elements is Config.CSVPARSER_NUMBER_ROW_COLS (2)
   *
   * @param elements is an String[] array
   * @throws Exception to thrown when elements are not equal to Config.CSVPARSER_NUMBER_ROW_COLS
   */
  public void checkDefinedNumRowCol(String[] elements) throws Exception {
    if (elements.length != Config.CSVPARSER_NUMBER_ROW_COLS) {
      throw new ParserException(
          String.valueOf(Config.CSVPARSER_NUMBER_ROW_COLS),
          String.valueOf(elements.length),
          Config.CSVPARSER_INVALID_NUM_ELEMENTS);
    }
  }
}
