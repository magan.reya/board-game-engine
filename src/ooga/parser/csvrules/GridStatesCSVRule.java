package ooga.parser.csvrules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import ooga.parser.packet.CSVPacket;
import ooga.error.ParserException;
import ooga.resources.Config;

public class GridStatesCSVRule implements CSVRule {

  /**
   * Converts each element to an int and add the entire row into the CSVPacket
   *
   * @param elements  is a String[] array of elements
   * @param csvPacket is a CSVPacket object that stores information of the CSV file
   */
  @Override
  public void addValueLoad(String[] elements, CSVPacket csvPacket) {
    int[] row = new int[elements.length];
    for (int i = 0; i < row.length; i += 1) {
      row[i] = Integer.parseInt(elements[i]);
    }
    csvPacket.addToStates(row);
  }

  /**
   * Checks four conditions: 1. array must not be empty 2. checks number of elements in the array 3.
   * checks to see of all elements are numeric 4. checks to see if any of the elements is zero
   *
   * @param elements  is a String array of elements
   * @param csvPacket is the packet where we store the elements if it is valid
   * @throws Exception is thrown if an ParserException is thrown
   */
  @Override
  public void checkValidityLoad(String[] elements, CSVPacket csvPacket) throws Exception {
    isEmpty(elements); // check null case first
    isNumeric(elements); // then check if the strings are numbers
    checkCorrectColumnDimensions(elements, csvPacket);
    checkValidStates(elements);
  }

  /**
   * Checks to see if the number of columns in the given array is equal to the intialized number of
   * columns
   *
   * @param elements  is a String array of elements
   * @param csvPacket is the packet in which we parser to see if the number of columns is valid
   * @throws Exception is thrown if number of columns doesn't correspond to the length of elements
   */
  public void checkCorrectColumnDimensions(String[] elements, CSVPacket csvPacket)
      throws Exception {
    if (csvPacket.getNumCols() != elements.length) {
      throw new ParserException(
          String.valueOf(csvPacket.getNumCols()),
          String.valueOf(elements.length),
          Config.CSVPARSER_INVALID_NUM_ELEMENTS);
    }
  }

  /**
   * Given elements, check to see if the elements are 0, 1, 2.
   *
   * @param elements is a String array of elements
   * @throws Exception is thrown if given element is not a valid state
   */
  public void checkValidStates(String[] elements) throws Exception {
    for (String element : elements) {
      if (!Config.CSVPARSER_VALID_GRID_STATES.contains(element)) {
        List<String> sortedOrder = new ArrayList<>(Config.CSVPARSER_VALID_GRID_STATES);
        Collections.sort(sortedOrder);
        throw new ParserException(sortedOrder.toString(), element,
            Config.CSVPARSER_NON_VALID_STATE_ELEMENT);
      }
    }
  }
}
