package ooga.view;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import ooga.controller.PropertyObservable;
import ooga.resources.Config;
import ooga.view.viewAPI.CellViewAPI;

public class CellView extends PropertyObservable
        implements PropertyChangeListener, CellViewAPI {

  private Circle piece;
  private Rectangle cell;
  private double cellWidth, cellHeight;
  private double circleRadius;
  private double x, y;

  //Constructor for the CellView object containing the Rectangle Cell object and Circle Piece object
  public CellView(int rectWidth, int rectHeight, int row, int col, String ID) {
    cellWidth = rectWidth;
    cellHeight = rectHeight;
    circleRadius = Math.min(rectHeight, rectWidth) / 2;
    x = Config.GRIDVIEW_X_START + col * (rectWidth + Config.GRIDVIEW_GAP);
    y = Config.GRIDVIEW_Y_START + row * (rectHeight + Config.GRIDVIEW_GAP);
    createRectangle(ID);
  }

  //Creates the rectangle that represents an individual cell in the grid
  private void createRectangle(String ID) {
    cell = new Rectangle(x, y, cellWidth, cellHeight);
    cell.setFill(Color.GREY);
    cell.setStroke(Color.BLACK);
    cell.setId(ID);
  }

  /**
   * Creates a piece object at the specified location in question
   *
   * @param c the color of the Circle used to represent the Piece object
   * @param ID the id of the Circle being created
   */
  public void updateCircle(Color c, String ID) {
    piece = new Circle(circleRadius);
    piece.setCenterX(x + circleRadius);
    piece.setCenterY(y + circleRadius);
    piece.setFill(c);
    piece.setStroke(Color.BLACK);
    piece.setId(ID);
  }

  /**
   * Gets the Rectangle used to represent a Cell object
   *
   * @return the Rectangle object representing the Cell
   */
  public Rectangle getRectangle() {
    return cell;
  }

  /**
   * Gets the Circle used to represent a Piece object
   *
   * @return the Circle object representing the Piece
   */
  public Circle getCircle() {
    return piece;
  }

  /**
   * Makes a cell clickable and displays to the user that it is clickable with a color change
   *
   * @param column the column of the Cell in the gridView
   * @param row the row of the Cell in the gridView
   */
  public void cellClicked(int column, int row) {
    cell.setOnMouseClicked(event -> {
      boardClicked(column, row);
    });
    cell.setFill(Color.LIGHTGREEN);
  }

  /**
   * Makes a piece clickable and displays to the user that it is clickable with a color change
   *
   * @param column the column of the Piece in the gridView
   * @param row the row of the Piece in the gridView
   */
  public void circleClickable(int column, int row) {
    piece.setOnMouseClicked(event ->  {
      pieceClicked(column, row);
    });
    cell.setFill(Color.LIGHTBLUE);
  }

  /**
   * Handles the situation where a piece is clicked
   *
   * @param column the column of the Piece in the gridView
   * @param row the row of the Piece in the gridView
   */
  public void pieceClicked(int column, int row) {
    notifyObserver(Config.GRID_VIEW_CLICK_PIECE, new Point(column, row));
  }

  /**
   * Passes observer to the model to see if this is a valid point
   *
   * @param row the row of the CellView in the GridView that was clicked
   * @param col the column of the CellView in the GridView that was clicked
   */
  public void boardClicked(int col, int row) {
    notifyObserver(Config.GRID_VIEW_ADD_PIECE, new Point(col, row));
  }

  /**
   * Clears the clickable feature of a given Cell once it is no longer used
   */
  public void clearCellClick() {
    cell.setOnMouseClicked(event -> {
      return;
    });
    cell.setFill(Color.GREY);
  }

  /**
   * Removes the piece at the given cell location
   */
  public void removeCircle() {
    piece = null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    //does nothing here, since this should not accept any propertyChanges
  }
}
