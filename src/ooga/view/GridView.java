package ooga.view;

import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import ooga.controller.PropertyObservable;
import ooga.reflection.ReflectionFactory;
import ooga.resources.Config;
import ooga.view.viewAPI.GridViewAPI;

import static ooga.Main.LOGGER;

public class GridView extends PropertyObservable
        implements PropertyChangeListener, GridViewAPI {

  private Map<Point, CellView> cellViews;
  private Group cells;
  private int numRows, numColumns;
  private PropertyChangeListener listener;
  private ReflectionFactory reflection;

  //Constructor for the GridView instance
  public GridView() {
    cellViews = new HashMap<>();
    cells = new Group();
    listener = this;
    reflection = new ReflectionFactory();
  }

  /**
   * Sets the number of columns for the Grid
   *
   * @param columns the number of columns
   */
  public void setColumns(int columns) {
    numColumns = columns;
  }

  /**
   * Sets the number of rows for the Grid
   *
   * @param rows the number of rows
   */
  public void setRows(int rows) {
    numRows = rows;
  }

  /**
   * Sets the pieces of a given state to be clickable
   *
   * @param state the ID of the pieces that should be made clickable
   */
  public void setPieceClicks(int state) {
    for(Point p : cellViews.keySet()) {
      CellView cell = cellViews.get(p);
      if(cell.getCircle() != null && cell.getCircle().getFill().equals(Config.GAME_CONTROLLER_PIECE_COLORS.get(state - 1))) {
        cell.circleClickable(p.x, p.y);
      }
    }
  }

  /**
   * Sets the cells of all possible moves for a given game to be clickable
   *
   * @param possibleMoves the list of points to set as possible to place a piece at
   */
  public void setPossibleMoves(List<Point> possibleMoves) {
    for(Point p : possibleMoves) {
      cellViews.get(p).cellClicked(p.x, p.y);
    }
  }

  /**
   * Sets up the initial values of each CellView present in our Grid
   *
   * @param row    the row of the Grid with the specified CellView
   * @param column the column of the Grid with the specified CellView
   * @param state  the initial state of the specified CellView
   */
  public void setCellView(int row, int column, int state) {
    int rectWidth = Config.GRIDVIEW_WIDTH / numRows - Config.GRIDVIEW_GAP;
    int rectHeight = Config.GRIDVIEW_HEIGHT / numColumns - Config.GRIDVIEW_GAP;
    CellView cell = new CellView(rectWidth, rectHeight, row, column, Integer.toString(numRows * row + column));
    cell.addObserver(listener);
    cellViews.put(new Point(column, row), cell);
    cells.getChildren().add(cell.getRectangle());
    setPiece(row, column, state);
  }

  /**
   * Sets up the piece by adding a Circle object to the Cell depending on whether it's necessary
   *
   * @param row    the row of the Grid with the specified CellView
   * @param column the column of the Grid with the specified CellView
   * @param state  the initial state of the specified CellView
   */
  public void setPiece(int row, int column, int state) {
    Group cells = getGrid();
    Map<Point, CellView> cellViews = getMapOfCellViews();
    if(state != 0) {
      CellView cell = cellViews.get(new Point(column, row));
      if (state < 0){
        state = -1 * state;
      LOGGER.info("New King piece");
      }
      cell.updateCircle(Config.GAME_CONTROLLER_PIECE_COLORS.get(state - 1), Integer.toString((numRows * numColumns) + (numRows * row + column)));
      cells.getChildren().add(cell.getCircle());
    }
  }

  @Deprecated
  public void setBackground(){}

  @Deprecated
  public void displayCurrentGrid(List<Point> possibleMoves){}

  @Deprecated
  public boolean lightUpPossibleCells(List<Point> possibleMoves) {
    for(Point p : possibleMoves) {

    }
    return false;
  }

  @Deprecated
  /**
   * Creates the piece at each point in the Grid group
   *
   * @param p     Point indicating which Cell to update with a piece
   * @param state the state indicating the index for the color of the cell that should be displayed
   */
  public void updatePiece(Point p, int state) {
    cellViews.get(p).updateCircle(Config.GAME_CONTROLLER_PIECE_COLORS.get(state - 1), Integer.toString((numRows * numColumns) + (numRows * p.y + p.x)));
    cells.getChildren().add(cellViews.get(p).getCircle());
  }

  /**
   * Gets the grid Group object
   *
   * @return Group representing all CellViews in our GridView object
   */
  public Group getGrid() {
    return cells;
  }

  /**
   * Returns the map of Points corresponding to CellViews in the Grid
   */
  protected Map<Point, CellView> getMapOfCellViews() {
    return cellViews;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    Class<?> currClass = GridView.class;
    reflection.getNonClassReflection(Config.REFLECTION_FACTORY_PROPERTY_REFLECTION, currClass,
        evt.getPropertyName(), this, evt);
  }

  //Sends propertyChange notifying the gameView that a cellView in the grid was updated
  private void gridViewAddPiece(PropertyChangeEvent evt) {
    notifyObserver(Config.GAME_VIEW_ADD_PIECE, evt.getNewValue());
  }

  //Handles the event where a piece is clicked
  private void gridViewClickPiece(PropertyChangeEvent evt) {
    for(Point p : cellViews.keySet()) {
      CellView cell = cellViews.get(p);
      if(cell.getRectangle().getFill().equals(Color.LIGHTGREEN)) {
        cell.clearCellClick();
      }
    }
    notifyObserver(Config.GAME_VIEW_CLICK_PIECE, evt.getNewValue());
  }
}