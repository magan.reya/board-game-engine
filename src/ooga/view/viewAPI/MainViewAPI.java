package ooga.view.viewAPI;

import java.beans.PropertyChangeListener;
import javafx.scene.Parent;
import javafx.scene.Scene;
import ooga.error.ParserException;

public interface MainViewAPI {

    /**
     * Changes the Root to a different Scene
     *
     * @param display the root with the Scene elements to change
     */
    void updateToGameScene(Parent display);

    void setGameKeyPress(PropertyChangeListener pcl);

    void updateToMainScene();

    Scene setUpMain(int width, int height);

    void errorDisplay(String message);

    void parserErrorConversion(ParserException e);
}