package ooga.view.viewAPI;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.File;
import java.util.List;

public interface ViewElementsAPI {

    MenuBar makeMenuBar(String text,  String id);

    File makeFileChooser(String description, String extensions);

    Label makeLabel(String text);

    Button makeButton(String label, EventHandler<ActionEvent> handler);

    Alert createAlertDialog(String title, String message, Alert.AlertType alertType);

    HBox makeHBox(List<Node> children, int spacing);

    VBox makeVBox(List<Node> children, int spacing);

    TextInputDialog makeTextInputDialog(String title, String header, String text);
}
