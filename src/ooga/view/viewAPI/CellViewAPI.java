package ooga.view.viewAPI;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public interface CellViewAPI {

    Rectangle getRectangle();

    void updateCircle(Color c, String ID);

    Circle getCircle();

    void cellClicked(int row, int column);

    void circleClickable(int column, int row);

    void pieceClicked(int column, int row);

    void boardClicked(int row, int col);

    void clearCellClick();

    void removeCircle();
}
