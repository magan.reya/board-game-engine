package ooga.view.viewAPI;

import javafx.scene.Group;

import java.awt.*;
import java.util.List;

public interface GridViewAPI {

    void setColumns(int columns);

    void setRows(int rows);

    void setCellView(int row, int column, int state);

    void setPiece(int row, int column, int state);

    void setPieceClicks(int state);

    void setPossibleMoves(List<Point> possibleMoves);

    @Deprecated
    void updatePiece(Point p, int state);

    Group getGrid();
}
