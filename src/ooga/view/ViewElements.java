package ooga.view;

import java.io.File;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import ooga.view.viewAPI.ViewElementsAPI;

public class ViewElements implements ViewElementsAPI {

  /**
   * Generates a MenuBar object
   *
   * @param text the text that the MenuBar will display
   * @return MenuBar object
   */
  public MenuBar makeMenuBar(String text, String id) {
    MenuBar menuBar = new MenuBar();
    Menu m = new Menu(text);
    menuBar.getMenus().add(m);
    menuBar.setId(id);
    return menuBar;
  }

  /**
   * Makes the menuItems that will be placed into the MenuBars
   * @param name the name of the menuItem
   * @param id the ID of the menuItem
   * @param actionEvent the action event that will be executed when that menuItem is clicked
   * @return a MenuItem object to be placed in a MenuBar
   */
  public MenuItem makeMenuItems(String name, String id, EventHandler actionEvent) {
    MenuItem menuItem = new MenuItem(name);
    menuItem.setId(id);
    menuItem.setOnAction(actionEvent);
    return menuItem;
  }

  /**
   * Generates a UI that allows the user to choose a file and gives that file
   * @param description the description as to which files can be selected
   * @param extensions  the extension that will be passed in the fileChooser to only allow for certain file types
   * @return File indicating the selected file by the user
   */
  public File makeFileChooser(String description, String extensions) {
    FileChooser myFileChooser = new FileChooser();
    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(description,
        extensions);
    myFileChooser.getExtensionFilters().add(extFilter);
    File selectedFile = myFileChooser.showOpenDialog(null);
    return selectedFile;
  }

  /**
   * Makes the save dialog for a fileChooser object when a certain state needs to be saved into a file
   */
  public File makeSaveFileChooser() {
    FileChooser myFileChooser = new FileChooser();
    return myFileChooser.showSaveDialog(null);
  }

  /**
   * Generates a Label object
   * @param text the text that the Label will display
   * @return Label object representing the label in question
   */
  public Label makeLabel(String text) {
    Label label = new Label(text);
    // label.setFont(Font.font("Arial", FontWeight.BOLD, size));
    label.setWrapText(true);
    label.setId(text);
    return label;
  }

//  // Creates a text field input
//  public TextField makeTextField(String name) {
//    TextField textField = new TextField(name);
//    textField.setId(name);
//    return textField;
//  }

  /**
   * This creates a button given a string and action
   * @param label name of the button
   * @param handler the handler that executes when the button is pressed
   * @return the Button with the specified action
   */
  public Button makeButton(String label, EventHandler<ActionEvent> handler) {
    Button result = new Button();
    result.setId(label);
    result.setText(label);
    result.setOnAction(handler);
    return result;
  }

  /**
   * Creates an Error Dialog message
   * @param title the title of the error dialog
   * @param message the message that this error dialog is representing
   * @param alertType the type of alert that is generated
   * @return the Alert Dialog representing the error in question
   */
  public Alert createAlertDialog(String title, String message, AlertType alertType) {
    Alert alert = new Alert(alertType);
    alert.setTitle(title);
    alert.setContentText(message);
    return alert;
  }

  /**
   * Sets the spacing and the children of an inputted HBox
   * @param children the children to add to the HBox
   * @param spacing  the spacing between elements
   * @return the HBox representing the given elements
   */
  public HBox makeHBox(List<Node> children, int spacing) {
    HBox result = new HBox();
    for (Node n : children) {
      result.getChildren().add(n);
    }
     result.setAlignment(Pos.CENTER);
     result.setSpacing(spacing);
    return result;
  }

  /**
   * Sets the spacing and the children of an inputted VBox
   * @param children the children to add to the VBox
   * @param spacing  the spacing between elements
   * @return the VBox representing the given elements
   */
  public VBox makeVBox(List<Node> children, int spacing) {
    VBox result = new VBox();
    for (Node n : children) {
      result.getChildren().add(n);
    }
    result.setAlignment(Pos.CENTER);
    result.setSpacing(spacing);
    return result;
  }

  /**
   * Creates an input dialog with a text field
   * @param title the title of the dialog box
   * @param header the header of the dialog box
   * @param text the text next to the text field
   * @return the TextInputDialog representing a requested text input from a dialog box
   */
  public TextInputDialog makeTextInputDialog(String title, String header, String text) {
    TextInputDialog dialog = new TextInputDialog();
    dialog.setTitle(title);
    dialog.setHeaderText(header);
    dialog.getDialogPane().setContentText(text);
    dialog.showAndWait();
    return dialog;
  }
}
