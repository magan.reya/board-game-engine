package ooga.view.game;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;
import javafx.animation.Animation.Status;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import ooga.components.componentAPI.ImmutableGrid;
import ooga.players.Player;
import ooga.resources.Config;

public class CPUCPUGameView extends GameView {

  private Timeline myAnimation;
  private HBox panel;
  private Button startAnimationButton;
  private Button pauseAnimationButton;
  private Button stepAnimationButton;

  public CPUCPUGameView(String gameType, String language, String mode, ImmutableGrid grid,
      Player currPlayer, List<Player> players) throws Exception {
    super(gameType, language, mode, grid, currPlayer, players);
    makePanel();
  }

  @Override
  protected void gameViewAddPieceHelper(Player currPlayer, Point point) {
    // Nothing happens
  }

  /**
   * Sets up the Scene object (display) for the game view
   *
   * @return Node
   */
  @Override
  public Node setUpGameView() {
    super.setUpGameView();

    VBox bottomPanel = new VBox();
    bottomPanel.getChildren().addAll(panel, getBottomPanel());
    getPane().setBottom(bottomPanel);

    return getPane();
  }


  /**
   * Creates the animation panel within the game view (start animation, step, and pause animation)
   */
  private void makePanel() {
    panel = null;

    startAnimationButton = getViewElements().makeButton(
        getMyResources().getString(Config.GAMEVIEW_START_ANIMATION_BUTTON_LABEL),
        e -> startAnimation());

    stepAnimationButton = getViewElements().makeButton(
        getMyResources().getString(Config.GAMEVIEW_STEP_ANIMATION_BUTTON_LABEL),
        e -> stepAnimation());

    pauseAnimationButton = getViewElements().makeButton(
        getMyResources().getString(Config.GAMEVIEW_PAUSE_ANIMATION_BUTTON_LABEL),
        e -> pauseAnimation());

    panel = getViewElements().makeHBox(
        Arrays.asList(startAnimationButton, stepAnimationButton, pauseAnimationButton), 20);
    panel.getStyleClass().add(Config.GAMEVIEW_ANIMATION_PANEL_ID);
    panel.setId(Config.GAMEVIEW_ANIMATION_PANEL_ID);
  }

  private void setUpAnimation() {
    myAnimation = new Timeline();
    myAnimation.setCycleCount(Timeline.INDEFINITE);
    myAnimation.getKeyFrames().add(
        new KeyFrame(Duration.seconds(Config.GAMEVIEW_ANIMATION_SPEED_DEFAULT), e -> step()));
    startAnimationButton.setText(
        getMyResources().getString(Config.GAMEVIEW_RESUME_ANIMATION_BUTTON_LABEL));
  }

  private void startAnimation() {
    if (myAnimation == null) {
      setUpAnimation();
    }
    if (isAnimationRunning()) {
      errorDisplay(getMyResources().getString(Config.ANIMATION_RUNNING_MESSAGE));
    }
    myAnimation.play();
  }

  private void stepAnimation() {
    if (isAnimationRunning()) {
      errorDisplay(getMyResources().getString(Config.ANIMATION_RUNNING_MESSAGE));
    }
    step();
  }

  private void step() {
    if (myAnimation == null){
      setUpAnimation();
    }
    if (getIsWin()) {
      myAnimation.stop();
    }
    List<Object> packet = List.of(getCurrPlayer(), new Point(-1, -1));
    notifyObserver(Config.GAME_CONTROLLER_STEP, packet);
  }

  private void pauseAnimation() {
    if (myAnimation == null) {
      errorDisplay(getMyResources().getString(Config.ANIMATION_NOT_STARTED));
      return;
    }
    if (!isAnimationRunning()) {
      errorDisplay(getMyResources().getString(Config.ANIMATION_NOT_RUNNING_MESSAGE));
      return;
    }
    myAnimation.stop();
  }

  private boolean isAnimationRunning() {
    if (myAnimation == null) return false;
    return myAnimation.getStatus() == Status.RUNNING;
  }
}
