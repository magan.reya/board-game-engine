package ooga.view.game;

import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.ResourceBundle;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import ooga.components.componentAPI.ImmutableGrid;
import ooga.controller.PropertyObservable;
import ooga.players.Player;
import ooga.reflection.ReflectionFactory;
import ooga.reflection.ReflectionSetUp;
import ooga.resources.Config;
import ooga.view.ViewElements;
import ooga.view.game.gameviewAPI.GameViewAPI;
import ooga.view.GridView;

public abstract class GameView extends PropertyObservable
    implements PropertyChangeListener, GameViewAPI {

  private ResourceBundle myResources;
  private ViewElements viewElements;
  private GridView gridView;
  private ReflectionFactory reflection;
  private ReflectionSetUp setUp;
  private GameViewBottomPanel gameViewBottomPanel;
  private GameViewTopPanel gameViewTopPanel;
  private Scene scene;
  private BorderPane pane;
  private boolean isWin;

  /**
   * Constructor
   *
   * @return
   */
  public GameView(String gameType, String language, String mode, ImmutableGrid grid,
      Player currPlayer, List<Player> players) {
    myResources = ResourceBundle.getBundle(
        String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
    this.viewElements = new ViewElements();
    this.reflection = new ReflectionFactory();
    this.setUp = new ReflectionSetUp();
    this.isWin = false;

    this.gridView = new GridView();
    gridView.addObserver(this);
    setGridViewValues(grid);

    this.gameViewBottomPanel = new GameViewBottomPanel(viewElements, myResources);
    gameViewBottomPanel.addObserver(this);

    this.gameViewTopPanel = new GameViewTopPanel(viewElements, gameType, currPlayer, players);
    gameViewTopPanel.addObserver(this);
  }

  public GridView getGridView() {
    return gridView;
  }

  /**
   * Sets up the GameView panel by adding the top, bottom, and center Nodes.
   */
  public Node setUpGameView() {
    pane = new BorderPane();
    pane.setTop(gameViewTopPanel.getPanel());
    pane.setCenter(gridView.getGrid());
    pane.setBottom(gameViewBottomPanel.getPanel());
    pane.getStyleClass().add(Config.GAMEVIEW_ID);
    return pane;
  }

  protected abstract void gameViewAddPieceHelper(Player currPlayer, Point point);

  /**
   * Returns a scene object for the MainController to show
   *
   * @param width  is the width of the screen
   * @param height is the height of the screen
   * @return a Scene object
   */
  public Scene getGameViewScene(int width, int height) {
    scene = new Scene(pane, width, height);
    scene.getStylesheets().add(getClass().getResource(
        String.format(Config.DEFAULT_PATH_STYLESHEET, Config.DEFAULT_STYLESHEET)).toExternalForm());
    return scene;
  }

  /**
   * Returns the root of the Scene
   * @return Root indicating the root of the scene in question
   */
  public Parent getRoot() {
    return pane;
  }

  /**
   * Getters
   */
  protected HBox getBottomPanel() {
    return gameViewBottomPanel.getPanel();
  }

  protected BorderPane getPane() {
    return pane;
  }

  protected Player getCurrPlayer() {
    return gameViewTopPanel.getCurrPlayer();
  }

  protected ResourceBundle getMyResources() {
    return myResources;
  }

  protected ViewElements getViewElements() {
    return viewElements;
  }

  /**
   * Returns whether or not the winning state has been achieved or not
   *
   * @return a boolean for winning
   */
  public boolean getIsWin() {
    return isWin;
  }

  /**
   * Given a state (number), set all elements in the board with the state as clickable for Checkers.
   * This is required for clicking available pieces in Checkers.
   *
   * @param state is an int from 0 to 2
   */
  public void setPieceClicks(int state) {
    gridView.setPieceClicks(state);
  }

  /**
   * Given a list of points from the Model of possible moves, we set all the points in the Grid that
   * is possible to be moeved to as clickable.
   *
   * @param possibleMoves is a list of Point objects given from the Model
   */
  public void addPossibleMoves(List<Point> possibleMoves) {
    gridView.setPossibleMoves(possibleMoves);
  }

  /**
   * Given a Grid object from a model, sets all the grid cells in the view to either have a piece or
   * not.
   *
   * @param grid is an ImmutableGrid object from the model
   */
  public void setGridViewValues(ImmutableGrid grid) {
    gridView.setRows(grid.getNumRows());
    gridView.setColumns(grid.getNumCols());
    for (int i = 0; i < grid.getNumRows(); i++) {
      for (int j = 0; j < grid.getNumCols(); j++) {
        gridView.setCellView(i, j, grid.getState(j, i));
      }
    }
  }

  /**
   * Displays the error Alert when an exception is caught.
   *
   * @param message is the String to show in the Alert
   */
  public void errorDisplay(String message) {
    Alert alert = viewElements.createAlertDialog(Config.ALERT_MESSAGE, message,
        Alert.AlertType.ERROR);
    alert.show();
  }

  /**
   * Updates the player turn by setting the currPlayer passed in to the gameViewTopPanel.update
   *
   * @param currPlayer
   */
  public void updatePlayerTurn(Player currPlayer) {
    gameViewTopPanel.update(currPlayer);
  }

  /**
   * Given a Player, show a won game dialog for them if they have won.
   *
   * @param winPlayer
   */
  public void showWin(Player winPlayer) {
    isWin = true;
    String winMessage = String.format(myResources.getString(Config.WIN_MESSAGE),
        winPlayer.getName());
    Alert alert = viewElements.createAlertDialog(winMessage, winMessage, AlertType.CONFIRMATION);
    ImageView checkImage = new ImageView(Config.CHECK_IMAGE);
    checkImage.setFitHeight(Config.CHECK_IMAGE_DIM);
    checkImage.setPreserveRatio(true);
    alert.getDialogPane().setGraphic(checkImage);
    alert.show();
  }

  /*************************** Everything below is Observable/Observer pipeline **************/
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    Class<?> currCLass = GameView.class;
    reflection.getNonClassReflection(Config.REFLECTION_FACTORY_PROPERTY_REFLECTION, currCLass,
        evt.getPropertyName(), this, evt);
  }

  /**
   * Add piece pipeline Helper function is required for different gametypes (Human vs. Human) (CPU
   * vs. CPU) (Human vs. CPU)
   */
  private void gameViewAddPiece(PropertyChangeEvent evt) {
    gameViewAddPieceHelper(gameViewTopPanel.getCurrPlayer(), (Point) evt.getNewValue());
  }

  private void gameViewClickPiece(PropertyChangeEvent evt) {
    notifyObserver(Config.GAME_CONTROLLER_CLICK_PIECE, evt.getNewValue());
  }

  private void gameViewSaveClicked(PropertyChangeEvent evt) {
    if (evt.getNewValue() == null) {
      return;
    }
    notifyObserver(Config.GAME_CONTROLLER_SAVE_CLICKED, evt.getNewValue());
  }

  private void gameViewBackClicked(PropertyChangeEvent evt) {
    notifyObserver(Config.GAME_CONTROLLER_BACK_BUTTON_CLICKED, evt.getNewValue());
  }
}
