package ooga.view.game;

import java.util.Arrays;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import ooga.controller.PropertyObservable;
import ooga.players.Player;
import ooga.resources.Config;
import ooga.view.ViewElements;

public class GameViewTopPanel extends PropertyObservable {

  private ViewElements viewElements;
  private HBox panel;
  private Node gameTypeLabel;
  private Node playerTurnLabel;
  private GridPane scoreBoard;
  private List<Player> players;
  private Player currPlayer;

  public GameViewTopPanel(ViewElements viewElements, String gameType, Player currPlayer, List<Player> players) {
    this.viewElements = viewElements;
    this.currPlayer = currPlayer;
    this.players = players;

    makePanel(gameType);
  }

  /**
   * Getter
   *
   * @returns the panel
   */
  public HBox getPanel() {
    return panel;
  }

  /**
   * Creates the top panel within the game view, (Game name, player's turn, scoreboard)
   */
  private void makePanel(String gameType) {
    gameTypeLabel = viewElements.makeLabel(gameType);
    updatePanelElements();

    panel = viewElements.makeHBox(
        Arrays.asList(gameTypeLabel, playerTurnLabel, scoreBoard), 20);
    panel.getStyleClass().add(Config.GAMEVIEW_TOP_PANEL_ID);
    panel.setId(Config.GAMEVIEW_TOP_PANEL_ID);
  }

  /**
   * Creating a scoreboard
   */
  private void makeScoreBoard() {
    scoreBoard = new GridPane();
    for (int i = 0; i < players.size(); i += 1) {
      Player player = players.get(i);
      Node playerLabel = viewElements.makeLabel(player.getName());
      Node playerScore = viewElements.makeLabel(String.valueOf(player.getScore()));
      scoreBoard.addRow(i, playerLabel, playerScore);
    }
    scoreBoard.getStyleClass().add(Config.GAMEVIEW_SCOREBOARD_PANEL_ID);
    scoreBoard.setId(Config.GAMEVIEW_SCOREBOARD_PANEL_ID);
  }

  private void updatePanelElements() {
    playerTurnLabel = viewElements.makeLabel(currPlayer.getName());
    makeScoreBoard();
  }

  /**
   * Updates a player turn and updates the view
   */
  public void update(Player currPlayer) {
    this.currPlayer = currPlayer;

    updatePanelElements();
    panel.getChildren().clear();
    panel.getChildren().addAll(Arrays.asList(gameTypeLabel, playerTurnLabel, scoreBoard));
  }

  /**
   * Returns the current player object
   * @return a Player object
   */
  public Player getCurrPlayer(){
    return currPlayer;
  }
}
