package ooga.view.game.gameviewAPI;

import javafx.scene.Node;
import javafx.scene.Scene;
import ooga.components.componentAPI.ImmutableGrid;
import ooga.players.Player;
import ooga.view.GridView;

import java.awt.*;
import java.util.List;

public interface GameViewAPI {

    Node setUpGameView();

    Scene getGameViewScene(int width, int height);

    void errorDisplay(String message);

    void updatePlayerTurn(Player currPlayer);

    void showWin(Player winPlayer);

    GridView getGridView();

    boolean getIsWin();

    void setPieceClicks(int state);

    void addPossibleMoves(List<Point> possibleMoves);

    void setGridViewValues(ImmutableGrid grid);
}
