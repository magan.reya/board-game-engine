package ooga.view.game;

import java.awt.Point;
import java.util.List;
import ooga.components.componentAPI.ImmutableGrid;
import ooga.players.Player;
import ooga.resources.Config;

public class HumanHumanGameView extends GameView {

  public HumanHumanGameView(String gameType, String language, String mode, ImmutableGrid grid,
      Player currPlayer, List<Player> players) {
    super(gameType, language, mode, grid, currPlayer, players);
  }

  @Override
  protected void gameViewAddPieceHelper(Player currPlayer, Point point) {
    List<Object> packet = List.of(currPlayer, point);
    notifyObserver(Config.GAME_CONTROLLER_STEP, packet);
  }
}
