package ooga.view.game;

import java.io.File;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import ooga.controller.PropertyObservable;
import ooga.resources.Config;
import ooga.view.ViewElements;

public class GameViewBottomPanel extends PropertyObservable {

  private HBox panel;
  private Button saveFileButton;
  private Button backButton;
  private ViewElements viewElements;
  private ResourceBundle myResources;

  public GameViewBottomPanel(ViewElements viewElements, ResourceBundle myResources) {
    this.viewElements = viewElements;
    this.myResources = myResources;
    makePanel();
  }

  /**
   * Getter
   * @returns the panel
   */
  public HBox getPanel() {
    return panel;
  }

  /**
   * Creates the bottom panel within the game view (Save file and back buttons)
   */
  private void makePanel() {
    panel = null;
    saveFileButton = viewElements.makeButton(myResources.getString(Config.GAMEVIEW_SAVE_FILE_BUTTON_LABEL),
        e -> saveButtonPress());

    backButton = viewElements.makeButton(myResources.getString(Config.GAMEVIEW_BACK_BUTTON_LABEL),
        e -> backButtonPress());

    panel = viewElements.makeHBox(Arrays.asList(saveFileButton, backButton), 20);
    panel.getStyleClass().add(Config.GAMEVIEW_BOTTOM_PANEL_ID);
    panel.setId(Config.GAMEVIEW_BOTTOM_PANEL_ID);
  }

  private void saveButtonPress() {
    File file = viewElements.makeSaveFileChooser();
    if (file == null) {
      return;
    }
    notifyObserver(Config.GAMEVIEW_SAVE_CLICKED, file);
  }

  private void backButtonPress() {
    notifyObserver(Config.GAMEVIEW_BACK_BUTTON_CLICKED, Config.CONTROLLER_PROMPTS.get(3));
  }
}
