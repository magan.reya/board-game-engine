package ooga.view.game;

import java.awt.Point;
import java.util.List;
import ooga.components.componentAPI.ImmutableGrid;
import ooga.players.Player;
import ooga.resources.Config;

public class HumanCPUGameView extends GameView {

  public HumanCPUGameView(String gameType, String language, String mode, ImmutableGrid grid,
      Player currPlayer, List<Player> players) {
    super(gameType, language, mode, grid, currPlayer, players);
  }

  /**
   * In Human vs. CPU, Human Player is run first and a step function is called for that.
   * Then, CPUPlayer makes a move with an invalid Point, as the Model will handle finding an optimal point.
   * @param currPlayer
   * @param point
   */
  @Override
  protected void gameViewAddPieceHelper(Player currPlayer, Point point) {
    int currPlayerId = currPlayer.getID();
    // Human step is run
    List<Object> humanPacket = List.of(currPlayer, point);
    notifyObserver(Config.GAME_CONTROLLER_STEP, humanPacket);

    Player updatedCurrPlayer = getCurrPlayer();
    int nextPlayerId = updatedCurrPlayer.getID();

    if (currPlayerId != nextPlayerId) {
      // CPU step, passing Point == -1,-1 because CPUPlayer has not point click
      List<Object> cpuPacket = List.of(getCurrPlayer(), new Point(-1, -1));
      notifyObserver(Config.GAME_CONTROLLER_STEP, cpuPacket);
    }
  }

}
