package ooga.view.keyInputs;

import java.beans.PropertyChangeListener;
import java.io.File;
import javafx.scene.input.KeyCode;
import ooga.controller.PropertyObservable;
import ooga.reflection.ReflectionFactory;
import ooga.resources.Config;
import ooga.view.ViewElements;

public class KeyInputGame extends PropertyObservable implements KeyInput {

    private ReflectionFactory reflection;
    private ViewElements viewElements;

    //constructor for the keys that are allowed to be clicked for cheating purposes specific to GameView
    public KeyInputGame() {
        reflection = new ReflectionFactory();
        viewElements = new ViewElements();
    }

    /**
     * The method that calls reflection on the keys that can be pressed for GameView and calls the correct method to execute its function
     * @param keyCode the KeyCode indicating the key that has been pressed
     * @param pcl the PropertyChangeListener that was used to send updates to that specified class object/instance
     */
    public void keyPressed(KeyCode keyCode, PropertyChangeListener pcl) {
        if (!Config.GAME_KEYS.contains(keyCode.toString())) return;

        addObserver(pcl);
        reflection.getNonClassReflection(Config.REFLECTION_FACTORY_METHOD_REFLECTION,
                getClass(), Config.GET_KEY_PRESSES_GAME.get(Config.GAME_KEYS.indexOf(keyCode.toString())), this, null);
    }

    //handles the save game key press, saving the game at that instantaneous moment without a button press
    private void saveGame() {
        notifyObserver(Config.GAMEVIEW_SAVE_CLICKED, viewElements.makeSaveFileChooser());
    }

    //handles the back to home key press, going back to mainView at that instantaneous moment without a button press
    private void backToHome() {
        notifyObserver(Config.GAMEVIEW_BACK_BUTTON_CLICKED, Config.CONTROLLER_PROMPTS.get(3));
    }

    //handles the creation of an optimal move display for a human to determine which position is best for them to place a specified piece
    private void optimalNextMove() {
        notifyObserver(Config.GET_KEY_PRESSES_GAME.get(2), null);
    }
}
