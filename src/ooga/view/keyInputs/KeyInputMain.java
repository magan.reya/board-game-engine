package ooga.view.keyInputs;

import javafx.scene.input.KeyCode;
import ooga.controller.PropertyObservable;
import ooga.reflection.ReflectionFactory;
import ooga.resources.Config;
import ooga.view.MainView;

import java.beans.PropertyChangeListener;
import java.util.List;

public class KeyInputMain extends PropertyObservable implements KeyInput {

    private ReflectionFactory reflection;

    //constructor for the keys that are allowed to be clicked for cheating purposes specific to MainView
    public KeyInputMain() {
        reflection = new ReflectionFactory();
    }

    /**
     * The method that calls reflection on the keys that can be pressed for MainView and calls the correct method to execute its function
     * @param keyCode the KeyCode indicating the key that has been pressed
     * @param pcl the PropertyChangeListener that was used to send updates to that specified class object/instance
     */
    public void keyPressed(KeyCode keyCode, PropertyChangeListener pcl) {
        if (!Config.MAIN_KEYS.contains(keyCode.toString())) return;

        addObserver(pcl);
        reflection.getNonClassReflection(Config.REFLECTION_FACTORY_METHOD_REFLECTION,
                getClass(), Config.GET_KEY_PRESSES_MAIN.get(Config.MAIN_KEYS.indexOf(keyCode.toString())), this, null);
    }

    //handles the event that creates a generic CPU vs CPU Gomoku game without setting up the game manually
    private void CPUGomoku() {
        notifyObserver(Config.DEFAULT_GAME_SETUP, List.of("Gomoku", "CvC"));
    }

    //handles the event that creates a generic CPU vs CPU ConnectFour game without setting up the game manually
    private void CPUConnectFour() {
        notifyObserver(Config.DEFAULT_GAME_SETUP, List.of("ConnectFour", "CvC"));
    }

    //handles the event that creates a generic CPU vs CPU Checkers game without setting up the game manually
    private void CPUCheckers() {
        notifyObserver(Config.DEFAULT_GAME_SETUP, List.of("Checkers", "CvC"));
    }

    //handles the event that creates a generic CPU vs CPU Othello game without setting up the game manually
    private void CPUOthello() {
        notifyObserver(Config.DEFAULT_GAME_SETUP, List.of("Othello", "CvC"));
    }

    //handles the event that creates a generic Human vs CPU Gomoku game without setting up the game manually
    private void humanCPUGomoku() {
        notifyObserver(Config.DEFAULT_GAME_SETUP, List.of("Gomoku", "PvC"));
    }

    //handles the event that creates a generic Human vs CPU ConnectFour game without setting up the game manually
    private void humanCPUConnectFour() {
        notifyObserver(Config.DEFAULT_GAME_SETUP, List.of("ConnectFour", "PvC"));
    }

    //handles the event that creates a generic Human vs CPU Checkers game without setting up the game manually
    private void humanCPUCheckers() {
        notifyObserver(Config.DEFAULT_GAME_SETUP, List.of("Checkers", "PvC"));
    }

    //handles the event that creates a generic Human vs CPU Othello game without setting up the game manually
    private void humanCPUOthello() {
        notifyObserver(Config.DEFAULT_GAME_SETUP, List.of("Othello", "PvC"));
    }

    //these have to be specified files
    private void checkersKingGame() {
        notifyObserver(Config.GET_KEY_PRESSES_MAIN.get(8), null);
    }

    //these have to be specified files
    private void stalemateCondition() {
        notifyObserver(Config.GET_KEY_PRESSES_MAIN.get(9), null);
    }
}
