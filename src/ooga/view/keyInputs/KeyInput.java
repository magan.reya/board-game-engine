package ooga.view.keyInputs;

import javafx.scene.input.KeyCode;

import java.beans.PropertyChangeListener;

public interface KeyInput {
    void keyPressed(KeyCode keyCode, PropertyChangeListener pcl);
}
