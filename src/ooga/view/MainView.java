package ooga.view;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import ooga.controller.PropertyObservable;
import ooga.error.ParserException;
import ooga.reflection.ReflectionFactory;
import ooga.resources.Config;
import ooga.view.keyInputs.KeyInput;
import ooga.view.keyInputs.KeyInputGame;
import ooga.view.keyInputs.KeyInputMain;
import ooga.view.viewAPI.MainViewAPI;

import static ooga.Main.LOGGER;

public class MainView extends PropertyObservable implements MainViewAPI, PropertyChangeListener {

  private Scene myScene;
  private ViewElements elements;
  private ReflectionFactory reflection;
  private String language, color, game, matchUp;
  private HBox menuBar;
  private HBox menuButtons;
  private VBox result;
  private List<Button> buttons;
  private List<MenuBar> menuBars;
  private List<String> names, gameMode;
  private ResourceBundle languageResources;
  private Class<?> currClass;
  private KeyInput codes;

  //constructor for the MainView
  public MainView() {
    elements = new ViewElements();
    reflection = new ReflectionFactory();
    language = Config.LANGUAGES.get(Config.ZERO);
    color = Config.COLORS.get(Config.ONE);
    game = Config.EMPTY_STRING;
    matchUp = Config.EMPTY_STRING;
    currClass = MainView.class;
    languageResources = ResourceBundle.getBundle(
        String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
    menuBars = new ArrayList<>();
    buttons = new ArrayList<>();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    reflection.getNonClassReflection(Config.REFLECTION_FACTORY_PROPERTY_REFLECTION,
        getClass(), evt.getPropertyName(), this, evt);
  }

  //handles the event where the game is set up for cheat keys that start specified games (with specified types and modes)
  private void defaultGameSetUp(PropertyChangeEvent evt) {
    List<String> values = (List<String>) evt.getNewValue();
    game = values.get(0);
    matchUp = values.get(1);
    buttons.get(0).fire();
  }

  /**
   * Changes the Root to a different Scene
   *
   * @param display the root with the Scene elements to change
   */
  public void updateToGameScene(Parent display) {
    myScene.setRoot(display);
  }

  /**
   * IF we have our scene to be that of the GameView, we make the button presses associated with GameView specific key presses
   *
   * @param pcl the PropertyChangeListener object to send each key press to
   */
  public void setGameKeyPress(PropertyChangeListener pcl) {
    codes = new KeyInputGame();
    myScene.setOnKeyPressed(e -> codes.keyPressed(e.getCode(), pcl));
  }

  /**
   * Changes the Root to the MainView Scene
   */
  public void updateToMainScene() {
    myScene.setRoot(result);
    codes = new KeyInputMain();
    myScene.setOnKeyPressed(e -> codes.keyPressed(e.getCode(), this));
  }

  /**
   * sets up the MainView scene with specified dimensions for the window size
   *
   * @param width  the width of the scene
   * @param height the height of the scene
   * @return the Scene representing the main screen
   */
  public Scene setUpMain(int width, int height) {
    Node welcomeMessage = makeWelcomeText();
    List<Node> createElements = createMenuBars();
    menuBar = elements.makeHBox(createElements, Config.HBOX_SPACING);
    List<Node> mainElements = createButtons();
    menuButtons = elements.makeHBox(mainElements, Config.HBOX_SPACING);
    List<Node> menuElements = List.of(welcomeMessage, menuBar, menuButtons);
    result = elements.makeVBox(menuElements, Config.VBOX_SPACING);
    myScene = new Scene(result, width, height);
    myScene.getStylesheets().add(getClass().getResource(
            String.format(Config.DEFAULT_PATH_STYLESHEET, Config.DEFAULT_STYLESHEET)).toExternalForm());
    myScene.getStylesheets().add(getClass().getResource(
            String.format(Config.DEFAULT_PATH_STYLESHEET, String.format(Config.CSS_ENDING, color)))
        .toExternalForm());
    codes = new KeyInputMain();
    myScene.setOnKeyPressed(e -> codes.keyPressed(e.getCode(), this));
    return myScene;
  }

  //creates the welcome text on the top of the screen to show the user what our program does
  private Node makeWelcomeText() {
    Label welcome = elements.makeLabel(Config.WELCOME_MESSAGE);
    welcome.setMaxWidth(Config.WELCOME_SIZE);
    welcome.setWrapText(true);
    welcome.setAlignment(Pos.CENTER);
    return welcome;
  }

  //makes the dropdown menuBars with the different options for each menubar
  private List<Node> createMenuBars() {
    List<Node> createPanel = new ArrayList<>();
    addMenuBars(Config.ZERO, Config.MAIN_VIEW_MENUBARS.size(), createPanel);
    return createPanel;
  }

  //makes the button prompts to input values from the menuBars or do other related actions
  private List<Node> createButtons() {
    Button createButton = elements.makeButton(
        languageResources.getString(Config.BUTTON_NAMES.get(Config.ZERO)), event -> {
          try {
            sendNewData();
            clear();
          } catch (Exception e) {
            errorDisplay(e.getMessage());
          }
        });
    Button loadButton = elements.makeButton(
        languageResources.getString(Config.BUTTON_NAMES.get(Config.ONE)),
        event -> selectFile());
    List<Node> buttons = List.of(createButton, loadButton);
    addButtons(buttons);
    return buttons;
  }

  //the adding of the buttons to our list of buttons to allow for modification of the text of each button based on its language
  private void addButtons(List<Node> bottomButtons) {
    for (Node b : bottomButtons) {
      buttons.add((Button) b);
    }
  }

  //sets up the menuBars for each respective menuBar that is created from either the load or create panel
  private void addMenuBars(int start, int end, List<Node> panel) {
    for (int i = start; i < end; i++) {
      MenuBar menuBar = elements.makeMenuBar(
          languageResources.getString(Config.MAIN_VIEW_MENUBARS.get(i)),
          Config.MAIN_VIEW_MENUBARS.get(i));
      for (String s : Config.MENUBARS.get(i)) {
        String temp = Config.MAIN_VIEW_VARIABLES.get(i);
        menuBar.getMenus().get(Config.ZERO).getItems()
            .add(elements.makeMenuItems(languageResources.getString(s), s,
                event -> {
                  reflection.getNonClassReflection(Config.REFLECTION_FACTORY_FIELD_REFLECTION,
                      currClass, temp, this, s);
                  handleType(temp);
                }));
      }
      panel.add(menuBar);
      menuBars.add(menuBar);
    }
  }

  //determines which method to call depending on whether the menuItem selected is a language item or a color item
  private void handleType(String value) {
    if (Config.MAIN_VIEW_VARIABLES.indexOf(value) >= Config.NUMBER_OF_GAME_MENUBARS) {
      reflection.getNonClassReflection(Config.REFLECTION_FACTORY_METHOD_REFLECTION, currClass,
          Config.STYLE_METHODS.get(Config.MAIN_VIEW_VARIABLES.indexOf(value) -
              Config.NUMBER_OF_GAME_MENUBARS), this, Config.EMPTY_STRING);
    }
  }

  //updates the colors of the scene depending on the color option selected
  private void updateSceneColors() {
    myScene.getStylesheets().clear();
    myScene.getStylesheets().add(getClass().getResource(
        String.format(Config.DEFAULT_PATH_STYLESHEET, Config.DEFAULT_STYLESHEET)).toExternalForm());
    myScene.getStylesheets().add(getClass().getResource(
            String.format(Config.DEFAULT_PATH_STYLESHEET, String.format(Config.CSS_ENDING, color)))
        .toExternalForm());
  }

  //updates the language of all View elements depending on the language that was selected
  private void changeLanguages() {
    languageResources = ResourceBundle.getBundle(
        String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
    for (int i = Config.ZERO; i < buttons.size(); i++) {
      buttons.get(i).setText(languageResources.getString(Config.BUTTON_NAMES.get(i)));
    }
    for (int i = Config.ZERO; i < menuBars.size(); i++) {
      menuBars.get(i).getMenus().get(Config.ZERO)
          .setText(languageResources.getString(Config.MAIN_VIEW_MENUBARS.get(i)));
      for (int j = Config.ZERO; j < menuBars.get(i).getMenus().get(Config.ZERO).getItems().size();
          j++) {
        menuBars.get(i).getMenus().get(Config.ZERO).getItems().get(j)
            .setText(languageResources.getString(Config.MENUBARS.get(i).get(j)));
      }
    }
  }

  //sends the new data to the controller when creating a new Game
  void sendNewData() throws Exception {
    if (!game.equals(Config.EMPTY_STRING) && !matchUp.equals(Config.EMPTY_STRING)) {
      namePrompts();
      gameMode = setUpGameMode();
      LOGGER.info(String.format("Entered %s with gameMode=%s", game, gameMode.toString()));
      notifyObserver(Config.CREATE, List.of(List.of(game, language, color), names, gameMode));
    } else {
      throw new Exception(Config.NEW_DATA_EXCEPTION);
    }
  }

  //Sets up the mode by distinguishing what types of players are being generated to play the game in question
  private List<String> setUpGameMode() {
    String[] arr = matchUp.split(Config.SPLIT_STRING);
    return List.of(arr[Config.ZERO], arr[Config.ONE]);
  }

  //returns a list of the names that are generated from the input data fields
  List<String> getNames() {
    return names;
  }

  //Sets up the dialog boxes to be used when creating a new game depending on the mode of the players
  private void namePrompts() {
    try {
      Method action = MainView.class.getDeclaredMethod(
          Config.GAME_MODE_METHODS.get(Config.MODES.indexOf(matchUp)));
      action.invoke(this);
    } catch (NullPointerException e) {
      errorDisplay(e.getMessage());
    } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
      errorDisplay(Config.CONTROLLER_PROPERTIES);
    }
  }

  //sets up the names of the players when they are both humans
  private void handlePlayers() {
    names = new ArrayList<>();
    names.add(nameInput(
        elements.makeTextInputDialog(languageResources.getString(Config.TITLES.get(Config.ZERO)),
                languageResources.getString(Config.HEADERS.get(Config.ZERO)),
                languageResources.getString(Config.CONTEXT_TEXT)).getEditor().getCharacters()
            .toString()));
    names.add(nameInput(
        elements.makeTextInputDialog(languageResources.getString(Config.TITLES.get(Config.ONE)),
                languageResources.getString(Config.HEADERS.get(Config.ONE)),
                languageResources.getString(Config.CONTEXT_TEXT)).getEditor().getCharacters()
            .toString()));
  }

  //sets up the names of the players when there is one human and one computer
  private void handlePlayerAndComputer() {
    names = new ArrayList<>();
    names.add(nameInput(
        elements.makeTextInputDialog(languageResources.getString(Config.TITLES.get(Config.TWO)),
                languageResources.getString(Config.HEADERS.get(Config.TWO)),
                languageResources.getString(Config.CONTEXT_TEXT)).getEditor().getCharacters()
            .toString()));
    names.add(Config.CPUS.get(Config.TWO));
  }

  //sets up the names of the players when they are both computers
  private void handleComputers() {
    names = new ArrayList<>();
    names.add(Config.CPUS.get(Config.ZERO));
    names.add(Config.CPUS.get(Config.ONE));
  }

  //Takes the input from the dialog name boxes and determines whether there was an inputted value or not (or closed out of the dialog box)
  private String nameInput(String input) throws NullPointerException {
    if (input.equals(Config.EMPTY_STRING)) {
      throw new NullPointerException(Config.NULL_NAME_INPUT);
    } else {
      return input;
    }
  }

  //clears the instance variables when error is called (to reset) or if a game is created to allow for another new game
  private void clear() {
    for (int i = Config.ZERO; i < Config.NUMBER_OF_GAME_MENUBARS; i++) {
      reflection.getNonClassReflection(Config.REFLECTION_FACTORY_FIELD_REFLECTION,
          currClass, Config.MAIN_VIEW_VARIABLES.get(i), this, Config.EMPTY_STRING);
    }
  }

  //allows for the independent selection of a file from a file chooser when pressing the load file button
  private void selectFile() {
    File selectedFile = elements.makeFileChooser(Config.FILE_CHOOSER_SIM, Config.SIM_EXTENSION);
    fileInput(selectedFile);
  }

  //Takes the file input from the FileChooser as a SIM and sends a propertyChange based on the button that was clicked
  void fileInput(File selectedFile) {
    try {
      String absoluteFilePath = selectedFile.getPath();
      notifyObserver(Config.CONTROLLER_PROMPTS.get(Config.ONE), List.of(absoluteFilePath, language, color));
    } catch (Exception e) {
      errorDisplay(Config.SIMPARSER_INVALID_VALUE);
    }
  }

  /**
   * Displays the error message for a given exception that is thrown
   * @param message the message to be displayed in the error dialog box
   */
  public void errorDisplay(String message) {
    Alert alert = elements.createAlertDialog(Config.ALERT_MESSAGE, message, Alert.AlertType.ERROR);
    alert.show();
  }

  /**
   * Converts a ParserException object error message to a string value to be passed into errorDisplay()
   * @param e is a ParserException object, which is a wrapper around an Exception object
   */
  public void parserErrorConversion(ParserException e) {
    String message;
    if (e.getKey() == null && e.getValue() == null) {
      message = languageResources.getString(e.getMessage());
    } else if (e.getValue() == null) {
      message = String.format(languageResources.getString(e.getMessage()), e.getKey());
    } else {
      message = String.format(languageResources.getString(e.getMessage()), e.getKey(), e.getValue());
    }
    errorDisplay(message);
  }

}