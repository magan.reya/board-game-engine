package ooga.controller;

import static ooga.Main.LOGGER;

import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import ooga.controller.controllerAPI.GameControllerAPI;
import ooga.model.CheckersModel;
import ooga.model.Model;
import ooga.parser.CSVParser;
import ooga.parser.SIMParser;
import ooga.parser.packet.CSVPacket;
import ooga.parser.packet.MainViewLoadPacket;
import ooga.parser.packet.Packet;
import ooga.parser.packet.SIMPacket;
import ooga.players.Player;
import ooga.reflection.ReflectionFactory;
import ooga.reflection.ReflectionSetUp;
import ooga.resources.Config;
import ooga.view.game.CPUCPUGameView;
import ooga.view.game.GameView;

public class GameController extends PropertyObservable
    implements PropertyChangeListener, GameControllerAPI {

  private GameView gameView;
  private Model model;
  private ReflectionFactory reflection;
  private ReflectionSetUp setUp;
  private CSVParser csvParser;
  private SIMParser simParser;
  private List<Player> players;
  private SIMPacket simPacket;
  private CSVPacket csvPacket;
  private int currentPlayerIndex;

  public GameController(
      String filePath, // FilePath to be parsed
      String language,
      String mode,
      PropertyChangeListener listener
  ) {
    this.addObserver(listener);
    if (!setUpParsersAndPackets(filePath)) {
      return;
    }

    // Set player turn index
    currentPlayerIndex = simPacket.getPlayerNames().indexOf(simPacket.getPlayerTurn());

    setUpPlayerModelView(language, mode,
        simPacket.getPlayerNames(), simPacket.getPlayerTypes(), csvPacket.getStates());
  }

  public GameController(
      String gameType, // game type
      String language, // language (English, French, Spanish)
      String mode, // Light/Dark
      List<String> playerNames, // List of 2 values
      List<String> playerTypes, // Human, CPU
      PropertyChangeListener listener
  ) {
    this.addObserver(listener);

    String defaultBoardFilePath = String.format(Config.DEFAULT_BOARD_FILEPATH,
        gameType.toLowerCase());
    if (!setUpParsersAndPackets(defaultBoardFilePath)) {
      return;
    }

    simPacket.setPlayerNames(playerNames);
    simPacket.setPlayerTypes(playerTypes);

    // Change playerTypes
    List<String> playerTypesModified = new ArrayList<>();
    for (String type : playerTypes) {
      String playerType = (type.equals(Config.PLAYER_TYPE)) ? Config.HUMAN_TYPE : Config.CPU_TYPE;
      playerTypesModified.add(playerType);
    }

    // Set player turn index
    this.currentPlayerIndex = Config.DEFAULT_PLAYER_INDEX;

    setUpPlayerModelView(language, mode, playerNames, playerTypesModified,
        csvPacket.getStates());
  }

  private boolean setUpParsersAndPackets(String filePath) {
    this.csvParser = new CSVParser();
    this.simParser = new SIMParser();
    try {

      Packet viewPacket = new MainViewLoadPacket(filePath);

      simParser.loadFile(viewPacket);
      simPacket = simParser.getSimPacket();

      csvParser.loadFile(simPacket);
      csvPacket = csvParser.getCSVPacket();

    } catch (Exception e) {
      LOGGER.error(e.toString());
      notifyObserver(Config.MAIN_CONTROLLER_PARSER_EXCEPTION, e);
      return false;
    }
    return true;
  }

  private void setUpPlayerModelView(
      String language, // language (English, French, Spanish)
      String mode, // Light/Dark
      List<String> playerNames, // List of 2 values
      List<String> playerTypes, // Human, CPU
      int[][] states // states of the game
  ) {
    this.players = new ArrayList<>();
    this.reflection = new ReflectionFactory();
    this.setUp = new ReflectionSetUp();

    players = setUp.setUpPlayers(playerNames, playerTypes);
    model = setUp.setUpModel(simPacket.getGameType(), states);
    gameView = setUp.setUpGameView(simPacket.getGameType(), language, mode,
        model.getImmutableGrid(),
        players.get(currentPlayerIndex), players, playerTypes);

    checkGameStatus();

    if (!(gameView instanceof CPUCPUGameView)) {
      evaluateNextSteps();
    }
    gameView.addObserver(this);
    notifyObserver(Config.CONTROLLER_PROMPTS.get(4), gameView);
    notifyObserver(Config.CONTROLLER_PROMPTS.get(2), gameView.setUpGameView());
  }

  private boolean checkGameStatus() {
    if (model.getGameStatus() != 0
        && !gameView.getIsWin()) { // Stop the click if someone has already won
      for (Player p : players) {
        if (p.getID() == model.getGameStatus()) {
          gameView.showWin(p);
        }
      }
      return true;
    }
    return false;
  }

  private void evaluateNextSteps() {
    if (model instanceof CheckersModel) {
      makePiecesClickable();
    } else {
      updatePossibleMoves(null);
    }
  }

  private void makePiecesClickable() {
    gameView.setPieceClicks(currentPlayerIndex + 1);
  }

  private void updatePossibleMoves(Point point) {
    List<Object> infoForPossibleMoves;
    if (model instanceof CheckersModel) {
      infoForPossibleMoves = List.of(currentPlayerIndex + 1, point);
    } else {
      infoForPossibleMoves = List.of(currentPlayerIndex + 1);
    }
    List<Point> possiblePoints = model.getAllPossiblePlaces(infoForPossibleMoves);
    gameView.addPossibleMoves(possiblePoints);
  }

  private void switchPlayer() {
    currentPlayerIndex = (currentPlayerIndex + 1) % players.size();
    if (model.getCurrentPlayerID() < 0) {
      model.setCurrentPlayerID(-(currentPlayerIndex + 1));
    } else {
      model.setCurrentPlayerID(currentPlayerIndex + 1);
    }
    gameView.updatePlayerTurn(players.get(currentPlayerIndex)); // Switch player in the view
    if (!(gameView instanceof CPUCPUGameView)) {
      evaluateNextSteps();
    }
  }

  /**
   * Getters
   */
  public GameView getGameView() {
    return gameView;
  }

  public SIMPacket getSIMPacket() {
    return simPacket;
  }

  /**
   * General step function
   *
   * @param currPlayer is the current Player
   * @param point      is a Point object as to where in the grid the click occurs
   */
  private void step(Player currPlayer, Point point) {
    if (checkGameStatus()) {
      return;
    }

    try {
      model.move(point,
          currPlayer); // Interface with model to add the point selected by current player
      model.incrementScore(players);
    } catch (Exception e) {
      gameView.errorDisplay(e.getMessage());
      LOGGER.error(e.toString());
      return;
    }

    gameView.setGridViewValues(model.getImmutableGrid());

    if (checkGameStatus()) {
      return;
    }

    switchPlayer(); // Switch player in the controller
  }

  /*************************** Everything below is Observable/Observer pipeline **************/
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    Class<?> currClass = GameController.class;
    reflection.getNonClassReflection(Config.REFLECTION_FACTORY_PROPERTY_REFLECTION,
        currClass, evt.getPropertyName(), this, evt);
  }

  private void gameControllerPieceClicked(PropertyChangeEvent evt) {
    Point p = (Point) evt.getNewValue();
    ((CheckersModel) model).setPositionToBeRemoved(p.x, p.y);
    updatePossibleMoves(p);
  }

  private void gameControllerSaveClicked(PropertyChangeEvent evt) {
    saveClicked((File) evt.getNewValue());
  }

  /**
   * Save file pipeline: once save file is clicked in the view, SIMParser and CSVParser will save
   * file
   *
   * @param saveFile is a File object
   */
  public void saveClicked(File saveFile) {
    // Setting up SIMParser packet to be passed
    simPacket.setInitialState(saveFile.getAbsolutePath());
    SIMPacket saveSimPacket = new SIMPacket(Config.SIMPARSER_REQUIRED_KEYS,
        Config.GAMEVIEW_DEFAULT_AUTHOR,
        simPacket.getGameType(),
        saveFile.getAbsolutePath(),
        simPacket.getPlayerNames(),
        simPacket.getPlayerTypes(),
        players.get(currentPlayerIndex).getName());

    // Setting up CSVParser packet to be passed
    CSVPacket saveCSVPacket = new CSVPacket(saveSimPacket.getInitialState(),
        model.getImmutableGrid());

    try {
      simParser.saveFile(saveSimPacket);
      csvParser.saveFile(saveCSVPacket);
    } catch (Exception e) {
      LOGGER.error(e.toString());
      gameView.errorDisplay(e.getMessage());
    }
  }

  /**
   * handles the clicking of the back button to change the scene for the MainController
   */
  private void backButtonClicked(PropertyChangeEvent evt) {
    gameView = null;
    notifyObserver((String) evt.getNewValue(), null);
  }

  /**
   * Calls the next state in the model and display next state in the GameView
   */
  private void gameControllerStep(PropertyChangeEvent evt) throws Exception {
    List<Object> packet = (List<Object>) evt.getNewValue();
    Player currPlayer = (Player) packet.get(0);
    Point point = (Point) packet.get(1);

    step(currPlayer, (point.equals(new Point(-1, -1))) ? null : point);
  }
}
