package ooga.controller;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class PropertyObservable {

  private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

  /**
   * Connects an observable/observer edge between two classes String name is a unique identifier for
   * each observable/observer PropertyChangeListener is a class that acts as a observer
   *
   * @param l is a PropertyChangeListener
   */
  public void addObserver(PropertyChangeListener l) {
    pcs.addPropertyChangeListener(l);
  }

  /**
   * The observable class will notify the observer class, given an addObserver has been set up
   * String name is a unique identifier for each observable/observer Object o is an object/class
   * that is passed from observable to observer
   *
   * @param name is the name of the property change
   * @param o    is the Object to pass
   */
  public void notifyObserver(String name, Object o) {
    pcs.firePropertyChange(name, null, o);
  }
}
