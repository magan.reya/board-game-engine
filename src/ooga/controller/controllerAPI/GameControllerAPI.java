package ooga.controller.controllerAPI;

import java.io.File;
import ooga.parser.packet.SIMPacket;
import ooga.view.game.GameView;

public interface GameControllerAPI {

  GameView getGameView();

  SIMPacket getSIMPacket();

  void saveClicked(File saveFile);
}
