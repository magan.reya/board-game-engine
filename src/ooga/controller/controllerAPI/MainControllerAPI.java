package ooga.controller.controllerAPI;

import javafx.scene.Scene;
import ooga.controller.GameController;
import ooga.view.MainView;

public interface MainControllerAPI {

    MainView start();

    GameController getGameController();
}
