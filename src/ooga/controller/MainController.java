package ooga.controller;

import static ooga.Main.LOGGER;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javafx.scene.Parent;
import ooga.controller.controllerAPI.MainControllerAPI;
import ooga.error.ParserException;
import ooga.reflection.ReflectionFactory;
import ooga.resources.Config;
import ooga.view.MainView;

public class MainController extends PropertyObservable implements PropertyChangeListener, MainControllerAPI {

    private GameController controller;
    private MainView view;
    private ReflectionFactory reflection;

    //constructor for the controller
    public MainController(){
        view = new MainView();
        reflection = new ReflectionFactory();
        view.addObserver(this);
    }

    /**
     * Gets the GameController instance in the MainController
     * @return the GameController instance
     */
    public GameController getGameController() {
        return controller;
    }

    /**
     * Starts the SetUp View instance to allow user input prompts for files
     * @return the MainView representing the main screen
     */
    public MainView start() {
        return view;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        LOGGER.info(evt.getPropertyName());
        Class<?> currClass = MainController.class;
        reflection.getNonClassReflection(Config.REFLECTION_FACTORY_PROPERTY_REFLECTION, currClass,
                    Config.CONTROLLER_METHODS.get(
                            Config.CONTROLLER_PROMPTS.indexOf(evt.getPropertyName())),
                this, evt);
    }

    //handles the event from the MainView that creates a new game (therefore a new Game Controller with the inputted values
    private List<List<String>> createGame(PropertyChangeEvent evt) {
        List<List<String>> values = (List<List<String>>) evt.getNewValue();
        controller = new GameController(values.get(0).get(0), values.get(0).get(1), values.get(0).get(2), values.get(1), values.get(2), this);
        return values;
    }

    //handles the event from the MainView that loads a pre-existing game from a file
    private List<String> loadGame(PropertyChangeEvent evt) {
        List<String> values = (List<String>) evt.getNewValue();
        String filePath = values.get(0);
        String language = values.get(1);
        String mode = values.get(2); // color
        controller = new GameController(filePath, language, mode, this);
        return values;
    }

    //handles the event that changes the Application scene to the GameView
    private void changeGameScene(PropertyChangeEvent evt) {
        view.updateToGameScene((Parent) evt.getNewValue());
    }

    //handles the event that changes the Application scene to the MainView
    private void changeHomeScene(PropertyChangeEvent evt) {
        controller = null;
        view.updateToMainScene();
    }

    //handles the event that changes the key press functionality to that of only gameView specific button clicks
    private void changeToGameKeyPress(PropertyChangeEvent evt) {
        view.setGameKeyPress((PropertyChangeListener) evt.getNewValue());
    }

    //handles the event that handles parser exceptions when the sim file is not valid for loaded files
    private void parserException(PropertyChangeEvent evt) {
        ParserException e = (ParserException) evt.getNewValue();
        view.parserErrorConversion(e);
    }
}
