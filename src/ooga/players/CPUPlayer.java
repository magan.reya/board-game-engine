package ooga.players;

public class CPUPlayer extends Player {

  public CPUPlayer(int ID, String name) {
    super(ID, name);
  }

  public boolean isCPU() {
    return true;
  }

}
