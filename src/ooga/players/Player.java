package ooga.players;

public abstract class Player {

  private int myID;
  private String myName;
  private int myScore;

  public Player(int ID, String name) {
    myID = ID;
    myName = name;
    myScore = 0;
  }

  public int getID() {
    return myID;
  }

  public String getName() {
    return myName;
  }

  public int getScore() {
    return myScore;
  }

  public void setScore(int score) {
    myScore = score;
  }

  public abstract boolean isCPU();

}

