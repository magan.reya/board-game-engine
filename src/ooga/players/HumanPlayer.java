package ooga.players;

public class HumanPlayer extends Player {

  public HumanPlayer(int ID, String name) {
    super(ID, name);
  }

  public boolean isCPU() {
    return false;
  }


}
