package ooga;

import javafx.application.Application;
import javafx.stage.Stage;
import ooga.controller.GameController;
import ooga.controller.MainController;
import ooga.resources.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Feel free to completely change this code or delete it entirely. 
 */
public class Main extends Application {

    /**
     * Log4j2 Logger used to log user events and exceptions
     */
    public static final Logger LOGGER = LogManager.getLogger(Main.class);

    /**
     * @see Application#start(Stage)
     */
    @Override
    public void start (Stage stage) {
        MainController controller = new MainController();
        stage.setTitle(Config.MAIN_TITLE);
        stage.setScene(controller.start().setUpMain(Config.DEFAULT_WIDTH, Config.DEFAULT_HEIGHT));
        stage.show();
    }
}
