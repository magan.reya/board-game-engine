package view;

import static org.junit.jupiter.api.Assertions.assertEquals;

import controller.appcontroller.AppCtrlController;
import controller.gamecontroller.GameController;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import util.DukeApplicationTest;

public class ViewTestBeforeFile extends DukeApplicationTest {

  private String language = "English";
  private String mode = "Light";
  AppCtrlController appCtrl;
  AppCtrlView appCtrlView;
  GameController controller;
  private View myView;

  @Override
  public void start(Stage stage) {
    appCtrl = new AppCtrlController(language, stage);
    appCtrlView = appCtrl.getCtrlView();
    appCtrlView.newAutomaton("Game of Life", language, mode);
    controller = appCtrlView.getController();
    myView = controller.getView();
    stage.setScene(myView.setupDisplay());

  }

  @Test
  public void testLoadSimButton() {
    Button button = lookup("#LoadSIMFile").query();
    clickOn(button);
  }

  @Test
  public void testPlayBeforeFileLoaded() {
    Button button = lookup("#PlaySim").query();
    clickOn(button);
    String expected = "File has not been loaded!";
    assertEquals(expected, getDialogMessage());
  }

  @Test
  public void testStepBeforeFileLoaded() {
    Button button = lookup("#StepThrough").query();
    clickOn(button);
    String expected = "File has not been loaded!";
    assertEquals(expected, getDialogMessage());
  }

  @Test
  public void testPauseBeforeFileLoaded() {
    Button button = lookup("#PauseSim").query();
    clickOn(button);
    String expected = "Animation has not been started!";
    assertEquals(expected, getDialogMessage());
  }

  @Test
  public void testAboutBeforeFileLoaded() {
    Button button = lookup("#About").query();
    clickOn(button);
    String expected = "File has not been loaded!";
    assertEquals(expected, getDialogMessage());
  }
}