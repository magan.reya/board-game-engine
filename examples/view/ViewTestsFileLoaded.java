package view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import controller.appcontroller.AppCtrlController;
import controller.gamecontroller.GameController;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import util.DukeApplicationTest;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class ViewTestsFileLoaded extends DukeApplicationTest {

    private String language = "English";
    private String mode = "Light";
    AppCtrlController appCtrl;
    AppCtrlView appCtrlView;
    GameController controller;
    private View myView;

    @Override
    public void start(Stage stage) {
        appCtrl = new AppCtrlController(language, stage);
        appCtrlView = appCtrl.getCtrlView();
        appCtrlView.newAutomaton("Game of Life", language, mode);
        controller = appCtrlView.getController();
        myView = controller.getView();
        File file = new File("data/game_of_life/blinkers.sim");
        myView.passFileToController(file);
        myView.setSelectNeighborHoodString("Complete");
        myView.setSelectEdgePolicyString("Finite");
        //myView.loadSIMFile();
        stage.setScene(myView.setupDisplay());
    }

    @Test
    public void testPlayButton() {
        Button button = lookup("#PlaySim").query();
        clickOn(button);
        assertTrue(myView.getAnimationStarted());
    }

    @Test
    public void testPauseButtonBeforePlay() {
        Button button = lookup("#PauseSim").query();
        clickOn(button);
        String expected = "Animation has not been started!";
        assertEquals(expected, getDialogMessage());
    }
    @Test
    public void testPauseButtonAfterPlay() {
        Button play = lookup("#PlaySim").query();
        clickOn(play);
        Button pause = lookup("#PauseSim").query();
        clickOn(pause);
        assertTrue(myView.getAnimationPaused());
    }
    @Test
    public void testStepButton() {
        Button button = lookup("#StepThrough").query();
        clickOn(button);
        assertTrue(myView.getAnimationStarted());
    }
    @Test
    public void testAboutButton() {
        Button button = lookup("#About").query();
        clickOn(button);
    }
}
