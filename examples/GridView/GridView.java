package cellsociety.view;

import javafx.beans.property.Property;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

//Grid should be a part of a StackPane with all of its Cells
public class GridView {

    private static final int WIDTH = 300, HEIGHT = 300;
    private CellView [][] cells;
    private Group cellGroup;
    private int numRows, numColumns;
    private PropertyChangeSupport support;
    private ResourceBundle myPolygonResources;
    private static final String DEFAULT_RESOURCE_PACKAGE = "cellsociety.view.resources.";
    private double[] startPosition;
    private double sideLength;

    //constructor for the GridView
    public GridView() {
        support = new PropertyChangeSupport(this);
        myPolygonResources = ResourceBundle.getBundle(DEFAULT_RESOURCE_PACKAGE + "Polygon");
    }

    /**
     * Sets up the display for the GridView to be displayed in the SimulationDisplay instance
     * @param rows the number of rows of Cells
     * @param columns the number of columns of Cells
     */
    public void setGridDisplay(int rows, int columns, String polygonType, PropertyChangeListener listener) {
        numRows = rows;
        numColumns = columns;
        cells = new CellView[numRows][numColumns];
        createGroupOfCells(polygonType, listener);
    }

    /**
     * Creates GridPane instance
     * @param
     */
    private void createGroupOfCells(String polygonType, PropertyChangeListener listener) {
        calculateInitials(polygonType);
        cellGroup = new Group();
        Double[] tempStart = new Double[]{startPosition[0], startPosition[1]};
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numColumns; j++) {
                List<Double[]> pointCalculations = extractPoints(polygonType, isOdd(i,j));
                Double[] coordinates = new Double[pointCalculations.size() * 2];
                computeCoordinates(pointCalculations, coordinates, tempStart);
                createCell(i, j, coordinates, listener);
                boolean isOdd = isOdd(Integer.parseInt(myPolygonResources.getString(polygonType+"Row")) * i, j);
                tempStart[0] += isOdd ? Double.parseDouble(myPolygonResources.getString(polygonType+"XOdd").split(",")[0]) * sideLength :
                        Double.parseDouble(myPolygonResources.getString(polygonType+"XEven").split(",")[0]) * sideLength;
                tempStart[1] += isOdd ? Double.parseDouble(myPolygonResources.getString(polygonType+"XOdd").split(",")[1]) * sideLength :
                        Double.parseDouble(myPolygonResources.getString(polygonType+"XEven").split(",")[1]) * sideLength;
            }
            tempStart[0] = startPosition[0];
            double rowCalculation = isOdd(i, 0) ? Double.parseDouble(myPolygonResources.getString(polygonType+"OddEven")) * sideLength :
                    Double.parseDouble(myPolygonResources.getString(polygonType+"EvenEven")) * sideLength;
            tempStart[1] += isOdd(0, numColumns) ? Double.parseDouble(myPolygonResources.getString(polygonType+"YOdd")) * sideLength : rowCalculation;
        }
    }

    private void computeCoordinates(List<Double[]> pointCalculations, Double[] coordinates, Double[] tempStart) {
        int count = 0;
        for (Double[] point : pointCalculations){
            coordinates[count++] = tempStart[0] + point[0] * sideLength;
            coordinates[count++] = tempStart[1] + point[1] * sideLength;
        }
    }

    private void createCell(int row, int column, Double[] coordinates, PropertyChangeListener listener) {
        cells[row][column] = new CellView();
        cells[row][column].setCellDisplay(Color.WHITE, coordinates, row,column);
        cells[row][column].setOnMouseClick(row, column);
        cells[row][column].addPropertyChangeListener(listener);
        cellGroup.getChildren().add(cells[row][column].getPolygon());
    }

    /**
     * checks whether current indices add to odd value
     * @param row current row
     * @param column current column
     * @return whether row+col is Odd or not
     */
    private boolean isOdd(int row, int column){
        return ((row+column)%2==1);
    }

    /**
     * calculates sideLength and startPositions based on type of polygon
     * @param polygonType string pertaining to type of polygon
     */
    private void calculateInitials(String polygonType){
        List<Double> myCoefficients = extractCoefficients(polygonType);
        sideLength = Math.min(WIDTH/((myCoefficients.get(0)*numRows)+myCoefficients.get(1)),HEIGHT/((myCoefficients.get(0)*numColumns)+myCoefficients.get(1)));
        startPosition = new double[]{0,sideLength*myCoefficients.get(2)};
    }

    /**
     * Gets the GridPane representing the Cells in the Grid
     * @return the GridPane with all the Rectangles (CellViews)
     */
    public Group getGrid() {
        return cellGroup;
    }

    /**
     * Sets the state color of each CellView
     * @param row the row for the CellView that needs modification
     * @param column the column for the CellView that needs modification
     * @param color the Color for the CellView that needs modification
     */
    public void setStateColor(int row, int column, Color color) {
        cells[row][column].changeColor(color);
    }

    /**
     * Gets the color of a Cell
     * @param row the row position of the Cell
     * @param column the column position of the Cell
     * @return the Color of the Cell specified
     */
    public Color getStateColor(int row, int column) {
        return cells[row][column].getColor();
    }

    private List<Double[]> extractPoints(String input, boolean isOdd) {
        List<Double[]> edgeCalculations = new ArrayList<>();
        input = isOdd ? myPolygonResources.getString(input + "CoordinatesOdd") : myPolygonResources.getString(input + "CoordinatesEven");
        String[] pairs = input.split(";");
        for (String pair : pairs) {
            String[] xAndY = pair.split(",");
            Double[] neighbor = {Double.parseDouble(xAndY[0]), Double.parseDouble(xAndY[1])};
            edgeCalculations.add(neighbor);
        }
        return edgeCalculations;
    }

    private List<Double> extractCoefficients(String input){
        List <Double> coefficients = new ArrayList<>();
        coefficients.add(Double.parseDouble(myPolygonResources.getString(input+"Coefficient")));
        coefficients.add(Double.parseDouble(myPolygonResources.getString(input+"Sum")));
        coefficients.add(Double.parseDouble(myPolygonResources.getString(input+"Start")));
        return coefficients;
    }
}

