package controller.parsersTest;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import controller.parser.simparser.FireSIMParser;
import controller.parser.simparser.LifeSIMParser;
import controller.parser.simparser.PercolationSIMParser;
import controller.parser.simparser.SIMParser;
import controller.parser.simparser.SchellingSIMParser;
import controller.parser.simparser.WarToGameSIMParser;
import java.util.Map;
import java.util.ResourceBundle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SIMParserLoadFileTest {

  private static final String DEFAULT_RESOURCE_PACKAGE = "view.resources.";
  private String language = "English";
  private ResourceBundle myResources = ResourceBundle.getBundle(
      DEFAULT_RESOURCE_PACKAGE + language);

  @Test
  public void LifeSIMParserLoadFileTest() throws Exception {
    SIMParser simParser = new LifeSIMParser(language);

    Map<String, Object> infoMap1 = simParser.loadFile("data/game_of_life/blinkers.sim");
    assertNotEquals(infoMap1, null);

    Map<String, Object> infoMap2 = simParser.loadFile("data/game_of_life/glider.sim");
    assertNotEquals(infoMap2, null);

    Map<String, Object> infoMap3 = simParser.loadFile("data/game_of_life/penta_decathlon.sim");
    assertNotEquals(infoMap3, null);
  }

  @Test
  public void PercolationSIMParserLoadFileTest() throws Exception {
    SIMParser simParser = new PercolationSIMParser(language);

    Map<String, Object> infoMap1 = simParser.loadFile("data/percolation/long_pipe.sim");
    assertNotEquals(infoMap1, null);

    Map<String, Object> infoMap2 = simParser.loadFile("data/percolation/simple_pipe.sim");
    assertNotEquals(infoMap2, null);

    Map<String, Object> infoMap3 = simParser.loadFile("data/percolation/volcano.sim");
    assertNotEquals(infoMap3, null);
  }

  @Test
  public void FireCSVParserLoadFileTest() throws Exception {
    SIMParser simParser = new FireSIMParser(language);

    Map<String, Object> infoMap1 = simParser.loadFile("data/fire/fire_center.sim");
    assertNotEquals(infoMap1, null);

    Map<String, Object> infoMap2 = simParser.loadFile("data/fire/fire_corner.sim");
    assertNotEquals(infoMap2, null);

    Map<String, Object> infoMap3 = simParser.loadFile("data/fire/one_burning.sim");
    assertNotEquals(infoMap3, null);
  }

  @Test
  public void SchellingCSVParserLoadFileTest() throws Exception {
    SIMParser simParser = new SchellingSIMParser(language);

    Map<String, Object> infoMap1 = simParser.loadFile("data/schelling/sameExceptOne.sim");
    assertNotEquals(infoMap1, null);

    Map<String, Object> infoMap2 = simParser.loadFile("data/schelling/divided.sim");
    assertNotEquals(infoMap2, null);

    Map<String, Object> infoMap3 = simParser.loadFile("data/schelling/example.sim");
    assertNotEquals(infoMap3, null);

    Map<String, Object> infoMap4 = simParser.loadFile("data/schelling/simpleDivided.sim");
    assertNotEquals(infoMap4, null);
  }

  @Test
  public void WatorCSVParserLoadFileTest() throws Exception {
    SIMParser simParser = new WarToGameSIMParser(language);

    Map<String, Object> infoMap1 = simParser.loadFile("data/wator/buffet.sim");
    assertNotEquals(infoMap1, null);

    Map<String, Object> infoMap2 = simParser.loadFile("data/wator/divided.sim");
    assertNotEquals(infoMap2, null);

    Map<String, Object> infoMap3 = simParser.loadFile("data/wator/lines.sim");
    assertNotEquals(infoMap3, null);

    Map<String, Object> infoMap4 = simParser.loadFile("data/wator/smallDivided.sim");
    assertNotEquals(infoMap4, null);
  }

  @Test
  public void SIMParserExceptionTest() throws Exception {
    SIMParser simParser = new PercolationSIMParser(language);

    Exception exception1 = Assertions.assertThrows(Exception.class, () -> {
      simParser.loadFile("data/faultyGameOfLifeFiles/blinkersFaulty1.csv");
    });
    Assertions.assertTrue(
        exception1.getMessage().equals(myResources.getString("SIMErrorIncorrectFileType")));

    Map<String, Object> infoMap2 = simParser.loadFile(
        "data/faultyGameOfLifeFiles/blinkersFaulty2.sim");
    Exception exception2 = Assertions.assertThrows(Exception.class, () -> {
      simParser.checkValidFile(infoMap2);
    });
    Assertions.assertTrue(
        exception2.getMessage().equals(myResources.getString("SIMErrorMissingGameType")));

    Map<String, Object> infoMap3 = simParser.loadFile(
        "data/faultyGameOfLifeFiles/blinkersFaulty3.sim");
    Exception exception3 = Assertions.assertThrows(Exception.class, () -> {
      simParser.checkValidFile(infoMap3);
    });
    Assertions.assertTrue(
        exception3.getMessage().equals(myResources.getString("SIMErrorIncorrectGameType")));
  }
}
