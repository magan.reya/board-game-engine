package controller.parsersTest;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import controller.parser.csvparser.CSVParser;
import controller.parser.csvparser.FireCSVParser;
import controller.parser.csvparser.LifeCSVParser;
import controller.parser.csvparser.PercolationCSVParser;
import controller.parser.csvparser.SchellingCSVParser;
import controller.parser.csvparser.WarToGameCSVParser;
import java.util.Map;
import java.util.ResourceBundle;
import model.Grid;
import model.cells.CellState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CSVParserLoadFileTest {

  private static final String DEFAULT_RESOURCE_PACKAGE = "view.resources.";
  private String language = "English";
  private ResourceBundle myResources = ResourceBundle.getBundle(
      DEFAULT_RESOURCE_PACKAGE + language);

  @Test
  public void LifeCSVParserLoadFileTest() throws Exception {
    Map<Integer, CellState> cellIntToStateMap = Map.ofEntries(
        entry(0, CellState.DEAD),
        entry(1, CellState.LIVE)
    );
    CSVParser csvParser = new LifeCSVParser(language, cellIntToStateMap);

    Grid grid1 = csvParser.loadFile("data/game_of_life/glider.csv");
    assertNotEquals(grid1, null);

    Grid grid2 = csvParser.loadFile("data/game_of_life/glider.csv");
    assertNotEquals(grid2, null);

    Grid grid3 = csvParser.loadFile("data/game_of_life/penta-decathlon.csv");
    assertNotEquals(grid3, null);
  }

  @Test
  public void PercolationCSVParserLoadFileTest() throws Exception {
    Map<Integer, CellState> cellIntToStateMap = Map.ofEntries(
        entry(0, CellState.BLOCKED),
        entry(1, CellState.FULL),
        entry(2, CellState.EMPTY)
    );
    CSVParser csvParser = new PercolationCSVParser(language, cellIntToStateMap);

    Grid grid1 = csvParser.loadFile("data/percolation/long_pipe.csv");
    assertNotEquals(grid1, null);

    Grid grid2 = csvParser.loadFile("data/percolation/simple_pipe.csv");
    assertNotEquals(grid2, null);

    Grid grid3 = csvParser.loadFile("data/percolation/volcano.csv");
    assertNotEquals(grid3, null);
  }

  @Test
  public void FireCSVParserLoadFileTest() throws Exception {
    Map<Integer, CellState> cellIntToStateMap = Map.ofEntries(
        entry(0, CellState.BLOCKED),
        entry(1, CellState.FULL),
        entry(2, CellState.EMPTY)
    );
    CSVParser csvParser = new FireCSVParser(language, cellIntToStateMap);

    Grid grid1 = csvParser.loadFile("data/fire/fire_center.csv");
    assertNotEquals(grid1, null);

    Grid grid2 = csvParser.loadFile("data/fire/fire_corner.csv");
    assertNotEquals(grid2, null);

    Grid grid3 = csvParser.loadFile("data/fire/one_burning.csv");
    assertNotEquals(grid3, null);
  }

  @Test
  public void SchellingCSVParserLoadFileTest() throws Exception {
    Map<Integer, CellState> cellIntToStateMap = Map.ofEntries(
        entry(0, CellState.BLOCKED),
        entry(1, CellState.FULL),
        entry(2, CellState.EMPTY)
    );
    CSVParser csvParser = new SchellingCSVParser(language, cellIntToStateMap);

    Grid grid1 = csvParser.loadFile("data/schelling/sameExceptOne.csv");
    assertNotEquals(grid1, null);

    Grid grid2 = csvParser.loadFile("data/schelling/divided.csv");
    assertNotEquals(grid2, null);

    Grid grid3 = csvParser.loadFile("data/schelling/example.csv");
    assertNotEquals(grid3, null);

    Grid grid4 = csvParser.loadFile("data/schelling/simpleDivided.csv");
    assertNotEquals(grid4, null);
  }

  @Test
  public void WatorCSVParserLoadFileTest() throws Exception {
    Map<Integer, CellState> cellIntToStateMap = Map.ofEntries(
        entry(0, CellState.BLOCKED),
        entry(1, CellState.FULL),
        entry(2, CellState.EMPTY)
    );
    CSVParser csvParser = new WarToGameCSVParser(language, cellIntToStateMap);

    Grid grid1 = csvParser.loadFile("data/wator/buffet.csv");
    assertNotEquals(grid1, null);

    Grid grid2 = csvParser.loadFile("data/wator/divided.csv");
    assertNotEquals(grid2, null);

    Grid grid3 = csvParser.loadFile("data/wator/lines.csv");
    assertNotEquals(grid3, null);

    Grid grid4 = csvParser.loadFile("data/wator/smallDivided.csv");
    assertNotEquals(grid4, null);
  }

  @Test
  public void CSVParserExceptionTest() {
    Map<Integer, CellState> cellIntToStateMap = Map.ofEntries(
        entry(0, CellState.DEAD),
        entry(1, CellState.LIVE)
    );
    CSVParser csvParser = new LifeCSVParser(language, cellIntToStateMap);

    Exception exception1 = Assertions.assertThrows(Exception.class, () -> {
      csvParser.loadFile("data/faultyGameOfLifeFiles/blinkersFaulty1.csv");
    });
    Assertions.assertTrue(
        exception1.getMessage().equals(myResources.getString("CSVErrorNumRowColNotDefined")));

    Exception exception2 = Assertions.assertThrows(Exception.class, () -> {
      csvParser.loadFile("data/faultyGameOfLifeFiles/blinkersFaulty2.csv");
    });
    Assertions.assertTrue(
        exception2.getMessage().equals(myResources.getString("CSVErrorInvalidNumRowCol")));

    Exception exception3 = Assertions.assertThrows(Exception.class, () -> {
      csvParser.loadFile("data/faultyGameOfLifeFiles/blinkersFaulty3.csv");
    });
    Assertions.assertTrue(
        exception3.getMessage().equals(myResources.getString("CSVErrorInvalidElement")));

    Exception exception4 = Assertions.assertThrows(Exception.class, () -> {
      csvParser.loadFile("data/faultyGameOfLifeFiles/blinkersFaulty4.csv");
    });
    Assertions.assertTrue(
        exception4.getMessage().equals(myResources.getString("CSVErrorInvalidElementInGame")));

    Exception exception5 = Assertions.assertThrows(Exception.class, () -> {
      csvParser.loadFile("data/faultyGameOfLifeFiles/blinkersFaulty5.csv");
    });
    Assertions.assertTrue(
        exception5.getMessage().equals(myResources.getString("CSVErrorInvalidDimensions")));

    Exception exception6 = Assertions.assertThrows(Exception.class, () -> {
      csvParser.loadFile("data/faultyGameOfLifeFiles/blinkersFaulty1.sim");
    });
    Assertions.assertTrue(
        exception6.getMessage().equals(myResources.getString("CSVErrorIncorrectFileType")));

    Exception exception7 = Assertions.assertThrows(Exception.class, () -> {
      csvParser.loadFile("data/faultyGameOfLifeFiles/blinkersFaulty6.csv");
    });
    Assertions.assertTrue(
        exception7.getMessage().equals(myResources.getString("CSVErrorRowColFlipped")));
  }
}
