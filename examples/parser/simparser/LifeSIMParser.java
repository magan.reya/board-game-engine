package controller.parser.simparser;

public class LifeSIMParser extends SIMParser {

  public LifeSIMParser(String language) {
    super(language);
  }

  @Override
  protected String getType() {
    return getMyResources().getString("LifeGameType");
  }
}
