package controller.parser.simparser;

import controller.PropertyChangeObservable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.TreeMap;
import javafx.scene.paint.Color;
import model.cells.CellState;
import view.resources.Config;

public abstract class SIMParser extends PropertyChangeObservable implements PropertyChangeListener {

  private static final String DEFAULT_RESOURCE_PACKAGE = "view.resources.";
  private ResourceBundle myResources;

  public SIMParser(String language) {
    myResources = ResourceBundle.getBundle(DEFAULT_RESOURCE_PACKAGE + getLanguage(language));
  }

  /**
   * Getters and setters
   */
  protected ResourceBundle getMyResources() {
    return myResources;
  }

  protected abstract String getType();


  /**
   * LoadFile is called when the View notifies the SIMParser that a file has been loaded.
   *
   * @param evt is a packet sent from the View class to SIMParser
   * @throws Exception is thrown when the file given is not a SIM file
   */
  private void loadFile(PropertyChangeEvent evt) throws Exception {
    String fileString = "" + evt.getNewValue();
    Map<String, Object> infoMap = loadFile(fileString);
    checkValidFile(infoMap);
    notifyObserver(Config.LOAD_SIM_SIM_TO_CONTROLLER, infoMap);
    notifyObserver(Config.LOAD_CSV_SIM_TO_CSV, infoMap);
  }

  /**
   * Helper loadFile function
   *
   * @param fileString takes in a string path for the file
   * @return a mapping of all the key-value pairs in the SIM file
   * @throws FileNotFoundException occurs if the file is not found
   */
  public Map<String, Object> loadFile(String fileString) throws Exception {
    // Checks valid file
    if (!fileString.endsWith(Config.SIMFILE_TYPE)) {
      throw new Exception(myResources.getString("SIMErrorIncorrectFileType"));
    }
    Map<String, Object> infoMap = new HashMap<>();
    File file = new File(fileString);
    Scanner in = new Scanner(file);
    while (in.hasNextLine()) {
      String nextLine = in.nextLine();
      if (nextLine.isEmpty() || nextLine.startsWith("#")) {
        continue;
      }
      String[] lineSplitted = nextLine.split("=");
      if (lineSplitted[0].equals(Config.INITIALSTATES)) {
        infoMap.put(lineSplitted[0], parseCSVFileString(fileString, lineSplitted));
      } else if (lineSplitted[0].equals(Config.STATECOLORS)) {
        infoMap.put(lineSplitted[0], parseCellStateColorMap(lineSplitted));
      } else {
        infoMap.put(lineSplitted[0], lineSplitted[1]);
      }
    }
    return infoMap;
  }

  /**
   * Helper function for parsing the specific CSV file line - handles windows and macs properly
   */
  private String parseCSVFileString(String fileString, String[] lineSplitted) {
    if (!lineSplitted[1].contains("/")) {
      lineSplitted[1] = '/' + lineSplitted[1];
    }
    String csvFileWindowsString = fileString.replaceFirst("[^\\\\]*$",
        lineSplitted[1].split("/")[lineSplitted.length - 1]);
    String csvFileMacString = fileString.replaceFirst("[^/]*$",
        lineSplitted[1].split("/")[lineSplitted.length - 1]);
    if (csvFileMacString.length() > csvFileWindowsString.length()) {
      return csvFileMacString;
    } else {
      return csvFileWindowsString;
    }
  }

  /**
   * Helper function for parsing the cell state map, which is given as a string list but needs to be
   * converted to a hashmap
   */
  private Map<Integer, String> parseCellStateColorMap(String[] lineSplitted) {
    Map<Integer, String> cellStateColorMap = new HashMap<>();
    String[] colorsSplitted = lineSplitted[1].split(",");
    for (int i = 0; i < colorsSplitted.length; i += 1) {
      cellStateColorMap.put(i, colorsSplitted[i]);
    }
    return cellStateColorMap;
  }

  /**
   * Checks to see if the given file contains valid information
   *
   * @param infoMap is a map of key=value pairs in the file
   * @throws Exception if given file is not a SIM file nor a correct game type.
   */
  public void checkValidFile(Map<String, Object> infoMap) throws Exception {
    if (!infoMap.containsKey(Config.TYPESTRING)) {
      throw new Exception(myResources.getString("SIMErrorMissingGameType"));
    }
    String fileGameType = (String) infoMap.get(Config.TYPESTRING);
    if (!fileGameType.equals(getType())) {
      throw new Exception(myResources.getString("SIMErrorIncorrectGameType"));
    }
  }

  /**
   * Gets language for printing error
   *
   * @param inLanguage
   * @return
   */
  private String getLanguage(String inLanguage) {
    if (inLanguage.equals("English")) {
      return "English";
    } else if (inLanguage.equals("Espanol")) {
      return "Spanish";
    } else {
      return "French";
    }
  }

  /**
   * Save file pipeline of getting information from the controller and storing it in a sim file
   *
   * @param infoMap is a packet of all information in the current simulation that needs to be stored
   *                in the SIM file
   */
  public void saveFile(Map<String, Object> infoMap) {
    List<String> notRequired = Arrays.asList(Config.GRIDSTRING, Config.INITIALSTATES,
        Config.SAVEPATH, Config.INTTOCELLSTATEMAP_STRING);
    String filename = (String) infoMap.get(Config.TITLESTRING);
    File saveFile = (File) infoMap.get(Config.SAVEPATH);
    File parentFile = saveFile.getParentFile();
    try {
      FileWriter fileWriter = new FileWriter(new File(parentFile, filename + Config.SIMFILE_TYPE));
      for (Map.Entry entry : infoMap.entrySet()) {
        if (notRequired.contains((String) entry.getKey())) {
          continue;
        } else if (entry.getKey().equals(Config.STATECOLORS)) {
          Map<Integer, CellState> intToCellStateMap = (Map<Integer, CellState>) infoMap.get(
              Config.INTTOCELLSTATEMAP_STRING);
          intToCellStateMap = new TreeMap<>(intToCellStateMap);
          Map<CellState, Color> colorMap = (Map<CellState, Color>) entry.getValue();
          StringBuilder sb = new StringBuilder();
          for (Map.Entry intEntry : intToCellStateMap.entrySet()) {
            Color color = colorMap.get(intEntry.getValue());
            sb.append(toRGBCode(color));
            sb.append(",");
          }
          String colorString = sb.substring(0, sb.length() - 1);
          fileWriter.write(Config.STATECOLORS + "=" + colorString + "\n");
        } else {
          fileWriter.write(String.valueOf(entry) + "\n");
        }
      }
      // Add custom initial state
      fileWriter.write(Config.INITIALSTATES + "=" + filename + Config.CSVFILE_TYPE);
      fileWriter.close();
    } catch (IOException e) {
      notifyObserver(Config.INCORRECT_FILE_TYPE_SIM_TO_VIEW, e);
    }
    notifyObserver(Config.SAVE_SIM_TO_CSV, infoMap);
  }

  /**
   * Helper function to convert color into String
   */
  private static String toRGBCode(Color color) {
    return String.format(Config.RGBHEXFORMAT,
        (int) (color.getRed() * Config.RGBMAXVALUE),
        (int) (color.getGreen() * Config.RGBMAXVALUE),
        (int) (color.getBlue() * Config.RGBMAXVALUE));
  }

  /**
   * Observer pipeline (observable data is passed down to here from various different classes)
   * Switch case checks for the observable type (a string) and acts accordingly to the given
   * observable.
   *
   * @param evt is a packet of data
   */
  public void propertyChange(PropertyChangeEvent evt) {
    switch (evt.getPropertyName()) {
      case Config.LOAD_SIM_VIEW_TO_SIM:
        try {
          loadFile(evt);
        } catch (Exception e) {
          notifyObserver(Config.INCORRECT_FILE_TYPE_SIM_TO_VIEW, e);
        }
        break;
    }
  }
}
