package controller.parser.simparser;

public class WarToGameSIMParser extends SIMParser {

  public WarToGameSIMParser(String language) {
    super(language);
  }

  @Override
  protected String getType() {
    return getMyResources().getString("WaTorGameType");
  }
}
