package controller.parser.simparser;

public class FireSIMParser extends SIMParser {

  public FireSIMParser(String language) {
    super(language);
  }

  @Override
  protected String getType() {
    return getMyResources().getString("FireGameType");
  }
}
