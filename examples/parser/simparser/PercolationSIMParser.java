package controller.parser.simparser;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PercolationSIMParser extends SIMParser {

  public PercolationSIMParser(String language) {
    super(language);
  }

  @Override
  protected String getType() {
    return getMyResources().getString("PercolationGameType");
  }
}
