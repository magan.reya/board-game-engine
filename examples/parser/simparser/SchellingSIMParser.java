package controller.parser.simparser;

public class SchellingSIMParser extends SIMParser {

  public SchellingSIMParser(String language) {
    super(language);
  }

  @Override
  protected String getType() {
    return getMyResources().getString("SchellingGameType");
  }
}
