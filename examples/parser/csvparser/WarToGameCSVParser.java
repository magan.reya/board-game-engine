package controller.parser.csvparser;

import java.util.Map;
import model.cells.Cell;
import model.cells.CellState;
import model.cells.animalCells.PredatorCell;
import model.cells.animalCells.PreyCell;

public class WarToGameCSVParser extends CSVParser {

  public WarToGameCSVParser(String language, Map<Integer, CellState> cellStateMap) {
    super(language, cellStateMap);
  }

  @Override
  protected Cell getCell(CellState state) {
    return state == CellState.PREDATOR ? new PredatorCell(state) : new PreyCell(state);
  }

}
