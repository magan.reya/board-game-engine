package controller.parser.csvparser;

import java.util.Map;
import model.cells.CellState;

public class LifeCSVParser extends CSVParser {

  public LifeCSVParser(String language, Map<Integer, CellState> cellStateMap) {
    super(language, cellStateMap);
  }
}
