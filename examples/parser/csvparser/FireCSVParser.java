package controller.parser.csvparser;

import java.util.Map;
import model.cells.CellState;

public class FireCSVParser extends CSVParser {

  public FireCSVParser(String language, Map<Integer, CellState> cellStateMap) {
    super(language, cellStateMap);
  }
}
