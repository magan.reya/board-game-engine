package controller.parser.csvparser;

import java.util.Map;
import model.cells.CellState;

public class PercolationCSVParser extends CSVParser {

  public PercolationCSVParser(String language, Map<Integer, CellState> cellStateMap) {
    super(language, cellStateMap);
  }
}
