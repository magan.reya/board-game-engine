package controller.parser.csvparser;

import controller.PropertyChangeObservable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;
import model.Grid;
import model.GridIterator;
import model.cells.Cell;
import model.cells.CellState;
import view.resources.Config;

public abstract class CSVParser extends PropertyChangeObservable implements PropertyChangeListener {

  private static final String DEFAULT_RESOURCE_PACKAGE = "view.resources.";
  private ResourceBundle myResources;
  private Map<Integer, CellState> cellStateMap;
  private Map<CellState, Integer> reversedCellStateMap;

  public CSVParser(String language, Map<Integer, CellState> cellStateMap) {
    myResources = ResourceBundle.getBundle(DEFAULT_RESOURCE_PACKAGE + language);
    this.cellStateMap = cellStateMap;
    reversedCellStateMap = reverseMap(cellStateMap);
  }

  /**
   * Getters and setters
   */
  private CellState mapIntToCellState(int state) {
    return cellStateMap.get(state);
  }

  protected Cell getCell(CellState state) {
    return new Cell(state);
  }

  /**
   * Generate reversed cell state to int map
   */
  private Map<CellState, Integer> reverseMap(Map<Integer, CellState> cellStateMap) {
    Map<CellState, Integer> reversedCellStateMap = new HashMap<>();
    for (Map.Entry<Integer, CellState> entry : cellStateMap.entrySet()) {
      reversedCellStateMap.put(entry.getValue(), entry.getKey());
    }
    return reversedCellStateMap;
  }

  /**
   * LoadFile is called when the View notifies the SIMParser that a file has been loaded.
   *
   * @param evt is a packet sent from the View class to SIMParser
   * @throws Exception is thrown when the file given is not a SIM file
   */
  private void loadFile(PropertyChangeEvent evt) throws Exception {
    Map<String, Object> infoMap = (Map<String, Object>) evt.getNewValue();
    String fileString = (String) infoMap.get(Config.INITIALSTATES);
    Grid grid = loadFile(fileString);
    notifyObserver(Config.SETUP_GRID_CSV_TO_GRID, grid);
  }

  /**
   * Helper loadFile function
   *
   * @param fileString takes in a string path for the file
   * @return a mapping of all the key-value pairs in the SIM file
   * @throws FileNotFoundException occurs if the file is not found
   */
  public Grid loadFile(String fileString) throws Exception {
    File file = new File(fileString);
    Scanner in = new Scanner(file);

    // Checks if is a CSV file
    if (!fileString.endsWith(Config.CSVFILE_TYPE)) {
      throw new Exception(myResources.getString("CSVErrorIncorrectFileType"));
    }

    // Parse the first line
    String[] firstLine = in.next().split(",");
    if (firstLine.length != 2) {
      throw new Exception(myResources.getString("CSVErrorNumRowColNotDefined"));
    }

    int numRows, numCols;
    try {
      numCols = Integer.parseInt(firstLine[0]);
      numRows = Integer.parseInt(firstLine[1]);
    } catch (Exception e) {
      throw new Exception(myResources.getString("CSVErrorInvalidNumRowCol"));
    }

    Grid grid = new Grid(numRows, numCols);

    // Parse the rest of the file
    int id = 0;
    int rows = 0;
    int cols = 0;
    while (in.hasNext()) {
      String nextLine = in.nextLine();
      if (nextLine.isEmpty()) {
        continue;
      }
      String[] lineSplitted = nextLine.split(",");
      for (String s : lineSplitted) {
        int intState;
        try {
          intState = Integer.parseInt(s);
        } catch (Exception e) {
          throw new Exception(myResources.getString("CSVErrorInvalidElement"));
        }

        if (!cellStateMap.containsKey(intState)) {
          throw new Exception(myResources.getString("CSVErrorInvalidElementInGame"));
        }

        CellState state = mapIntToCellState(intState);
        Cell cell = getCell(state);
        grid.fillSpotWithCell(id, cell);
        id += 1;
      }
      cols = lineSplitted.length;
      rows += 1;
    }
    if (id != numCols * numRows) {
      throw new Exception(myResources.getString("CSVErrorInvalidDimensions"));
    }
    if (cols != numCols && rows != numRows) {
      throw new Exception(myResources.getString("CSVErrorRowColFlipped"));
    }
    return grid;
  }

  /**
   * Save file pipeline of getting information from the SIM (Grid specifically) and storing it in a
   * CSV
   *
   * @param evt is a packet of all information in the current simulation that needs to be stored in
   *            the SIM file
   */
  public void saveFile(PropertyChangeEvent evt) throws Exception {
    Map<String, Object> infoMap = (Map<String, Object>) evt.getNewValue();
    String filename = (String) infoMap.get(Config.TITLESTRING);
    File saveFile = (File) infoMap.get(Config.SAVEPATH);
    File parentFile = saveFile.getParentFile();

    FileWriter fileWriter = new FileWriter(new File(parentFile, filename + Config.CSVFILE_TYPE));
    Grid currGrid = (Grid) infoMap.get(Config.GRIDSTRING);
    GridIterator gridIterator = currGrid.getGridIterator();
    int numCols = currGrid.getNumCols();
    int numRows = currGrid.getNumRows();
    int iter = 0;

    fileWriter.append(numCols + "," + numRows + "\n");

    while (gridIterator.hasNext()) {
      Cell currC = gridIterator.next().getCell();
      CellState currCS = currC.getState();
      int currVal = reversedCellStateMap.get(currCS);

      fileWriter.append("" + currVal);
      iter += 1;

      if (iter != numCols) {
        fileWriter.append(",");
      } else {
        fileWriter.append("\n");
        iter = 0;
      }
    }
    fileWriter.close();
    notifyObserver(Config.SAVE_CSV_TO_VIEW, null);
  }


  /**
   * Observer pipeline (observable data is passed down to here from various different classes)
   * Switch case checks for the observable type (a string) and acts accordingly to the given
   * observable.
   *
   * @param evt is a packet of data
   */
  public void propertyChange(PropertyChangeEvent evt) {
    switch (evt.getPropertyName()) {
      case Config.LOAD_CSV_SIM_TO_CSV:
        try {
          loadFile(evt);
        } catch (Exception e) {
          notifyObserver(Config.INCORRECT_FILE_TYPE_CSV_TO_VIEW, e);
        }
        break;
      case Config.SAVE_SIM_TO_CSV:
        try {
          saveFile(evt);
        } catch (Exception e) {
          notifyObserver(Config.INCORRECT_FILE_TYPE_CSV_TO_VIEW, e);
        }
        break;
    }
  }
}
