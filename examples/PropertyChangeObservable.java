package controller;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class PropertyChangeObservable {

  private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

  public void addObserver(String name, PropertyChangeListener l) {
    pcs.addPropertyChangeListener(name, l);
  }

  public void notifyObserver(String name, Object o) {
    pcs.firePropertyChange(name, null, o);
  }
}
