package ooga.model;

import java.awt.Point;
import java.util.List;

import ooga.players.CPUPlayer;
import ooga.players.HumanPlayer;
import ooga.players.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class OthelloModelTest {

  private OthelloModel om;

  @BeforeEach
  public void setUp() {
    om = new OthelloModel(new int[4][4]);
    Point initialBlackPiece = new Point(1, 1);
    Point initialWhitePiece = new Point(2, 1);
    Point nextWhitePiece = new Point(1, 2);
    Point nextBlackPiece = new Point(2, 2);
    om.getGrid().addPiece(initialBlackPiece, 1);
    om.getGrid().addPiece(initialWhitePiece, 2);
    om.getGrid().addPiece(nextBlackPiece, 1);
    om.getGrid().addPiece(nextWhitePiece, 2);
  }


  @Test
  public void checkFlank() {
    Point flankedCell = new Point(2, 1);
    assertEquals(om.getGrid().getCell(flankedCell).getState(), 2);
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(3, 1);
    om.execute();
    assertEquals(om.getGrid().getCell(flankedCell).getState(), 1);
  }

  @Test
  public void checkMidWay() {
    Point first = new Point(2, 1);
    Point second = new Point(1, 1);
    Point third = new Point(1, 2);
    Point fourth = new Point(2, 2);
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(3, 1);
    om.execute();
    assertEquals(om.getGrid().getCell(first).getState(), 1);
    om.setCurrentPlayerID(2);
    om.setPositionToBePlaced(1, 0);
    om.execute();
    assertEquals(om.getGrid().getCell(second).getState(), 2);
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(0, 2);
    om.execute();
    assertEquals(om.getGrid().getCell(third).getState(), 1);
    om.setCurrentPlayerID(2);
    om.setPositionToBePlaced(3, 3);
    om.execute();
    assertEquals(om.getGrid().getCell(fourth).getState(), 2);
  }

  @Test
  public void checkAllUpUntilOnePlayerCantFlank() {
    checkMidWay();
    Point change1 = new Point(1, 1);
    Point change2 = new Point(1, 1);
    Point change3 = new Point(2, 2);
    Point change4 = new Point(0, 1);
    Point change5 = new Point(0, 2);
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(0, 1);
    om.execute();
    assertEquals(om.getGrid().getCell(change1).getState(), 1);
    assertEquals(0, om.getGameStatus());
    om.setCurrentPlayerID(2);
    om.setPositionToBePlaced(0, 0);
    om.execute();
    assertEquals(om.getGrid().getCell(change2).getState(), 2);
    assertEquals(0, om.getGameStatus());
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(3, 2);
    om.execute();
    assertEquals(om.getGrid().getCell(change3).getState(), 1);
    om.setCurrentPlayerID(2);
    om.setPositionToBePlaced(0, 3);
    om.execute();
    assertEquals(om.getGrid().getCell(change4).getState(), 2);
    assertEquals(om.getGrid().getCell(change5).getState(), 2);
    om.setCurrentPlayerID(1);
    assertFalse(om.setPositionToBePlaced(3, 0));
    Point cantPlace = new Point(3, 0);
    om.execute();
    assertEquals(om.getGrid().getCell(cantPlace).getState(), 0);
  }

  @Test
  public void checkAll() {
    checkAllUpUntilOnePlayerCantFlank();
    Point f1 = new Point(3, 1);
    Point f2 = new Point(3, 2);
    om.setCurrentPlayerID(2);
    om.setPositionToBePlaced(3, 0);
    om.execute();
    assertEquals(om.getGrid().getCell(f1).getState(), 2);
    assertEquals(om.getGrid().getCell(f2).getState(), 2);
    Point cantPlace = new Point(1, 3);
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(1, 3);
    om.execute();
    assertEquals(om.getGrid().getCell(cantPlace).getState(), 0);
    om.setCurrentPlayerID(2);
  }

  @Test
  public void gameOverAndWonByPlayer2() {
    checkAll();
    Point f3 = new Point(1, 2);
    om.setPositionToBePlaced(1, 3);
    om.execute();
    assertEquals(om.getGrid().getCell(f3).getState(), 2);
    assertTrue(om.checkEndGameCondition());
    assertEquals(om.getGameStatus(), 2);

  }

  @Test
  public void testSmartMoveValidMove(){
    Point computerPoint = new Point(0,1);
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(1, 3);
    om.execute();
    om.setCurrentPlayerID(2);
    om.playSmart();
    om.execute();
    assertEquals(om.getGrid().getCell(computerPoint).getState(), 2);
  }

  @Test
  public void testSmartMoveMore(){
    testSmartMoveValidMove();
    Point newComp = new Point(0, 3);
    Point compTwo = new Point(3, 0);
    Point compThree = new Point(0,0);
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(1, 0);
    om.execute();
    om.setCurrentPlayerID(2);
    om.playSmart();
    om.execute();
    assertEquals(2, om.getGrid().getCell(newComp).getState());
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(2, 0);
    om.execute();
    om.setCurrentPlayerID(2);
    om.playSmart();
    om.execute();
    assertEquals(2, om.getGrid().getCell(compTwo).getState());
    om.setCurrentPlayerID(1);
    om.setPositionToBePlaced(3, 1);
    om.execute();
    om.setCurrentPlayerID(2);
    om.playSmart();
    om.execute();
    assertEquals(2, om.getGrid().getCell(compThree).getState());
  }

  @Test
  public void testScoreUpdate() throws Exception{
    Player p = new HumanPlayer(1, "reya");
    Point toMove = new Point(3,1);
    om.move(toMove, p);
    om.incrementScore(List.of(p));
    assertEquals(4, p.getScore());
  }

  @Test
  public void humanInvalidMove(){
    Player p = new HumanPlayer(1, "reya");
    Point toMove = new Point(0,0);
    assertThrows(Exception.class, () -> {om.move(toMove, p);});
  }

  @Test
  public void humanCPUAlternate() throws Exception {
    Point computerPoint = new Point(0,1);
    Player p1 = new HumanPlayer(1, "reya");
    Player p2 = new CPUPlayer(2, "CPU");
    Point playerPoint = new Point(1, 3);
    om.move(playerPoint, p1);
    om.move(computerPoint, p2);
    assertEquals(om.getGrid().getCell(computerPoint).getState(), 2);
  }

  @Test
  public void humanCPUScores() throws Exception {
    Point computerPoint = new Point(0,1);
    Player p1 = new HumanPlayer(1, "reya");
    Player p2 = new CPUPlayer(2, "CPU");
    Point playerPoint = new Point(1, 3);
    om.move(playerPoint, p1);
    om.move(computerPoint, p2);
    om.incrementScore(List.of(p1, p2));
    assertEquals(3, p1.getScore());
    assertEquals(3, p2.getScore());
  }

  @Test
  public void othelloTest() {

    Point point = new Point(1, 0);
    Point updatedPoint = new Point(1, 1);
    om.setCurrentPlayerID(2);
    om.setPositionToBePlaced(1, 0);
    om.execute();
    assertEquals(om.getGrid().getCell(point).getState(), 2); // Should be set as 2
    assertEquals(om.getGrid().getCell(updatedPoint).getState(), 2); // Should be changed to 2
  }

}
