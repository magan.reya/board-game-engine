package ooga.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Point;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class CheckersModelTest {

  private CheckersModel cm;
  private int[][] board;

  @BeforeEach
  public void setUp() {
    board = new int[15][15];
  }

  @Test
  public void validRemove() {
    board[7][7] = 1;
    cm = new CheckersModel(board);
    cm.setCurrentPlayerID(1);
    assertTrue(cm.setPositionToBeRemoved(7, 7));
  }

  @Test
  public void invalidRemove() {
    board[7][7] = 2;
    cm = new CheckersModel(board);
    cm.setCurrentPlayerID(1);
    assertFalse(cm.setPositionToBeRemoved(7, 7));
  }

  @Test
  public void validPlaceOnDiagonal() {
    board[7][7] = 1;
    cm = new CheckersModel(board);
    cm.setCurrentPlayerID(1);
    assertTrue(cm.setPositionToBeRemoved(7, 7));
    assertTrue(cm.isValidPlace(new Point(8, 8)));
    assertTrue(cm.isValidPlace(new Point(6, 8)));
    assertTrue(cm.setPositionToBePlaced(8, 8));
    cm.execute();
    assertEquals(0, cm.getGrid().getState(7, 7));
    assertEquals(1, cm.getGrid().getState(8, 8));
  }

  @Test
  public void invalidPlaceOnDiagonal() {
    board[7][7] = 1;
    board[8][8] = 2;
    cm = new CheckersModel(board);
    cm.setCurrentPlayerID(1);
    assertTrue(cm.setPositionToBeRemoved(7, 7));
    assertFalse(cm.isValidPlace(new Point(8, 8)));
    assertFalse(cm.isValidPlace(new Point(7, 8)));
  }

  @Test
  public void testCapture() {
    board[7][7] = 1;
    board[8][8] = 2;
    cm = new CheckersModel(board);
    cm.setCurrentPlayerID(1);
    assertTrue(cm.setPositionToBeRemoved(7, 7));
    assertTrue(cm.setPositionToBePlaced(9, 9));
    cm.execute();
    assertEquals(0, cm.getGrid().getState(7, 7));
    assertEquals(0, cm.getGrid().getState(8, 8));
    assertEquals(1, cm.getGrid().getState(9, 9));
  }

  @Test
  public void upgradeToKing() {
    board[1][7] = 2;
    cm = new CheckersModel(board);
    cm.setCurrentPlayerID(2);
    assertTrue(cm.setPositionToBeRemoved(7, 1));
    assertTrue(cm.setPositionToBePlaced(8, 0));
    cm.execute();
    assertEquals(0, cm.getGrid().getState(7, 1));
    assertEquals(-2, cm.getGrid().getState(8, 0));
  }

  @Test
  public void moveKingPiece() {
    board[7][7] = -1;
    cm = new CheckersModel(board);
    cm.setCurrentPlayerID(1);
    assertTrue(cm.setPositionToBeRemoved(7, 7));
    assertTrue(cm.isValidPlace(new Point(8, 8)));
    assertTrue(cm.isValidPlace(new Point(6, 6)));
    assertTrue(cm.isValidPlace(new Point(6, 8)));
    assertTrue(cm.isValidPlace(new Point(8, 6)));
  }


}
