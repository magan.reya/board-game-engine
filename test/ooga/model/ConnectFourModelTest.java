package ooga.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.awt.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConnectFourModelTest {
  private ConnectFourModel gm;
  private int COLS = 7;
  private int ROWS = 6;

  @BeforeEach
  public void setUp(){
    gm = new ConnectFourModel(new int[ROWS][COLS]);
    gm.setCurrentPlayerID(1);
  }

  @Test
  public void canPlacePiece(){
    assertTrue(gm.setPositionToBePlaced(2,ROWS-1));
  }

  @Test
  public void cannotPlacePieceVersionOne(){
    // Verifies that pieces must be added bottom-up
    assertFalse(gm.setPositionToBePlaced(2,2));
  }

  @Test
  public void cannotPleasePieceVersionTwo(){
    gm.setPositionToBePlaced(2, ROWS-1);
    gm.execute();
    assertFalse(gm.setPositionToBePlaced(2, ROWS-1));
  }

  @Test
  public void gameWorksWithoutGravity(){
    assertTrue(gm.setPositionToBePlaced(1,ROWS-1));
    gm.execute();
    assertTrue(gm.setPositionToBePlaced(1,ROWS-2));
  }

  @Test
  public void gameIsWon(){
    gm.setPositionToBePlaced(0,ROWS-1);
    gm.execute();
    gm.setPositionToBePlaced(0,ROWS-2);
    gm.execute();
    gm.setPositionToBePlaced(0,ROWS-3);
    gm.execute();
    gm.setPositionToBePlaced(0,ROWS-4);
    gm.execute();
    assertTrue(gm.checkEndGameCondition());
  }

  @Test
  public void gameIsNotWon(){
    gm.setPositionToBePlaced(0,ROWS-1);
    gm.execute();
    gm.setPositionToBePlaced(0,ROWS-2);
    gm.execute();
    gm.setPositionToBePlaced(2,ROWS-1);
    gm.execute();
    gm.setPositionToBePlaced(2,ROWS-2);
    gm.execute();
    assertFalse(gm.checkEndGameCondition());
  }

  @Test
  public void gameIsWonByPlayerOne(){
    gm.setPositionToBePlaced(0,ROWS-1);
    gm.execute();
    gm.setPositionToBePlaced(0,ROWS-2);
    gm.execute();
    gm.setPositionToBePlaced(0,ROWS-3);
    gm.execute();
    gm.setPositionToBePlaced(0,ROWS-4);
    gm.execute();
    assertEquals(1, gm.getGameStatus());
  }

  @Test
  public void twoPlayers() {
    gm.setCurrentPlayerID(2);
    gm.setPositionToBePlaced(0, ROWS-1);
    gm.execute();
    gm.setCurrentPlayerID(1);
    gm.setPositionToBePlaced(0, ROWS-2);
    gm.execute();
    gm.setCurrentPlayerID(2);
    gm.setPositionToBePlaced(1, ROWS-1);
    gm.execute();
    gm.setCurrentPlayerID(1);
    gm.setPositionToBePlaced(1, ROWS-2);
    gm.execute();
    gm.setCurrentPlayerID(2);
    gm.setPositionToBePlaced(2, ROWS-1);
    gm.execute();
    gm.setCurrentPlayerID(1);
    gm.setPositionToBePlaced(2, ROWS-2);
    gm.execute();
    gm.setCurrentPlayerID(2);
    gm.setPositionToBePlaced(3, ROWS-1);
    gm.execute();
    assertEquals(2, gm.getGameStatus());
  }

  @Test
  public void gameIsNotWonBecauseDifferentIDPieces(){
    gm.setCurrentPlayerID(2);
    gm.setPositionToBePlaced(0,ROWS-1);
    gm.execute();
    gm.setCurrentPlayerID(1);
    gm.setPositionToBePlaced(1,ROWS-1);
    gm.execute();
    gm.setCurrentPlayerID(2);
    gm.setPositionToBePlaced(2,ROWS-1);
    gm.execute();
    gm.setCurrentPlayerID(1);
    gm.setPositionToBePlaced(3, ROWS-1);
    gm.execute();
    gm.setCurrentPlayerID(2);
    gm.setPositionToBePlaced(4, ROWS-1);
    gm.execute();
    assertFalse(gm.checkEndGameCondition());
  }

  @Test
  public void outOfBoardPlace() {
    assertFalse(gm.setPositionToBePlaced(15,15));
  }

  @Test
  public void getAllMoves(){
    for (int i = ROWS-1; i >= 0; i--) {
      gm.setPositionToBePlaced(0, i);
      gm.execute();
    }
    List<Point> places = gm.getAllPossiblePlaces(List.of(gm.getCurrentPlayerID()));
  }
}
