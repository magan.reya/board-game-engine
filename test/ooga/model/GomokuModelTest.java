package ooga.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GomokuModelTest {
    private GomokuModel gm;

    @BeforeEach
    public void setUp() {
        gm = new GomokuModel(new int[15][15]);
        gm.setCurrentPlayerID(1);
    }


    @Test
    public void canPlacePiece() {
        assertTrue(gm.setPositionToBePlaced(10, 10));
    }

    @Test
    public void cannotPlacePiece() {
        gm.setPositionToBePlaced(10, 10);
        gm.execute();
        assertFalse(gm.setPositionToBePlaced(10, 10));
    }

    @Test
    public void gameIsWon() {
        gm.setPositionToBePlaced(10, 10);
        gm.execute();
        gm.setPositionToBePlaced(10, 9);
        gm.execute();
        gm.setPositionToBePlaced(10, 8);
        gm.execute();
        gm.setPositionToBePlaced(10, 7);
        gm.execute();
        gm.setPositionToBePlaced(10, 6);
        gm.execute();
        assertTrue(gm.checkEndGameCondition());
    }

    @Test
    public void gameIsNotWon() {
        gm.setPositionToBePlaced(10, 10);
        gm.execute();
        gm.setPositionToBePlaced(10, 9);
        gm.execute();
        gm.setPositionToBePlaced(10, 8);
        gm.execute();
        gm.setPositionToBePlaced(10, 7);
        gm.execute();
        assertFalse(gm.checkEndGameCondition());
    }

    @Test
    public void gameIsWonByPlayerOne() {
        gm.setPositionToBePlaced(10, 10);
        gm.execute();
        gm.setPositionToBePlaced(10, 9);
        gm.execute();
        gm.setPositionToBePlaced(10, 8);
        gm.execute();
        gm.setPositionToBePlaced(10, 7);
        gm.execute();
        gm.setPositionToBePlaced(10, 6);
        gm.execute();
        assertEquals(1, gm.getGameStatus());
    }

    @Test
    public void diagonalWin() {
        gm.setPositionToBePlaced(10, 10);
        gm.execute();
        gm.setPositionToBePlaced(9, 9);
        gm.execute();
        gm.setPositionToBePlaced(6, 6);
        gm.execute();
        gm.setPositionToBePlaced(8, 8);
        gm.execute();
        gm.setPositionToBePlaced(7, 7);
        gm.execute();
        assertEquals(1, gm.getGameStatus());
    }

    @Test
    public void twoPlayers() {
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(10, 10);
        gm.execute();
        gm.setCurrentPlayerID(1);
        gm.setPositionToBePlaced(6, 5);
        gm.execute();
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(9, 9);
        gm.execute();
        gm.setCurrentPlayerID(1);
        gm.setPositionToBePlaced(6, 6);
        gm.execute();
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(8, 8);
        gm.execute();
        gm.setCurrentPlayerID(1);
        gm.setPositionToBePlaced(6, 4);
        gm.execute();
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(7, 7);
        gm.execute();
        gm.setCurrentPlayerID(1);
        gm.setPositionToBePlaced(6, 7);
        gm.execute();
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(11, 11);
        gm.execute();
        gm.setCurrentPlayerID(1);
        gm.setPositionToBePlaced(6, 3);
        gm.execute();
        assertEquals(1, gm.getGameStatus());
    }

    @Test
    public void gameIsNotWonBecauseDifferentIDPieces() {
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(10, 10);
        gm.execute();
        gm.setCurrentPlayerID(1);
        gm.setPositionToBePlaced(10, 9);
        gm.execute();
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(10, 8);
        gm.execute();
        gm.setCurrentPlayerID(1);
        gm.setPositionToBePlaced(10, 7);
        gm.execute();
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(10, 6);
        gm.execute();
        assertFalse(gm.checkEndGameCondition());
    }

    @Test
    public void outOfBoardPlace() {
        assertFalse(gm.setPositionToBePlaced(15, 15));
    }

    @Test
    public void checkEndGameConditionOnBoundary() {
        gm.setPositionToBePlaced(3, 3);
        gm.execute();
        gm.setPositionToBePlaced(4, 4);
        gm.execute();
        gm.setPositionToBePlaced(0, 0);
        gm.execute();
        gm.setPositionToBePlaced(1, 1);
        gm.execute();
        gm.setPositionToBePlaced(2, 2);
        gm.execute();
        assertEquals(1, gm.getGameStatus());
    }

    @Test
    public void testAllPossiblePlaces() {
        gm.setPositionToBePlaced(3, 3);
        gm.execute();
        gm.setPositionToBePlaced(3, 4);
        gm.execute();
        Set<Point> expected = new HashSet<>(Arrays.asList(
                new Point(2, 2),
                new Point(2, 3),
                new Point(2, 4),
                new Point(3, 2),
                new Point(4, 2),
                new Point(4, 3),
                new Point(4, 4),
                new Point(2, 5),
                new Point(3, 5),
                new Point(4, 5)
        ));
        assertEquals(expected, new HashSet<>(gm.getAllPossiblePlaces(List.of(gm.getCurrentPlayerID()))));
    }

    @Test
    public void testSmartMoveOneStep() {
        gm.setPositionToBePlaced(3, 3);
        gm.execute();
        gm.setPositionToBePlaced(4, 4);
        gm.execute();
        gm.setPositionToBePlaced(0, 0);
        gm.execute();
        gm.setPositionToBePlaced(1, 1);
        gm.execute();
        gm.playSmart();
        assertEquals(new Point(2, 2), gm.getPositionToBePlaced());
    }

    @Test
    public void testSmartMoveTwoSteps() {
        gm.setPositionToBePlaced(3, 3);
        gm.execute();
        gm.setPositionToBePlaced(4, 4);
        gm.execute();
        gm.setPositionToBePlaced(0, 0);
        gm.execute();
        gm.playSmart();
        gm.execute();
        assertEquals(0, gm.getGameStatus());
        gm.playSmart();
        gm.execute();
        assertEquals(1, gm.getGameStatus());
    }

    @Test
    public void testPlaySmartWithHuman() {
        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(10, 10);
        gm.execute();
        assertEquals(0, gm.getGameStatus());

        gm.setCurrentPlayerID(1);
        gm.execute();
        gm.playSmart();
        assertEquals(0, gm.getGameStatus());

        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(10, 8);
        gm.execute();
        assertEquals(0, gm.getGameStatus());

        gm.setCurrentPlayerID(1);
        gm.execute();
        gm.playSmart();
        assertEquals(0, gm.getGameStatus()); // Should still be 0, but game status is non zero

        gm.setCurrentPlayerID(2);
        gm.setPositionToBePlaced(10, 6);
        gm.execute();
        assertEquals(0, gm.getGameStatus());
        }

    @Test
    public void twoCPUsPlaying() {
        gm.setCurrentPlayerID(2);
        gm.playSmart();
        gm.execute();
        assertEquals(0, gm.getGameStatus());

        gm.setCurrentPlayerID(1);
        gm.playSmart();
        gm.execute();
        assertEquals(0, gm.getGameStatus());

        gm.setCurrentPlayerID(2);
        gm.playSmart();
        gm.execute();
        assertEquals(0, gm.getGameStatus());

        gm.setCurrentPlayerID(1);
        gm.playSmart();
        gm.execute();
        assertEquals(0, gm.getGameStatus());

        gm.setCurrentPlayerID(2);
        gm.playSmart();
        gm.execute();
        assertEquals(0, gm.getGameStatus());

        gm.setCurrentPlayerID(1);
        gm.playSmart();
        gm.execute();
        assertEquals(0, gm.getGameStatus());
    }

    @Test
    public void testAllPossibleMove() {
        gm.setCurrentPlayerID(1);
        List<Point> points = gm.getAllPossiblePlaces(List.of(1));
    }
}
