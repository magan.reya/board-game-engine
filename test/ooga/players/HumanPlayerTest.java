package ooga.players;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HumanPlayerTest {

    @Test
    void setUpHuman(){
        Player p = new HumanPlayer(1, "player");
        assertFalse(p.isCPU());
    }

    @Test
    void scorePlayer(){
        Player p = new HumanPlayer(1, "player");
        p.setScore(15);
        assertEquals(15, p.getScore());
    }

    @Test
    void getNameAndID(){
        Player p = new HumanPlayer(17, "Coding Devil");
        assertEquals(17, p.getID());
        assertEquals("Coding Devil", p.getName());
    }
}
