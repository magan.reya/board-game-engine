package ooga.players;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CPUPlayerTest {
    @Test
    void setUpHuman(){
        Player p = new CPUPlayer(1, "player");
        assertTrue(p.isCPU());
    }

    @Test
    void scorePlayer(){
        Player p = new CPUPlayer(1, "player");
        p.setScore(15);
        assertEquals(15, p.getScore());
    }

    @Test
    void getNameAndID(){
        Player p = new CPUPlayer(17, "Coding Devil");
        assertEquals(17, p.getID());
        assertEquals("Coding Devil", p.getName());
    }
}
