package ooga.components;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

public class CellTest {
    private Grid g;

    @BeforeEach
    public void setUp(){
        g = new Grid(new int[5][6]);
    }

    @Test
    public void checkNeighborsTrueTest(){
        Point point1 = new Point(4,4);
        Point point2 = new Point(5,4);
        Point point3 = new Point(2,2);
        g.addPiece(point1, 0);
        g.addPiece(point2, 0);
        g.addPiece(point3, 0);
        assertTrue(g.getCell(point1).getCompleteNeighbors().contains(g.getCell(point2)));
    }

    @Test
    public void checkNeighborsNotContainsTest(){
        Point point1 = new Point(4,4);
        Point point2 = new Point(5,4);
        Point point3 = new Point(2,2);
        g.addPiece(point1, 0);
        g.addPiece(point2, 0);
        g.addPiece(point3, 0);
        assertFalse(g.getCell(point1).getCompleteNeighbors().contains(g.getCell(point3)));
    }

    @Test
    public void setNextStateTest(){
        Cell p = new Cell(4,4, 0);
        p.setState(1);
        assertEquals(1, p.getState());
    }

    @Test
    public void setCurrentStateTest(){
        Cell p = new Cell(4,4, 0);
        p.setState(1);
        assertEquals(1, p.getState());
    }
}
