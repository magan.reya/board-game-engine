package ooga.components;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

public class GridTest {
    private Grid g;
    private int EMPTY = 0;

    @BeforeEach
    public void setUp(){
        g = new Grid(new int[5][6]);
    }

    @Test
    public void rightNumberOfRowsTest(){
        assertEquals(g.getNumRows(), 5);
    }

    @Test
    public void rightNumberOfColsTest(){
        assertEquals(g.getNumCols(), 6);
    }

    @Test
    public void getPieceNotOnGridTest(){
        Point point = new Point(4,4);
        assertEquals(EMPTY, g.getCell(point).getState());
    }

    @Test
    public void addAndCheckPieceExistsOnGridTest(){
        Point point = new Point(4,4);
        g.addPiece(point, 1);
        assertTrue(g.doesGridHavePiece(point));
    }

    @Test
    public void addAndgetStateOfPieceTest(){
        Point point = new Point(4,4);
        g.addPiece(point, 1);
        assertEquals(g.getState(4,4), 1);
    }

    @Test
    public void getStateOfInvalidPieceTest(){
        assertThrows(Exception.class, () -> {g.getState(7,7);});
    }

    @Test
    public void addThenRemovePieceAndCheckDoesntExistTest(){
        Point point = new Point(4,4);
        g.addPiece(point, 1);
        g.removePiece(point);
        assertFalse(g.doesGridHavePiece(point));
    }


}
