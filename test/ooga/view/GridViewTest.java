package ooga.view;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import ooga.controller.GameController;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;
import ooga.controller.MainController;
import ooga.resources.Config;
import ooga.view.game.GameView;
import org.junit.jupiter.api.Test;
import util.DukeApplicationTest;
import ooga.view.GridView;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

class GridViewTest extends DukeApplicationTest {

    private String gameType = "Checkers";
    private String language = "English";
    private String mode = "Light";
    private List<String> playerNames = Arrays.asList("playerA", "playerB");
    private List<String> playerTypes = Arrays.asList("Human", "Human");

    private MainController mainController;
    private GameController gameController;

    @Override
    public void start(Stage stage) {
        gameController = new GameController(
                gameType,
                language,
                mode,
                playerNames,
                playerTypes,
                mainController
        );
        stage.setScene(gameController.getGameView()
                .getGameViewScene(Config.GAMEVIEW_DEFAULT_WIDTH, Config.GAMEVIEW_DEFAULT_HEIGHT));
        stage.show();
    }

    @Test
    public void clickCheckersPiece() {
        gameController.getGameView().setPieceClicks(1);
        Circle piece = lookup("#72").query();
        clickOn(piece);
    }

    @Test
    public void clickCheckersPossiblePlace() {
        gameController.getGameView().setPieceClicks(1);
        Circle piece = lookup("#72").query();
        clickOn(piece);
        gameController.getGameView().addPossibleMoves(List.of(new Point(1, 2)));
        Rectangle cell = lookup("#17").query();
        clickOn(cell);
    }
}