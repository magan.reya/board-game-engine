package ooga.view.gameview;

import java.util.Arrays;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import ooga.controller.GameController;
import ooga.controller.MainController;
import ooga.players.HumanPlayer;
import ooga.players.Player;
import ooga.resources.Config;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.DukeApplicationTest;

public class CPUCPUGameViewTest extends DukeApplicationTest {

  private String gameType = "ConnectFour";
  private String language = "English";
  private String mode = "Light";
  private List<String> playerNames = Arrays.asList("playerA", "playerB");
  private List<String> playerTypes = Arrays.asList("Human", "Human");

  private MainController mainController;
  private GameController gameController;

  private Button saveButton;
  private Button backButton;

  private Button runButton;
  private Button stepButton;
  private Button pauseButton;

  @Override
  public void start(Stage stage) {
    gameController = new GameController(
        gameType,
        language,
        mode,
        playerNames,
        playerTypes,
        mainController
    );
    stage.setScene(gameController.getGameView()
        .getGameViewScene(Config.GAMEVIEW_DEFAULT_WIDTH, Config.GAMEVIEW_DEFAULT_HEIGHT));
    stage.show();

    saveButton = lookup("Save File").query();
    backButton = lookup("Back to Home").query();

    runButton = lookup("Run").query();
    stepButton = lookup("Step").query();
    pauseButton = lookup("Pause").query();
  }

  @Test
  public void findAllViewElementsTest() {
    HBox topPanel = lookup(
        String.format(Config.VIEW_ID_SEARCH, Config.GAMEVIEW_TOP_PANEL_ID)).query();
    HBox bottomPanel = lookup(
        String.format(Config.VIEW_ID_SEARCH, Config.GAMEVIEW_BOTTOM_PANEL_ID)).query();
    GridPane scoreBoard = lookup(
        String.format(Config.VIEW_ID_SEARCH, Config.GAMEVIEW_SCOREBOARD_PANEL_ID)).query();
  }

  @Test
  public void clickOnSaveButtonTest() {
      clickOn(saveButton);
  }

  @Test
  public void clickOnBackButtonTest() {
    clickOn(backButton);
  }

  @Test
  public void runButtonTest() {
    clickOn(runButton);
  }

  @Test
  public void stepButtonTest() {
    clickOn(stepButton);
  }

  @Test
  public void pauseButtonTest() {
    clickOn(pauseButton);
  }

  @Test
  public void animationTest() {
    clickOn(runButton);
    clickOn(pauseButton);

    clickOn(stepButton);
    clickOn(stepButton);
    clickOn(stepButton);

    clickOn(runButton);
    clickOn(pauseButton);
  }

  @Test
  public void stepUntilWinTest() {
    for (int i = 0; i < 50; i += 1){
      clickOn(stepButton);
    }
    Assertions.assertTrue(gameController.getGameView().getIsWin());
  }
}
