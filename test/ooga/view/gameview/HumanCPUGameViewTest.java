package ooga.view.gameview;

import java.util.Arrays;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import ooga.controller.GameController;
import ooga.controller.MainController;
import ooga.players.HumanPlayer;
import ooga.players.Player;
import ooga.resources.Config;
import org.junit.jupiter.api.Test;
import util.DukeApplicationTest;

public class HumanCPUGameViewTest extends DukeApplicationTest {

  private String gameType = "Gomoku";
  private String language = "English";
  private String mode = "Light";
  private List<String> playerNames = Arrays.asList("playerA", "playerB");
  private List<String> playerTypes = Arrays.asList("CPU", "CPU");

  private int defaultWidth = Config.GAMEVIEW_DEFAULT_WIDTH;
  private int defaultHeight = Config.GAMEVIEW_DEFAULT_HEIGHT;

  private MainController mainController;
  private GameController gameController;

  private Button saveButton;
  private Button backButton;

  @Override
  public void start(Stage stage) {
    gameController = new GameController(
            gameType,
            language,
            mode,
            playerNames,
            playerTypes,
            mainController
    );
    stage.setScene(gameController.getGameView()
        .getGameViewScene(Config.GAMEVIEW_DEFAULT_WIDTH, Config.GAMEVIEW_DEFAULT_HEIGHT));
    stage.show();

    saveButton = lookup("Save File").query();
    backButton = lookup("Back to Home").query();
  }

  @Test
  public void findAllViewElementsTest() {
    HBox topPanel = lookup(
        String.format(Config.VIEW_ID_SEARCH, Config.GAMEVIEW_TOP_PANEL_ID)).query();
    HBox bottomPanel = lookup(
        String.format(Config.VIEW_ID_SEARCH, Config.GAMEVIEW_BOTTOM_PANEL_ID)).query();
    GridPane scoreBoard = lookup(
        String.format(Config.VIEW_ID_SEARCH, Config.GAMEVIEW_SCOREBOARD_PANEL_ID)).query();
  }

  @Test
  public void clickOnButtonsTest() {
    clickOn(saveButton);
  }

  @Test
  public void clickOnBackButtonTest() {
    clickOn(backButton);
  }


}
