package ooga.view;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import ooga.controller.MainController;
import ooga.error.ParserException;
import ooga.resources.Config;
import org.junit.jupiter.api.Test;
import util.DukeApplicationTest;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.*;

class MainViewTest extends DukeApplicationTest {

    private MainController controller;
    private MainView view;
    private Button loadButton, createButton;
    private MenuBar game, matchUp, language, color;
    private ResourceBundle resources;

    @Override
    public void start(Stage stage) {
        controller = new MainController();
        resources = ResourceBundle.getBundle(String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, "English"));
        stage.setScene(controller.start().setUpMain(Config.DEFAULT_WIDTH, Config.DEFAULT_HEIGHT));
        stage.show();
        view = controller.start();
        loadButton = lookup("Load File").query();
        createButton = lookup("Start Game").query();
        game = lookup("#GameType").query();
        matchUp = lookup("#GameMode").query();
        language = lookup("#Language").query();
        color = lookup("#Color").query();
    }

    @Test
    public void loadButtonTest() {
        clickOn(loadButton);
        view.fileInput(new File("data/exceptiondatafile/gametype_value_invalid.sim"));
        //assertEquals("From key Game Type, value Blah is invalid!", getDialogMessage());
    }

    @Test
    public void createButtonErrorTest() {
        clickOn(createButton);
        assertThrows(Exception.class, () -> view.sendNewData());
    }

    @Test
    public void generateDataTest() {
        clickMenuBars("Checkers", "Player vs Player", "English", "Dark");
        writeInputsToDialog("First");
        writeInputsToDialog("Second");
        assertTrue(view.getNames().get(0).equals("First"));
        assertTrue(view.getNames().get(1).equals("Second"));
    }

    @Test
    public void nameInputFailedTest() {
        clickMenuBars("Checkers", "Player vs CPU", "English", "Dark");
        writeInputsToDialog("");
        assertEquals(Config.CONTROLLER_PROPERTIES, getDialogMessage());
    }

    @Test
    public void handlePlayerAndComputerTest() {
        clickMenuBars("Checkers", "Player vs CPU", "English", "Dark");
        writeInputsToDialog("First");
        assertTrue(view.getNames().get(0).equals("First"));
        assertTrue(view.getNames().get(1).equals("CPU"));
    }

    @Test
    public void handleComputersTest() {
        clickMenuBars("Checkers", "CPU vs CPU", "English", "Dark");
        assertTrue(view.getNames().get(0).equals("CPU 1"));
        assertTrue(view.getNames().get(1).equals("CPU 2"));
    }

    private void clickMenuBars(String type, String mode, String lang, String col) {
        clickOn(game).clickOn(String.join("#", type));
        clickOn(matchUp).clickOn(String.join("#", mode));
        clickOn(language).clickOn(String.join("#", lang));
        clickOn(color).clickOn(String.join("#", col));
        clickOn(createButton);
    }
}