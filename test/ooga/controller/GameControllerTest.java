package ooga.controller;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import ooga.resources.Config;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import util.DukeApplicationTest;

public class GameControllerTest extends DukeApplicationTest {

  private String gameType = "ConnectFour";
  private String language = "English";
  private String mode = "Light";
  private List<String> playerNames = Arrays.asList("playerA", "playerB");
  private List<String> playerTypes = Arrays.asList("Human", "Human");

  private MainController mainController;
  private GameController gameController;

  @Override
  public void start(Stage stage) {
    gameController = new GameController(
        gameType,
        language,
        mode,
        playerNames,
        playerTypes,
        mainController
    );
    stage.setScene(gameController.getGameView()
        .getGameViewScene(Config.GAMEVIEW_DEFAULT_WIDTH, Config.GAMEVIEW_DEFAULT_HEIGHT));
    stage.show();
  }

  @Test
  public void checkGameStatusTest() {
    // No win condition at start of the game
    Assertions.assertFalse(gameController.getGameView().getIsWin());
  }

  @Test
  public void checkSavePipelineTest() {
    File file = new File("data/savedatafile/gamecontrollertest");
    gameController.saveClicked(file);

    // Checks simpacket save directory
    Assertions.assertTrue(gameController.getSIMPacket().getInitialState().contains("savedatafile"));
  }
}
