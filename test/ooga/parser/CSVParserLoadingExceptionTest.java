package ooga.parser;

import java.util.List;
import java.util.ResourceBundle;
import ooga.parser.packet.Packet;
import ooga.parser.packet.SIMPacket;
import ooga.error.ParserException;
import ooga.resources.Config;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CSVParserLoadingExceptionTest {

  private String language = Config.ENGLISH_LANGUAGE;
  private ResourceBundle languageResources = ResourceBundle.getBundle(
      String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
  private CSVParser csvParser = new CSVParser();
  private String dataPath = "data/exceptiondatafile/%s";
  private String testSource = "testSource";

  public SIMPacket setUpSIMPacket(String filepath) {
    SIMPacket simPacket = new SIMPacket(Config.SIMPARSER_REQUIRED_KEYS);
    simPacket.setAuthor("AuthorValue");
    simPacket.setGameType("Gomoku");
    simPacket.setPlayerTurn("A");
    simPacket.setPlayerNames(List.of("A", "B"));
    simPacket.setPlayerTypes(List.of("Human", "Human"));
    simPacket.setPlayerIDs(List.of("1", "2"));
    simPacket.setInitialState(filepath);
    return simPacket;
  }

  @Test
  public void CSVParserEmptyRowTest() {
    // Row number is not a valid number
    String filePath = String.format(dataPath, "invalid_row_number.csv");
    SIMPacket simPacket = setUpSIMPacket(filePath);

    checkException(
        simPacket,
        Config.CSVPARSER_NON_NUMERIC,
        "a is not a number!"
    );
  }

  @Test
  public void CSVParserNoRowColTest() {
    // No first line (row and col)
    String filePath = String.format(dataPath, "no_row_col_first_line.csv");
    SIMPacket simPacket = setUpSIMPacket(filePath);

    checkException(
        simPacket,
        Config.CSVPARSER_INVALID_NUM_ELEMENTS,
        "Number of elements should be 2, got 10!"
    );
  }

  @Test
  public void CSVParserEmptyLineTest() {
    // Row and column number are zero
    String filePath = String.format(dataPath, "zero_element_row_col.csv");
    SIMPacket simPacket = setUpSIMPacket(filePath);

    checkException(
        simPacket,
        Config.CSVPARSER_ZERO_ELEMENTS,
        "Zero elements are given where they are not suppose to (0,0)!"
    );
  }

  @Test
  public void CSVParserInvalidStateTest() {
    // There is a non valid ID in the grid
    String filePath = String.format(dataPath, "non_valid_state.csv");
    SIMPacket simPacket = setUpSIMPacket(filePath);

    checkException(
        simPacket,
        Config.CSVPARSER_NON_VALID_STATE_ELEMENT,
        "[0, 1, 2] does not contain the given element 10!"
    );
  }

  public void checkException(Packet packet, String supposedResourceKey,
      String supposedMessage) {
    ParserException exception = Assertions.assertThrows(ParserException.class, () -> {
      csvParser.loadFile(packet);
    });
    String resourceKey = exception.getMessage();
    Assertions.assertTrue(resourceKey.equals(supposedResourceKey));

    String message;
    if (exception.getValue() == null) {
      message = String.format(languageResources.getString(resourceKey), exception.getKey());
    } else {
      message = String.format(languageResources.getString(resourceKey), exception.getKey(),
          exception.getValue());
    }
    Assertions.assertTrue(
        message.equals(supposedMessage));
  }
}
