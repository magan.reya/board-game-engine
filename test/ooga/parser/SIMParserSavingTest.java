package ooga.parser;

import java.util.List;
import java.util.ResourceBundle;
import ooga.parser.packet.SIMPacket;
import ooga.resources.Config;
import org.junit.jupiter.api.Test;

public class SIMParserSavingTest {

  private String language = Config.ENGLISH_LANGUAGE;
  private ResourceBundle languageResources = ResourceBundle.getBundle(
      String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
  private SIMParser simParser = new SIMParser();
  private String dataPath = "data/savedatafile/%s";
  private String testSource = "testSource";

  @Test
  public void SIMParserValidSavingTest() throws Exception {
    SIMPacket simPacket = new SIMPacket(Config.SIMPARSER_REQUIRED_KEYS);
    simPacket.setAuthor("AuthorValue");
    simPacket.setGameType("Gomoku");
    simPacket.setPlayerTurn("A");
    simPacket.setPlayerNames(List.of("A", "B"));
    simPacket.setPlayerTypes(List.of("Human", "Human"));
    simPacket.setPlayerIDs(List.of("5", "10"));
    simPacket.setInitialState(String.format(dataPath, "sim_save_1"));

    simParser.saveFile(simPacket);
  }
}
