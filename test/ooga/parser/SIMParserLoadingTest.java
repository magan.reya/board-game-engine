package ooga.parser;

import java.util.ResourceBundle;
import ooga.parser.packet.SIMPacket;
import ooga.parser.packet.MainViewLoadPacket;
import ooga.resources.Config;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SIMParserLoadingTest {
  private String language = Config.ENGLISH_LANGUAGE;
  private ResourceBundle languageResources = ResourceBundle.getBundle(
      String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
  private SIMParser simParser = new SIMParser();
  private String dataPath = "data/%s/default_board.sim";
  private String testSource = "testSource";

  @Test
  public void SIMParserGomokuValidLoadTest() throws Exception {
    // Valid load
    String filePath = String.format(dataPath, "gomoku");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    simParser.loadFile(mainViewLoadPacket);
    SIMPacket simPacket = simParser.getSimPacket();

    Assertions.assertEquals(simPacket.getAuthor(), "Norah");
    Assertions.assertEquals(simPacket.getGameType(), "Gomoku");
    Assertions.assertEquals(simPacket.getPlayerNames().toString(), "[default_player_A, default_player_B]");
    Assertions.assertEquals(simPacket.getPlayerTypes().toString(), "[Human, Human]");
    Assertions.assertEquals(simPacket.getPlayerTurn(), "default_player_A");
    Assertions.assertEquals(simPacket.getInitialState(), "data/gomoku/default_board.csv");
  }

  @Test
  public void SIMParserCheckersValidLoadTest() throws Exception {
    // Valid load
    String filePath = String.format(dataPath, "checkers");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    simParser.loadFile(mainViewLoadPacket);
    SIMPacket simPacket = simParser.getSimPacket();

    Assertions.assertEquals(simPacket.getAuthor(), "Norah");
    Assertions.assertEquals(simPacket.getGameType(), "Checkers");
    Assertions.assertEquals(simPacket.getPlayerNames().toString(), "[default_player_A, default_player_B]");
    Assertions.assertEquals(simPacket.getPlayerTypes().toString(), "[Human, Human]");
    Assertions.assertEquals(simPacket.getPlayerTurn(), "default_player_A");
    Assertions.assertEquals(simPacket.getInitialState(), "data/checkers/default_board.csv");
  }

  @Test
  public void SIMParserConnectFourValidLoadTest() throws Exception {
    // Valid load
    String filePath = String.format(dataPath, "connectfour");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    simParser.loadFile(mainViewLoadPacket);
    SIMPacket simPacket = simParser.getSimPacket();

    Assertions.assertEquals(simPacket.getAuthor(), "Norah");
    Assertions.assertEquals(simPacket.getGameType(), "ConnectFour");
    Assertions.assertEquals(simPacket.getPlayerNames().toString(), "[default_player_A, default_player_B]");
    Assertions.assertEquals(simPacket.getPlayerTypes().toString(), "[Human, Human]");
    Assertions.assertEquals(simPacket.getPlayerTurn(), "default_player_A");
    Assertions.assertEquals(simPacket.getInitialState(), "data/connectfour/default_board.csv");
  }

  @Test
  public void SIMParserOthelloValidLoadTest() throws Exception {
    // Valid load
    String filePath = String.format(dataPath, "othello");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    simParser.loadFile(mainViewLoadPacket);
    SIMPacket simPacket = simParser.getSimPacket();

    Assertions.assertEquals(simPacket.getAuthor(), "Norah");
    Assertions.assertEquals(simPacket.getGameType(), "Othello");
    Assertions.assertEquals(simPacket.getPlayerNames().toString(), "[default_player_A, default_player_B]");
    Assertions.assertEquals(simPacket.getPlayerTypes().toString(), "[Human, Human]");
    Assertions.assertEquals(simPacket.getPlayerTurn(), "default_player_A");
    Assertions.assertEquals(simPacket.getInitialState(), "data/othello/default_board.csv");
  }
}
