package ooga.parser;

import java.util.ResourceBundle;
import ooga.parser.packet.Packet;
import ooga.parser.packet.MainViewLoadPacket;
import ooga.error.ParserException;
import ooga.resources.Config;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SIMParserLoadingExceptionTest {

  private String language = Config.ENGLISH_LANGUAGE;
  private ResourceBundle languageResources = ResourceBundle.getBundle(
      String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
  private SIMParser simParser = new SIMParser();
  private String dataPath = "data/exceptiondatafile/%s";
  private String testSource = "testSource";

  @Test
  public void SIMParserEmptyValueTest() {
    // Key-value pair has an empty value
    String filePath = String.format(dataPath, "key_value_empty.sim");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    checkException(
        mainViewLoadPacket,
        Config.SIMPARSER_KEY_EMPTY_VALUE,
        "Key GameType string is empty!"
    );
  }

  @Test
  public void SIMParserMissingKeyTest() {
    // Missing player turn
    String filePath = String.format(dataPath, "key_missing.sim");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    checkException(
        mainViewLoadPacket,
        Config.SIMPARSER_KEYS_MISSING,
        "Keys [PlayerTurn] is missing!"
    );
  }

  @Test
  public void SIMParserInvalidKeyTest() {
    // Invalid key
    String filePath = String.format(dataPath, "key_invalid.sim");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    checkException(
        mainViewLoadPacket,
        Config.SIMPARSER_INVALID_KEY,
        "Key PlayerInvalidKey is invalid!"
    );
  }

  @Test
  public void SIMParserGameTypeValueInvalidTest() {
    // Given gametype value is not part of the game types
    String filePath = String.format(dataPath, "gametype_value_invalid.sim");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    checkException(
        mainViewLoadPacket,
        Config.SIMPARSER_INVALID_VALUE,
        "From key GameType, value Blah is invalid!"
    );
  }

  @Test
  public void SIMParserNumPlayersInvalidTest() {
    // Given game has invalid number of players
    String filePath = String.format(dataPath, "num_players_invalid.sim");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    checkException(
        mainViewLoadPacket,
        Config.SIMPARSER_NUM_PLAYERS_INVALID,
        "Number of player should be 2, got 1!"
    );
  }

  @Test
  public void SIMParserInvalidCSVFileTest() {
    // Given game has invalid number of players
    String filePath = String.format(dataPath, "file_missing.sim");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    checkException(
        mainViewLoadPacket,
        Config.SIMPARSER_MISSING_FILE,
        "Cannot find InitialState. File data/exceptiondatafile/sample_false.csv is missing!"
    );
  }

  @Test
  public void SIMParserInvalidSIMFile() {
    // Given game has invalid number of players
    String filePath = String.format(dataPath, "sample.csv");
    MainViewLoadPacket mainViewLoadPacket = new MainViewLoadPacket(filePath);

    checkException(
        mainViewLoadPacket,
        Config.INVALID_FILE,
        "data/exceptiondatafile/sample.csv is not a .sim file!"
    );
  }


  public void checkException(Packet packet, String supposedResourceKey, String supposedMessage) {
    ParserException exception = Assertions.assertThrows(ParserException.class, () -> {
      simParser.loadFile(packet);
    });
    String resourceKey = exception.getMessage();
    Assertions.assertTrue(resourceKey.equals(supposedResourceKey));

    String message;
    if (exception.getValue() == null) {
      message = String.format(languageResources.getString(resourceKey), exception.getKey());
    } else {
      message = String.format(languageResources.getString(resourceKey), exception.getKey(),
          exception.getValue());
    }
    Assertions.assertTrue(
        message.equals(supposedMessage));
  }
}
