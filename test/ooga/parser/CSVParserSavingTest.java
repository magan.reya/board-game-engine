package ooga.parser;

import java.util.List;
import java.util.ResourceBundle;
import ooga.parser.packet.CSVPacket;
import ooga.parser.packet.Packet;
import ooga.parser.packet.SIMPacket;
import ooga.resources.Config;
import org.junit.jupiter.api.Test;

public class CSVParserSavingTest {
  private String language = Config.ENGLISH_LANGUAGE;
  private ResourceBundle languageResources = ResourceBundle.getBundle(
      String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
  private CSVParser csvParser = new CSVParser();
  private String dataPath = "data/savedatafile/%s";
  private String testSource = "testSource";

  public SIMPacket setUpSIMPacket(String filepath) {
    SIMPacket simPacket = new SIMPacket(Config.SIMPARSER_REQUIRED_KEYS);
    simPacket.setAuthor("AuthorValue");
    simPacket.setGameType("Gomoku");
    simPacket.setPlayerTurn("A");
    simPacket.setPlayerNames(List.of("A", "B"));
    simPacket.setPlayerTypes(List.of("Human", "Human"));
    simPacket.setPlayerIDs(List.of("1", "2"));
    simPacket.setInitialState(filepath);
    return simPacket;
  }

  @Test
  public void CSVParserValidSavingTest() throws Exception {
    String filePathLoad = String.format(dataPath, "valid.csv");
    Packet simPacket = setUpSIMPacket(filePathLoad);

    csvParser.loadFile(simPacket);

    CSVPacket csvPacket = csvParser.getCSVPacket();
    String filePathSave = String.format(dataPath, "valid_output");
    csvPacket.setFilePath(filePathSave);

    csvParser.saveFile(csvPacket);
  }
}
