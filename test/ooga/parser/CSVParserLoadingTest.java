package ooga.parser;

import java.util.List;
import java.util.ResourceBundle;
import ooga.parser.packet.CSVPacket;
import ooga.parser.packet.SIMPacket;
import ooga.resources.Config;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CSVParserLoadingTest {

  private String language = Config.ENGLISH_LANGUAGE;
  private ResourceBundle languageResources = ResourceBundle.getBundle(
      String.format(Config.DEFAULT_RESOURCE_PACKAGE_PROPERTIES, language));
  private CSVParser csvParser = new CSVParser();
  private String dataPath = "data/%s/default_board.csv";
  private String testSource = "testSource";

  public SIMPacket setUpSIMPacket(String gameType, String filepath) {
    SIMPacket simPacket = new SIMPacket(Config.SIMPARSER_REQUIRED_KEYS);
    simPacket.setAuthor("Norah");
    simPacket.setGameType(gameType);
    simPacket.setPlayerTurn("default_player_A");
    simPacket.setPlayerNames(List.of("default_player_A", "default_player_B"));
    simPacket.setPlayerTypes(List.of("Human", "Human"));
    simPacket.setInitialState(filepath);
    return simPacket;
  }

  @Test
  public void CSVParserGomokuValidLoadTest() throws Exception {
    String filePath = String.format(dataPath, "gomoku");
    SIMPacket simPacket = setUpSIMPacket("Gomoku", filePath);

    csvParser.loadFile(simPacket);

    CSVPacket csvPacket = csvParser.getCSVPacket();

    Assertions.assertEquals(csvPacket.getNumRows(), 15);
    Assertions.assertEquals(csvPacket.getNumCols(), 15);
    String res = convertStatesToString(csvPacket.getStates());
  }

  @Test
  public void CSVParserCheckersValidLoadTest() throws Exception {
    String filePath = String.format(dataPath, "checkers");
    SIMPacket simPacket = setUpSIMPacket("Checkers", filePath);

    csvParser.loadFile(simPacket);

    CSVPacket csvPacket = csvParser.getCSVPacket();

    Assertions.assertEquals(csvPacket.getNumRows(), 8);
    Assertions.assertEquals(csvPacket.getNumCols(), 8);
    String res = convertStatesToString(csvPacket.getStates());
  }

  @Test
  public void CSVParserConnectFourValidLoadTest() throws Exception {
    String filePath = String.format(dataPath, "connectfour");
    SIMPacket simPacket = setUpSIMPacket("ConnectFour", filePath);

    csvParser.loadFile(simPacket);

    CSVPacket csvPacket = csvParser.getCSVPacket();

    Assertions.assertEquals(csvPacket.getNumRows(), 6);
    Assertions.assertEquals(csvPacket.getNumCols(), 7);
    String res = convertStatesToString(csvPacket.getStates());
  }

  @Test
  public void CSVParserOthelloValidLoadTest() throws Exception {
    String filePath = String.format(dataPath, "othello");
    SIMPacket simPacket = setUpSIMPacket("Othello", filePath);

    csvParser.loadFile(simPacket);

    CSVPacket csvPacket = csvParser.getCSVPacket();

    Assertions.assertEquals(csvPacket.getNumRows(), 8);
    Assertions.assertEquals(csvPacket.getNumCols(), 8);
    String res = convertStatesToString(csvPacket.getStates());
  }



  public String convertStatesToString(int[][] states) {
    if (states.length == 0 || states[0].length == 0) {
      return Config.EMPTY_STRING;
    }
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < states.length; i += 1) {
      for (int j = 0; j < states[0].length; j += 1) {
        sb.append(states[i][j]);
        sb.append(Config.SIMPARSER_LIST_REGEX);
      }
    }
    return sb.toString();
  }
}
