ooga
====

This project implements a player for multiple related games.

Names: Justin Jang, Reya Magan, Nate Zelter, Steven Cheng, Norah Tan


### Timeline

Start Date: Nov. 3rd, 2021.

Finish Date: Dec. 7th, 2021.

Hours Spent: On average, each student approximately 12 hours per week. 

### Primary Roles
Reya: Primarily on the backend, pair-programmed on Gomoku game and implemented Othello Human and CPU. Created a reflection factory
and set up class. Pair-programming on components and players.

Norah: Primarily on the backend, pair-programmed on Gomoku game and implemented Checkers Human and CPU. Created NeighborCells class to
get neighbors of pieces within grid. Pair-programming on components and players.

Nate: Primarily on the backend, pair-programmed on Gomoku game and implemented Connect 4 Human and CPU. Pair programmed on
components and players. Developed logging across project.

Steven: Primarily on Controller and View. Worked on GameController as well as connections between GameView and model. Worked on all the parsing and data file components. Created packets, simrules,
and csvrules for parsing of files and data collection. Created view elements and displays. Worked on cheat keys.

Justin: Primarily on Controller and View. Worked on MainController as well as connections between MainController, MainView, and GameController. Created view elements and displays. Implemented CellView and GridView
for each piece and the general game board. Worked on cheat keys.


### Resources Used
https://www.baeldung.com/java-8-streams-if-else-logic

https://www.worldothello.org/about/about-othello/othello-rules/official-rules/english

https://www.youtube.com/watch?v=l-hh51ncgDI


### Running the Program

Main class: Main.java

Data files needed: There are default .sim and .csv files that are loaded for each game. We also have the ability to have users load unique data files that show a current board and the game can start off that board.

Features implemented:

4 Games: Gomoku, Connect 4, Checkers, Othello

Load/Save Data Files

AI and Human players

ScoreBoard for games

Win Conditions

Light Up Valid Cells

Pause/Step/Run CPU vs CPU

Cheat Keys:

Save ( S ) (Steven)

Back ( B ) (Steven)

Run CPU vs CPU Gomoku ( G ) (Justin)

Run CPU vs CPU Connect4 ( 4 ) (Justin)

Run CPU vs CPU Checkers ( C )  (Justin)

Run CPU vs CPU Othello ( O ) (Justin)

Run Human vs CPU Gomoku ( 5 ) (Justin)

Run Human vs CPU Connect4 ( 6 ) (Justin)

Run Human vs CPU Checkers ( 7 ) (Justin)

Run Human vs CPU Othello ( 8 ) (Justin)


### Notes/Assumptions

Assumptions or Simplifications:

One thing that we made much simpler in the game was an invalid move. Because our algorithm is able to get every possible move,
we are able to connect the model to the controller and thus the view to light up these possible moves. In this way, we can simplify the issue
of incorrect moves by a user, because the view will only have on click events for lit up cells.

Interesting data files:

We have default data files for each game. However, an interesting functionality with data files is that a user can save the game at any point
and create a data file out of this. The user can then load this file and play as if they were "in the middle" of that game.

Known Bugs:

There are sometimes bugs in the displaying of the score and win conditions. We infer that there is some threading discrepancy between the view and the model,
as when the model declares the win, the view shows this win but does not always update the score board. This sometimes makes the win look incorrect - as the display is a step behind.
This does not happen often, but can occur sometimes.

Challenge Features:
We implemented AI players for each of our games using the MiniMax algorithm.

### Impressions

This was a really interesting project, especially because we had the flexibility to create anything. We not only applied design concepts learned in class and beyond,
but also learned about our strategy games and AI algorithms in the process. Overall it was a great learning experience, and we had strong design conversations.

