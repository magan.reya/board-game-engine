# API Review Lab
## December 1, 2021
## Nate Zelter (me, nrz3) with Jordan Castleman (partner, jlc153) from project team 7 

### Part 1
- How does your API encapsulate your implementation decisions? 
  - Our team's APIs hides all the game specifics (such as game rules for valid moves, end game
  condition, scoring policies) as it just interfaces with user input and posts results to the view
  side of our project. 
  
- What abstractions is your API built on?
  - We use the abstract Model class that sets up the APIs whereas there are actually implemented in
  the extended game classes (with different rules for valid moves, end game condition, scoring 
  policies). 

- What about your API's design is intended to be flexible? 
  - We keep our APIs rather general to allow it to be easily extended to more games. By excluding
  the specifics of how each game operates within our API, we allow it to be flexible and independent
  of the current games being played to that will be added to our game engine. 

- What exceptions (error cases) might occur in your API and how are they addressed?
  - There aren't many error cases that stem from our APIs. The ones that do are likely going to be
  related to improper user input (like clicking exactly on an edge or out of bound). As long as our
  Rules interface is correct, the API should provide correct calls regarding the Game's current
  status and board state. 

### Part 2
- How do you justify that your API is easy to learn?
  - Our API is easy to learn because it is streamlined/simple and utilizes intuitive naming
  conventions. Users won't get lost in our APIs because they are short and concise. They can be read
  and understood within only a few minutes.
  
- How do you justify that API leads to readable code? 
  - Again, because of simple and intuitive naming conventions, are APIs are hard to read. They often
  call private methods that encapsulate their inner-workings but that also have understandable names.

- How do you justify that API is hard to misuse? 
  - They are hard to misuse because they limited scopes of power and their results are always
  examined to confirm they are valid API calls. In other words, once an API call is made, the results
  must pass the smell test before they are fully incorporated into the state of our program. 

- Why do you think your API design is good (also define what your measure of good is)?
  - First and foremost, they are good because they function properly. They are easily integrated
  into our projects and are flexible for each of our games. Our APIs can also be extended to be
  applied to new games that we don't currently implement (such as Chess or Connect-5)!