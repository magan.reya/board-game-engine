# OOGA Backlog

### Team: 3

### Names: Steven Cheng, Nate Zelter, Norah Tan, Reya Magan, Justin Jang

### Use Cases

* Apply the rules to a cell: set the next state of a cell to dead by counting its number of
  neighbors using the Game of Life rules for a cell in the middle (i.e., with all of its neighbors)

```java
 OrderLine items=new OrderLine();
    if(order.isInStock(items)){
    total=order.getTotalPrice(items);
    }
```

* Move to the next generation: update all cells in a simulation from their current state to their
  next state

```java
 OrderLine items=new OrderLine();
    if(order.isInStock(items)){
    total=order.getTotalPrice(items);
    }
```

* Switch simulations: load a new simulation from a data file, replacing the current running
  simulation with the newly loaded one

```java
 OrderLine items=new OrderLine();
    if(order.isInStock(items)){
    total=order.getTotalPrice(items);
    }
```

* Make new game window

1. User enters the main loading page
2. User selects the language to display
3. User selects the Load button
4. If the User has not selected a language to display, error message pops up, telling User to select
   a language
5. Repeat step 4 until language selected
6. The Load button opens a FileChooser, allowing the user to upload a SIM file representing a Game's
   state
7. If the user selects cancel, error message pops up, prompting user to select a SIM file
8. Repeat step 8 until SIM file selected
9. Loads a new game window


* View high scores:

1. User enters the main loading page
2. User selects the language to display
3. User selects which game type's high score he wants to see
4. User selects the View High Scores Button
5. If the User has not selected a game type, error message pops up, telling User to select a game
   type
6. Repeat step 5 until game type selected
7. Pop-up appears with the selected game type's high score


* View current scoreboard:

1. User enters the main loading page
2. User selects the language to display
3. User selects the Load button
4. If the User has not selected a language to display, error message pops up, telling User to select
   a language
5. Repeat step 4 until language selected
6. The Load button opens a FileChooser, allowing the user to upload a SIM file representing a Game's
   state
7. If the user selects cancel, error message pops up, prompting user to select a SIM file
8. Repeat step 8 until SIM file selected
9. Loads a new game window
10. On the top right corner of the game window, a display will be seen with the players and their
    respective scores
11. Depending on the game type selected, score is updated by either the total number of turns for
    each player, or the total number of pieces left on the board
12. Every time a move is made, the scoreboard will most likely be updated


* Interact with Piece:

1. User enters the main loading page
2. User selects the language to display
3. User selects the Load button
4. If the User has not selected a language to display, error message pops up, telling User to select
   a language
5. Repeat step 4 until language selected
6. The Load button opens a FileChooser, allowing the user to upload a SIM file representing a Game's
   state
7. If the user selects cancel, error message pops up, prompting user to select a SIM file
8. Repeat step 8 until SIM file selected
9. Loads a new game window
10. Depending on the game type, if there is a piece already on the Grid, user can select that piece
    with their mouse, and it will light up


* See all possible moves:

1. User enters the main loading page
2. User selects the language to display
3. User selects the Load button
4. If the User has not selected a language to display, error message pops up, telling User to select
   a language
5. Repeat step 4 until language selected
6. The Load button opens a FileChooser, allowing the user to upload a SIM file representing a Game's
   state
7. If the user selects cancel, error message pops up, prompting user to select a SIM file
8. Repeat step 8 until SIM file selected
9. Loads a new game window
10. Depending on the game type, if there is a piece already on the Grid, user can select that piece
    with their mouse, and it will light up
11. Whether a piece is selected or there is no need to select a piece, all possible options will
    then light up in a different color than the grid to indicate all possible moves from that point
    forward


* Move to game result screens: Depending on the type of players and the outcome, transition to a
  win/lose screen

1. User enters the main loading page
2. User selects the language to display
3. User selects the Load button
4. If the User has not selected a language to display, error message pops up, telling User to select
   a language
5. Repeat step 4 until language selected
6. The Load button opens a FileChooser, allowing the user to upload a SIM file representing a Game's
   state
7. If the user selects cancel, error message pops up, prompting user to select a SIM file
8. Repeat step 8 until SIM file selected
9. Loads a new game window
10. Depending on the game type, if there is a piece already on the Grid, user can select that piece
    with their mouse, and it will light up
11. Whether a piece is selected or there is no need to select a piece, all possible options will
    then light up in a different color than the grid to indicate all possible moves from that point
    forward
12. After a move is played, the next player makes their move
13. Repeat step 12 until win condition is met for one of the players
14. If you are a player and you win, the win screen will override the current game window
15. If you are a player and you lose, the lose screen will override the current game window


* Make a (non-winning) move in the Connect 4 game:
    * The User is currently playing Connect 4, and they select an open cell to move their piece
      into.
    * This proposed change will be stored in the ooga.view, and the controller will get this information
      from the ooga.view and call the model
    * The model will first call the isEmpty() method to make sure that the cell that the user
      selected their piece to move into is empty, meaning that it is not already in the map
      containing pieces and their positions within the grid.
    * If the grid is empty, the rules will be run on this new piece to make sure that the user is
      completing a valid move in the scope of the Connect 4 rules, and that the game has not already
      been won.
    * If the move is valid, the open cell will be filled by the setPositionToBePlaced() method and
      the movePiece() method which will add the piece to the empty cell. The player's turn is over.
    * The update() method is called, and then the controller gets this to know that the turn is
      completed. This is then reflected in the ooga.view as well.


* Make a (winning) move in the Connect 4 game:
    * The User is currently playing Connect 4, and they select an open cell to move their piece
      into.
    * This proposed change will be stored in the ooga.view, and the controller will get this information
      from the ooga.view and call the model
    * The model will first call the isEmpty() method to make sure that the cell that the user
      selected their piece to move into is empty, meaning that it is not already in the map
      containing pieces and their positions within the grid.
    * If the grid is empty, the rules will be run on this new piece to make sure that the user is
      completing a valid move in the scope of the Connect 4 rules, and that the game has not already
      been won.
    * In this case the game has been won, so the open cell will be filled by the
      setPositionToBePlaced() method and the movePiece() method. The player's turn is complete, and
      the update() method will reflect that the game itself is over.
      (@Comment: Rules needs to determine if end-game condition is met. How do the two method calls differ?)
    * The controller will get this information, and also learn that the game is over. The controller
      will then tell the ooga.view that the game is over and the ooga.view will display this to the user,
      along with which player is the winner.


* Make an invalid move in the Connect 4 game:
    * The User is currently playing Connect 4, and they select an already filled cell to move their
      piece into.
    * This proposed change will be stored in the ooga.view, and the controller will get this information
      from the ooga.view and call the model.
    * The model will first call the isEmpty() method to make sure that the cell that the user
      selected their piece to move into is empty, but in this case that cell contains a piece that
      has already been registered in the map, so an error will be returned.
    * This error will be retrieved from the controller, which will signal to the ooga.view that an
      invalid move has been attempted. This will then be displayed to the user, and they will be
      prompted to make another move instead.


* A CPU player makes a winning move in the Connect 4 game:
    * The current player for the Connect 4 game is the CPU, and they implement a "smart move" which
      is the best move within the game algorithm.
    * This information is retrieved from the ooga.view by the controller, and then sent into the model.
    * The model checks to see that this move is valid by calling the isEmpty() method
    * Since the move is valid, the model runs the rules and sees that the CPU will win with this
      next move
    * The smartMove() method is ran in the model, and the update() method reflects this change and
      saves information that the game has been won.
    * The controller gets this information and sends it to the ooga.view, which notifies the players that
      the CPU has won.


* A player "regrets" their last move:
    * The current player for the Connect 4 game is a human, and they click the "Regret" button to
      undo their last move.
    * This button click is registered in the ooga.view, and the controller gets this information and
      sends it to the model.
    * The model already has a map with each Piece in the game and the (x,y) coordinate they are at.
      It deletes the last added piece from this map.
    * The update() method is then called, and the controller gets this information and sends it to
      the ooga.view.
    * The ooga.view removes this piece from the display, and prompts the user to redo their move by
      placing the piece elsewhere.


* A player "quits" the game:
    * The current player for the Connect 4 game clicks "Quit" signalling that they want the game to
      end.
    * This button click is registered in the ooga.view, and the animation is stopped and the user is
      prompted to start a new game.

[comment]: <> (Below 6 are from Norah)

* A player advances a piece in the Checkers game:
    - The User is currently playing Checkers and the game is in progress/already initialized.
    - The User first click on one cell on the board.
    - The frontend listens to this event, and call GameModel.setPositionToBeRemoved(int x, int y)
      which will return true if and only if the position that the user clicks is valid.
    - The User then click on another cell on the board to indicate where he or she wants the piece
      to be moved. The frontend listens to this event, and call GameModel.setPositionToBePlaced(int
      x, int y) which will return true if and only if the position that the user clicks is valid.
    - The frontend calls GameModel.update() to actually change the location of the piece on the
      board. At the end of GameModel.update(), the currentPlayerID is switched to the other player.
    - The frontend updates the GridView to display any changes.


* A CPU execute a move in the Checkers game:
    - Since at the end of GameModel.update(), the currentPlayerID is always switched to the other
      player, we can safely call update when it's CPU's turn.
    - The frontend calls GameModel.update() which will call smartMove() to change the location of
      one piece on the board.
    - The frontend updates the GridView to display any changes.


* A player needs a "hint" for a possibly successful move in the Checkers game:
    - The User clicks "Hint" button.
    - The frontend listens to this event and then call GameModel.giveHint()
    - GameModel.giveHint() will then run the same algorithm in smartMove() and return the location
      of the piece to be moved and where it should be placed to the frontend.
    - The frontend then displays the two locations on the screen: by either displaying a text
      message, or highlighting the piece and its new location on the board.


* A player upgrades one of its piece into a King:
    - The User selects a piece and select a new location for it to be placed. The frontend then
      calls update().
    - Inside GameModel.update(), check whether the piece will be moved to a place where it could be
      crowned to a King. If so, change the identity of the Piece.
    - The frontend updates the GridView to display any changes.


* A player saves the current game:
    - The User clicks "Save" button.
    - The frontend listens to this event and then call Parser.saveFile() to save the board to a CSV
      file.


* A player loads the current game:
    - The User clicks "load" button and click a CSV file to be read.
    - The frontend listens to this event and then call Parser.loadFile() to load the board from a
      previous game.

[comment]: <> (Below 6 are from Steven)

* Click load file button
    - When load file button is pressed, FileExplorer object is called and shows a panel to select
      your SIM file.
    - Upon clicking the SIM file, the SIMParser will verify if the SIM file is valid. If so, it will
      parse the SIM file and store it in a hashmap.
    - Once that is done, if there is a directory to the related CSV file, then it will load the CSV
      file as well.
    - The CSV file will be checked, if it is valid, it will generate a Grid object and pass it to
      the model.
    - The model will then notify the controller and ooga.view that a GridView can be generated.


* Click the save file button
    - When the save file button is pressed, a FileExplorer object is called and you can type in a
      name to save your current game progress in a SIM file and CSV file.
    - The View will ping the Controller, which will then ping the SIMParser and CSVParser to
      generate the attributed files.


* Select different types of CSS displays (Dark/Light mode)
    - There will be multiple properties files for displaying different CSS.
    - For now, we will assume only Dark and Light mode.
    - Upon selecting Light mode, the MainController will select the corresponding Light.css file to
      set up the background colors.

* Selecting languages
    - There will be multiple properties files for displaying different languages.
    - For now, we will assume only English, Spanish, and French.
    - Upon selecting English mode, the MainController will select the corresponding
      English.properties file as the properties file to utilize.

* Error in SIM/CSV file chosen
    - User selects a SIM file to generate.
    - The View passes that information to the Controller, which then passes it to the SIM and CSV parsers.
    - If the given file does not correspond correctly to the current game, the Parser will ping the Controller with the error.
    - Then, the Controller will ping the View and an error dialog will be displayed.

* Save file button is pressed when no file is loaded.
    - User presses the save file button.
    - The View passes that information to the Controller, which then passes it to the SIM and CSV parsers.
    - A boolean variable will store the current state of the file - if a file has not been selected, it will be false.
    - Upon seeing the boolean variable as false, the Controller will notify the View.
    - An error dialog will be displayed.

[comment]: <> (Below 6 are from Nate)

* Checkers: Capture an opponent's piece
    * See "A player advances a piece in the Checkers game" for sequence of how to move a piece.
    * In model.update(), apply Rule logic to determine if an opponent piece was captured via that move.
    * If so, update the Model's Point-to-Piece map by removing the captured piece's key.
    * Continue by switching CurrentPlayerID and call frontend to update GridView to reflect changes.

* Checkers: Turning into a "King":
    * In update() apply rule logic to determine a player's active piece just reached the opponents edge of the board.
    * If so, it becomes a "King" meaning it can move diagonally forwards or backwards any number of cells.
    * This change in the piece's ability will be reflected in an instance variable within the Piece.
    * Continue by switching CurrentPlayerID and call frontend to update GridView to reflect changes.

* Checkers: Get possible moves for a given piece:
    * The User is currently playing Checkers and the game is in progress/already initialized.
    * The User first clicks on one Cell on the board.
    * The frontend listens to this event, and call GameModel.setPositionToBeRemoved(int x, int y)
      which will return true iff the position that the User clicked is valid.
    * Rules is called to determine the Set of possible points that Piece can occupy next.
    * _Note_: This would be used as part of the CPU's minimax search and part of the User's getHint() option.

* Othello: A piece has become "Outflanked":
    * Within update(), iterate over all opponent Pieces to check if the most recent move now outflanks it.
    * Use Rules to recognize that a piece has become outflanked and update Point-to-Piece map to reflect the color change.
    * Make sure Rules checks diagonally, vertically, and horizontally; check for multiple outflanks.
    * Continue by switching CurrentPlayerID and call frontend to update GridView to reflect changes.

* Othello: Determine if player place a Piece in their turn:
    * At the start of a player's turn, determine if they are even eligible to place a Piece given that it must outflank some opponent piece(s).
    * Iterate over all uninitialized values in Point-to-Piece map and add empty Points to a Set.
    * Pass that Set to Rules to determine if any of the elements would outflank an opponent Piece.
    * If true, continue with currentPlayerID's turn. Otherwise, display message and switch currentPlayerID.

* Othello: "Track points" scoring mode:
    * Winner of a round gets (# of my pieces) - (# of opponent pieces) number of points.
    * This value would be calculated by Model after the end game condition is met. Model then calls an external handler to
      update a CSV file of the current rule profile which include data about the User's current score.
    * This value is added to and updated if/when the game continues. 
    