# OOGA Design Final
### Team: 3
### Names: Justin Jang, Steven Cheng, Nate Zelter, Norah Tan, Reya Magan
 
## Team Roles and Responsibilities
 
* Justin Jang: Justin was on the frontend/controller. He worked on the general architecture for the MainController and the MainView. These features mainly deal with inputting the values that are associated with creating a game, such as the game type, game mode, language, and color of the display as well as the ability to input names for the players. He also added the functionality to load a previously-saved file from the MainView into the GameController. He also made the general structure of the GridView and the CellView, as well as the PropertyChange pipeline to interact with. Finally, he added KeyInputs for cheat keys.  
 
* Steven Cheng: Steven was on the frontend/controller. He worked on the general architecture for the GameController and the GameView: all the logic in the playing of each board game and the connection to the model. Additionally, he also developed the general architecture of the Parser classes, both SIMParser and CSVParser.
 
* Nate Zelter: Nate was on the backend and helped design both the backend and controller's architecture. Nate spent most of his time on the APIs that were used between the backend and controller, as well as focusing on the games Gomoku and ConnectFour. Nate also helped with the implementation of the minimax algorithm as part of the CPU plyaer as well as refactoring the backend to extract methods and improve design. 
 
* Norah Tan: Norah was on the backend. She designed the structure for backend together with Reya and Nate, which includes Model classes, Player classes, the components package (Grid and Cell). She implemented the human vs human mode for GomokuModel with Reya, and completed the MiniMax CPU algorithm (interface) and initially applied it in GomokuModel. She also completed all the functionalities (both human mode and CPU mode) for CheckersModel. Norah did a lot of testing, debugging, discussion, and refactoring together with the backend team as well. 
 
* Reya Magan: Reya was on the backend. She worked with Nate and Norah for the general structure (Abstract Model, Player classes, Component Package). She worked on Gomoku human vs human with Norah. She completed the full functionality of Othello (human mode and CPU mode). She designed the ReflectionFactory and ReflectionSetUp classes to deal with all of the reflection within the game. She also refactored the abstract model to have the abstract ConnectModel extend from it so that two of the similar games (Gomoku and Connect Four) would have their own abstractions for better consideration of single responsibility and dependency inversion (She discussed these ideas with Steven). She assisted with some MainView components (with Justin) for a clearer intro screen. She worked on a lot of testing and discussion with the rest of the team.
 
 
## Design goals
 
#### What Features are Easy to Add 
What are the project's design goals?
* From the planning document, these were the goals we wanted to achieve:
    * Utilize SOLID principles throughout our program
        * **Achieved**: during our plan phase, we discussed thoroughly about good design architectures, and developed a well-thought-out program that is open for extensions for future additions. 
    * Built at least four well-designed board games
        * **Achieved**: we developed 4 games - Gomoku, Connect Four, Checkers, and Othello, which were all working and playable.
    * Create computer players to simulate opponents
        * **Achieved**: we developed three gamemodes for each game - Human vs Human, Human vs CPU, and CPU vs CPU. The CPUs utilized the Minimax algorithm and were good at finding the optimal next move.
    * Improve our design concepts -> inheritance for pieces, structures of view, etc
        * **Achieved**: we made sure that we had clever uses of inheritance and abstractions throughout our program. From the system diagrams, there is good use of submodules and clean organization.

 
## High-level Design
 
#### Core Classes

The core structure of our code starts with the interaction between the MainView and the MainController. This is where the data inputs, whether it be setting the specifications of the game to create a brand new game or by loading a pre-existing file of a game's state, are handled and sent to create a game. This game is created by first connecting the MainController with a GameController instance, which then has an instance of the GameView, Model, SIM parser, CSV parser, and Players. The SIM and CSV parsers are used to parse the data that is entered if we loaded a data file from the MainView. These classes are subclasses of the Parser parent class, and all of the specific instances of which data is parsed is abstracted into its respective subclasses, such as player names, player types, and etc. From there, using the data values, the Players are created, where the Player is a superclass of the CPUPlayer and the HumanPlayer, depending on the type inputted from the MainView.

For the GameView and Model construction, the GameView can be abstracted into its three different game types: CPU vs CPU, Human vs Human, and Human vs CPU. These GameView instances each have a GridView, which includes a Map of Points to CellViews. These classes communicate with each other through PropertyObservable, which interacts with the GameController when a certain Piece or Rectangle is clicked. There is also a KeyInputs class that deals with the cheat keys, which are abstracted out to distinguish the possible key clicks for the MainView versus the GameView.

For the Model, the Model is an abstracted class that houses the information about the games as they happen. This Model class has subclasses for each of the four games: Gomoku, Othello, ConnectFour, and Checkers. For Gomoku and ConnectFour, since the rules are very similar, an abstracted ConnectModel was created. The Model class has a Grid instance, which houses a Map of Points to Cells. The Grid is passed from the Model to the GameController to update the values through the ImmutableGrid interface, which only has getter methods to ensure encapsulation while passing the data structure around. Finally, an Algorithms class was abstracted to help with the CPU implementations between the four games. 
 
## Assumptions that Affect the Design

The major assumption that was made was that the Gomoku and Connect Four games could be grouped together. 
These games are similar in logic (5 in a row, 4 in a row), they just differ in what constitutes a valid move within the game (Connect 4 has gravity involved as a user can only select the lowest unfilled square within a column to place their piece). These games had similar methods that were put into the general abstract model class, so they were extracted out of this and placed into an extended abstract class called "ConnectModel" to deal with any "in a row" type of games.
 
#### Features Affected by Assumptions

Moving the Connect Four and Gomoku specific methods into a separate abstract class extended from the general abstract class not only maintained the functionality within our game, but also improved the design. Before, we had the common methods of Connect Four and Gomoku within the high level abstract model class. This reduced duplicate code, but did not follow the dependency-inversion principle as the abstract model class was relying on the subclasses (because these two methods were only used dependent on which games were being played). Moving these methods into the ConnectModel abstract subclass and having Gomoku and Connect Four Models extend from those allowed for better design without affecting functionality.
 
 
## Significant differences from Original Plan
Part of our initial plan was to create "Piece" objects that would occupy the game board and represent a players pieces that have been player. This pieces would contain some integer value to be associated with the player who the piece belongs to, as well as the level of the piece for a game like checkers where pieces can be standard (moving in only one direction) or a "King" which can move in both directions. However, as time went on, we decided to scrap this idea and use "Cell" objects instead. In this iteration, our "Board" constisted of a hashmap of Points to Cells, where Cell values were intitialized to 0 to represent an empty cell, and a player who placed a piece on a given cell would then change the Cell's value to the integer associated with that player, and this change would be reflected on the front end of our project. This new design was more robust and allowed for simpler APIs between the Components of our project and the Controller which would then interface with the backend and frontend to utilize said pieces.  
 
## New Features HowTo
Describe, in detail, how to add new features to your project, especially ones you were not able to complete by the deadline. 
 
#### Easy to Add Features
If we are adding a new type of connect game, where, for example, the winning condition is 6 pieces in a row, instead of 4 (ConnectFour) or 5 (Gomoku), we can simply create a NewGameModel class that extends from the abstract ConnectModel class and inherit most of the model features and dynamics. 

On the frontend side, we will need to add corresponding game type selections and data files in the mainview. But this will only be some minor changes and we do not need to create any new view classes at all because of our structure is able to adapt to this new game easily. 
 
#### Other Features not yet Done
We were planning to add some other algorithms (different types of CPU players), but we did not because of the time limit. These algorithms can be esaily added because of our interface design.

Since we already have the algorithm package, we can easily add other CPU algorithms as interfaces to this package, and let corresponding Model classes implement these interfaces. For example, we can create a dumbAlgorithm interface and add method playDumb (compared with miniMaxAlgorithm's playSmart). The dumbPlayer will just randomly choose a possible location to place. 
