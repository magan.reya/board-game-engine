# API Review

## Names: Steven Cheng (sc618) and Christen

## Part 1

### How does your API encapsulate your implementation decisions?

The GameController and GameView and Model API has all the public methods that other classes can
interface with . The GameController can access the Model and GameView API for calls.

### What abstractions is your API built on?

There is a lot of common functionality and good design between our games, so the API is clean and
can be extended easily.

### What about your API's design is intended to be flexible?

The API is really flexible for the implementation of all of our games. We have a structure that
works for similarities, and can be easily overridden for any unique components.

### What exceptions (error cases) might occur in your API and how are they addressed?

A lot of parser exceptions are taken into account. If we are given invalid sim and csv files, then
the SIMParser and CSVParser will pick that up.

## Part 2

### How do you justify that your API is easy to learn?

The methods are really clear and their names illuminate their functionality. It is easy to
understand the step by step process of how the gamecontroller and gameview and model interact with one another.

### How do you justify that API leads to readable code?

We have good naming for the APIs.

### How do you justify that API is hard to misuse?

The APIs have clear naming and limited scope. Hence they are hard to misuse.

### Why do you think your API design is good (also define what your measure of good is)?

Good API design is one that is easy to understand and organized in such a way that can be easily
extracted. The APIs we have are good in terms of readability and incredible flexibility.