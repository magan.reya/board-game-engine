#API Review
## Names: Richard Deng (rld39) and Justin Jang (jbj30)

##Part 1
###How does your API encapsulate your implementation decisions?
The View API encapsulates the implementation decisions of each individual class by the nature of the responsibility that
is given for each method that is called for each class in question. When constructing each method to be in a specified
class, we made sure that we considered what the individual responsibility of that class was and, therefore, what methods
should be defined to give that class certain responsibilities.

###What abstractions is your API built on?
The API view methods abstract key features related to the view and controller classes. For instance, the Parser classes
and their methods are abstracted to make sure that the commonly-used methods are not repeated across these classes in
terms of functionality. This also holds true for the GameView, which is abstracted to have distinguishing features
between each subclass.

###What about your API's design is intended to be flexible?
The parameters and data fields that are being utilized involve their superclasses instead of individually dealing with
subclasses as their declaring data type, which would limit the overall scope and flexibility of that particular method
in dealing with varying data values.

###What exceptions (error cases) might occur in your API and how are they addressed?
Some errors that could occur on the view and controller are incorrect values passed from the propertyChangeListener,
incorrect file formats, incomplete user inputs, and other miscommunications with the declaration of specific objects.

##Part 2
###How do you justify that your API is easy to learn?
My APIs are easy to learn because there are plenty of comments that help to describe what each method is doing as well
as descriptive parameters that help clarify exactly what is expected of each method.

###How do you justify that API leads to readable code?
These APIs lead to readable code due to the fact that the method and parameter names follow common naming conventions
and the spacing is clear.

###How do you justify that API is hard to misuse?
I think that the APIs are hard to misuse due to the fact that I think each method has a clearly-defined purpose, and the
expectation of that method aligns well with the specified return type, which can be abstracted/modified depending on
changing expectations.

###Why do you think your API design is good (also define what your measure of good is)?
I would say that my definition of good API design is the ability to predict what methods are going to be developed as
well as being able to handle as many different cases/new conditions as possible, and I think that the API methods are
abstracted and broad enough to allow for more specified abstractions derived from the general version if necessary.