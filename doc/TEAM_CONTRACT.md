# OOGA Team Contract
### Team Name: The Coding Devils (Team 3)
### Originally Created: November 4th, 2021
### Last Updated: November 9, 2021.


## As a project team, we promise to:
* Respect each other's ideas and work.
* Participate actively in the design and implementation of our project.
* Communicate with the team regularly to address design, progress, and milestones.
* Aim high and take this project seriously.
* Provide honest feedback to improve our shared project and individual abilities.

## If someone on our team breaks one or more of these promises, the team may:
* Have a team meeting to better understand the situation and discuss how to learn from it.
* Aim to resolve the issue internally.
* Contact the course leaders to get involved.


## Team Procedures

### Team meetings
* _Generally_: 
  - Update Google Docs [To-Do list]([https://docs.google.com/document/d/1c9JAfwPIzKMv2zLApGE5eKyvCCXs_jWcoZbFrNsuqz4/edit?usp=sharing).
  - Text often with small updates, progress, or questions.
  - Plan meeting agendas, location, and time ahead.
  - Reserve study rooms through [Duke library](https://library.duke.edu/using/library-spaces).
  - Be sure to listen to other's opinions during meetings.
  
* _Types of Meetings_:
  - "Sub-team meetings": internally within either frontend or backend sub-team, multiple times per week.
  - "Devils meetings": with the entire project team, at least once per week to address milestones and integration.
  - "Retrospectives": with the entire project team, after each Sprint.

### Communications
* Our preferred channel of communication is text message, use the group chat we have.
* We would like to have team meetings in person when possible
  * Resort to Zoom or calling for remote work.
  * Be open-minded to pair programming, can be really productive using CodeWithMe.
* Aim to respond as soon as possible.
  * let’s try to over communicate. 
  * Even if you’re not available, just explain your situation and get back to it when possible.
* We need consensus about big decisions, ideally we will all be agreeing on something before committing to that choice. 
  * Be willing to compromise and discuss multiple perspectives
  * All members are encouraged to have a say and express their thoughts.

### Helping each other
* We should not be afraid to ask for help once we have tried to come to a solution on our own.
  * Don’t just ask immediately, do some research/debugging first then reach out if you're still stuck.
  * Conversely, be willing to help out and go the extra mile to support a team member.
* We should be willing to help as needed and to be caring. 
  * Share knowledge so we can grow together.
  * Take initiative to check in with team members to maintain cohesion and productivity.
* If someone’s code is not to the expectations of the course, kindly say that the code should be fixed in this way
  * but do change other people’s code without their permission
  * Let's be invested in one another and hold ourselves to high standards (which is actualized through giving honest and respectful feedback).
* For pair programming, aim to split the commits between both programmers to make sure everyone gets clear representation for commit history.

### Commitments
* Do as much as you can (pull your own weight) and don’t be afraid to ask for help if needed
  * If there are problems, bring them up.
  * It's reasonable to ask questions about design/conceptual understanding, but try to do Java-specific code on your own (unless really difficult)
* Reference our [To-Do list]([https://docs.google.com/document/d/1c9JAfwPIzKMv2zLApGE5eKyvCCXs_jWcoZbFrNsuqz4/edit?usp=sharing) document.
* Constantly update group chat with progress and concerns.
* Collectively delegate tasks in the beginning of each Sprint that we agree on, then work, update, then discuss progress and plan next steps. 

