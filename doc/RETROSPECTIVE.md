# Lab Retrospective Discussion
### Nate, Reya, Steven, Justin, Norah


## Project's current progress
* Model: on track - but want to refactor 
* Controller: slightly behind 
* View: slightly behind

## Current level of communication
* Norah: just right 
* Steven: fine 
* Reya: just right 
* Nate: solid level of communication 
* Justin: good level of communication, updating group chat with any major merge requests

## Satisfaction with team roles
* Steven: fine 
* Nate: good but can improve with design planning 
* Reya: good, but I think in general we need to focus more on refactoring 
* Justin: good, but need to do more work on my end 
* Norah: fine


## Teamwork that worked well
* Norah: communication is great. We know what other people are doing 
* Nate: I agree with Norah; good collective awareness of progress 
* Steven: Very good communication. 
* Reya: Definitely the communication is great, and I think everyone has been good about getting their assigned tasks done. 
* Justin: I think that we are bringing up great design decisions that need to be addressed



## Teamwork that could be improved
* Norah: maybe when we diverge in designing ideas and couldn't decide where to proceed, we can just start coding in our own branch and then adjust/decide. 
* Steven: I agree with the above, we should develop through code for substitutions/changes to existing infrastructure. 
* Reya: I think that we should refactor as we code, so that we don’t have to make huge design changes after having a lot implemented already 
* Nate: I agree with the above, especially that we should all be on the same page about significant design choices before implementing functionality and features. 
* Justin: I think that we could definitely make these addressed differences separately to see if they work. Also, we should definitely refactor code


## Teamwork to improve next Sprint
* Steven: have a clearer agenda for discussion, try not to get into the weeds too much. 
* Justin: Make more contributions, communicate those, meet deadlines more often, meet with members to discuss design issues more often 
* Nate: Create a better understanding of milestone interdependencies and progress pipeline. 
* Reya: Refactor a lot, and discuss design ideas so that it is easy to add functionality anywhere 
* Norah: Agree with Nate, for the backend, we need to figure out a clear interdependency relations for models, players and the game controller. 
