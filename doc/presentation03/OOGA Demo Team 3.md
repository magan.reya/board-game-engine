# OOGA Demo Team 3

---

# Part I: Introduction

* Team 3: The Coding Devils
* Team members: 
    * Justin Jang: frontend/controller
    * Steven Cheng: frontend/controller
    * Nate Zelter: backend
    * Norah Tan: backend
    * Reya Magan: backend


---

# Part II: Functionality

---

# Part III: Design

---

# Overall Design

---

![](https://i.imgur.com/EM5famj.jpg)

---

# Frontend

---

![](https://i.imgur.com/bJgnRIB.png)

---

# Backend

---

![](https://i.imgur.com/fir2Ce4.png)

---

![](https://i.imgur.com/LKdaRBk.png)

---

![](https://i.imgur.com/bN0dZJA.png)

---

![](https://i.imgur.com/Yd0tu0P.jpg)

---

# APIs and Use Cases

![](https://i.imgur.com/FBtuih8.png)

---

![](https://i.imgur.com/gqkzHd5.png)

---

![](https://i.imgur.com/KkkjnCK.png)

---

# Part IV: Team

---

# Planning

Contrast the completed project with where you planned it to be in your initial Wireframe and the initial planned priorities and Sprints with the reality of when things were implemented.

---

# Development

Individually, share one thing each person learned from using the Agile/Scrum process to manage the project.


---

# Timeline

Show a timeline of at least four significant events (not including the Sprint deadlines) and how communication was handled for each (i.e., how each person was involved or learned about it later).


* Event 1: Fininishing basic backend and frontend separately
* Event 2: Connecting backend with frontend and some refactoring
* Event 3: Adding CPU modes to each game 
* Event 4: Refactoring and completion (logs, cheat keys)

---

# Learnings

Individually, share one thing each person learned from trying to manage a large project yourselves. 


---

# Teamwork

Describe specific things the team actively worked to improve on during the project and one thing that could still be improved. 

---

# Team Culture

Individually, share one thing each person learned about creating a positive team culture.

---

# Team Contract

Revisit your Team Contract to assess what parts of the contract are still useful and what parts need to be updated (or if something new needs to be added).

---

# Communication and Problem-solving

Individually, share one thing each person learned about how to communicate and solve problems collectively, especially ways to handle negative team situations.




