The following is a list of APIs that
were either changed or added in this sprint:

1. Components:
```java 
@Deprecated
   void setNextState(int ID);

@Deprecated
int getNextState(int ID);
```
We deprecated the setNextState and getNextState APIs within our components package CellSetupAPI, as throughout the game we only worry about
the current state of a piece within the game, not what it could be next. We thought originally that this functionality would be useful
for the CPU algorithm, but it was unnecessary.

```java
Set<Point> getAllPieces();
```
We added the getAllPieces() method to the ImmutableGrid API as it was useful in our CPU algorithms to get all the current pieces within the grid so that the CPU could
effectively make its next move. We had a getAllPoints() method already, but this returns empty points as well.

2. Reflection Factory
```java
List<Player> setUpPlayers(List<String> playerNames, List<String> playerTypes);

Model setUpModel(String gameType, int[][] states);

GameView setUpGameView(String gameType, String language, String mode, ImmutableGrid grid,
        Player currPlayer, List<Player> players, List<String> playerTypes);

@Deprecated
    public GridView setUpGridView(String gameType)
```
We added set up functions for reflection into our APIs as a lot of reflection within classes requires parameter organization before 
reflection is done. These APIs allow for the ReflectionSetUp class to easily implement this without duplicated code throughout
the project. We had added a setUpGridView API, but we now only have one standard GridView, so there is no reflection needed for that.

3. Model
```java
void playSmart();

void setSmartPlace(Point point);

List<Point> getAllPossiblePlaces(List<Object> possibles);

void tryMiniMax(Point point, int playerID);

void recoverFromMiniMax(Point point);

default int recursiveMiniMax(int depth, int playerID, int maxDepth);

```
We created a new interface: MiniMaxAlgorithm for all of the CPU within our game. We had a few thoughts regarding this. First,
the CPU algorithms are similar across three games (Gomoku, Connect Four, and Othello), so we thought to create a default API
for this that we could override for unique functionality in Checkers. The difference in these games is the tryMiniMax() and recoverFromMiniMax() APIs. 
These methods attempt scoring for all possible valid moves, and clear any simulated actions during the algorithm respectively. We wanted to make this an interface because
the CPU relies on our already existing model APIs, but we wanted to show its separation. As a result, we have each extended subclass implement the tryMiniMax and recoverFromMiniMax
methods uniquely.


   