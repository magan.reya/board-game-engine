# OOGA API Review
# Scribe: Norah Tan (Team 3)
# Partner: Tim Jang (Team 6)
# Date: Dec 1st, 2021

## Summary of Discussion
Our teams both pick strategy genre but Team 6 is designing one complicated game Chess and we are designing four simple games. 
So we share a pretty similar design, regarding placing pieces on board, execute step, and check end game condition. 
However, our implementation also takes into account the CPU player with minimax/minmax algorithm to have more variation. 
Their implementation uses more interfaces to divide responsibilities as their pieces have many roles. 

## Discussion Note
* How does your API encapsulate your implementation decisions? 
For backend, the GameController initiate Model and Player(s) instances and call public methods in Model to execute every step according to the player type and also switch player. 

* What abstractions is your API built on? 
Our backend API is mainly built on the abstraction of Model and Player. Frontend API is mainly built on GameView and GridView.