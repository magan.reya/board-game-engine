# Example Games
## OOGA Team 3 (The Coding Devils)

### Four Games:
####1. Connect Four
   - Game Example: [link](https://www.youtube.com/watch?v=utXzIFEVPjA)
   1. Initialization:
      - Start with an empty 8x8 grid, vertically oriented.
      - One player is randomly selected to go first, they place the first piece in the grid.
   2. Turns:
      - Players drop one of their distinctly colored pieces in one of the 8 columns which drops to the lowest possible
      row within that column. Then it is the other players turn. 
      - This process repeats until the end game condition is met.
   3. End Game Condition:
      - A player wins when at least four of their pieces "connect" in any direction (in-a-row, in-a-column, on-a-diagonal).
      - If the 8x8 grid is fully populated without any winners, the game is declared a tie. 
   
####2. Mancala
   - Game example: [link](https://www.youtube.com/watch?v=-A-djjimCcM)
   1. Initialization:
      - 2 by 6 board, with one extra grid element on the left and right side of the board which represent each player's store of points.
      - Player A has the top row, and the left spot is their store of points.
      - Player B has the bottom row, and the right spot is their store of points.
   2. Turns:
      - One player starts by picking up all the rocks in a grid spot.
      - In a counter-clockwise direction, place a rock one at a time into each adjacent grid spot.
      - If player A passes the left spot, then place a rock in it. If they pass the right spot, ignore it.
      - Likewise for player B.
      - A player keeps continuing the previous steps until they finishing placing a rock into an empty spot/wells.
   3. End Game Condition:
      - When one row is completely devoid of rocks, then the game ends.
      - Tally up the total number of rocks in the left and right store of points and whoever has the most rocks wins.

####3. Checkers:
   - Game Example: [link](https://www.ultraboardgames.com/checkers/game-rules.php)
   1. Initialization:
      - 8 by 8 Board; alternate white and dark Cells; only black Cells are used
      - One player starts with 12 black pieces and the other starts with 12 white pieces, placed closest to themselves.
   2. Move and capture: 
      - The pieces always move diagonally and single pieces are always limited to forward moves.
      - A piece making a non-capturing move may move only one square.
      - To capture a piece of your opponent, your piece leaps over one of the opponent's pieces and lands in a straight diagonal line on the other side. This landing square must be empty.
   3. Upgrade a piece to a King:
      - When a piece reaches the furthest row, it is crowned and becomes a king. 
      - Kings are limited to moving diagonally but can move both forward and backward.
      - Kings may combine jumps in several directions (forward and backward) on the same turn.
   4. End game condition:
      - A player wins the game when the opponent cannot make a move. 

####4. Gomoku
   1. Initialization:
      - It is played using a 15×15 board while in the past a 19×19 board was standard.
      - There are two players (black, white)
   2. Move
      - Players alternate turns placing a stone of their color on an empty intersection. 
      - Black plays first and must be placed in the center. 
   3. End Game Condition:
      - The winner is the first player to form an unbroken chain of five stones horizontally, vertically, or diagonally.
      - These are called overlines.
   
### Game Similarities
* 2 players, discrete alternating turns
* All have some board component with placeable pieces
* All use the minmax/minimax algorithm for automated players
* Gomoku, Connect Four, and Checkers are played on square boards (8 x 8, or 15 x 15)

Gomoku vs Connect Four:
* Very similar winning condition (4 in a row for Connect 4, 5 in a row for Gomoku)
* Differences are that Connect 4 uses gravity, whereas Gomoku happens on a "flat" plane

### Game Differences 
- Gomoku and Connect Four are "x in a row" type of games, whereas Checkers and Mancala differ in their 
end game conditions.
- Checkers and Mancala can have multiple moves in one turn, but Gomoku and Connect Four require
one move per turn
- Connect Four and Gomoku have no board setup whereas Mancala and Checkers require initialization
- Piece abilities in Checkers can change whereas there are no changes in the other games


### Example of events 

- Action of playing (Gomoku and Connect 4, human vs. human)
  1. Human clicks the board -> different interactions for each game
  2. Game verifies valid move, then instantiates a new element (Piece/Cell) in Grid.
  3. Checks any end game condition (see if a human player has won) -> different interactions for each game
     1. If a player has won, sends that information to the ooga.view to display success message.
  4. View checks Grid, sees new element, and updates the grid ooga.view.
  5. Change to another player
  6. Repeat steps 1 - 5 until game ends.

- Action of playing (Checkers, human vs. human)
  1. Game sets up a Grid, passes this grid to the View.
     1. If Checkers, then Grid will have pieces already set up.
  2. Human selects piece to move, Game tells View to highlight all possible moves for selected piece.
  3. Human moves piece to highlighted destination, updates Grid and View.
  4. Game checks end game condition and passes to other Human.
  5. Repeat steps 2 - 4 until game ends. 



- Controller (abstract)
- View (abstract)
- Parse (abstract)
- Model (abstract)
- Player (abstract)
  - CPU
  - Human
- Rules (interface)
- Grid
- Piece