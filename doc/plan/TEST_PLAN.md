# OOGA Test Plan Discussion

### Team: 3

### Names: Steven Cheng, Nate Zelter, Norah Tan, Reya Magan, Justin Jang

## Project Testability

### Specific Strategies

One specific strategy that we implemented for API testing was to implement exception handling, as we
wanted to test bad input files by testing public methods that had try-catch statements to handle
exceptions and display that to the user on the main Game window. Through this strategy, we made it
so that multiple facets were more easily testable: the exception handling messages/triggers and when
the data was passed through smoothly.

Another strategy that we implemented for API testing was to make the methods more dynamic by
returning the value of the specified object that it was manipulating. This was implemented because
we wanted to make the methods achieve multiple purposes, such as setting values and getting the
modified values. This allows ease of access for testing as well as more readability and reduced
lines of code for methods that are unnecessary.We also implemented necessary getter and setter
methods with the hopes of using encapsulation to limit the score of the use of these values, but
also consider the fact that we need these methods for other purposes besides testing.

### Test Scenarios

1. One turn in any given game:

It is important to test what one turn would look like within our 4 different games. We could test
them in the following three ways:

* The turn is valid, but the game is not won:
    * This test would take in the coordinates of an open cell, and try to map a new Piece to that
      cell. After that, this test would check to see if the map of Point, Piece has this new Piece
      in it. This should pass, as the cell that the user tried to add a new Piece to was empty.
* The turn is invalid:
    * This test would take in the coordinates of a cell within the game board that has already been
      filled. The test would try to add this new Piece object to the already filled cell, but we
      would return an exception that the move is invalid. This test would check for that exception,
      and thus it would pass as the program will return an invalid move to the user.
* The turn is valid, and the game is won:
    * This test would take in the coordinates of an open cell, and try to map a new Piece to that
      cell. After that, this test would check to see if the map of Point, Piece has this new Piece
      in it. Then, the test would check to see if the value of getGameStatus from the rules was not
      0, as if it was not 0 the game would have been won. This would pass as the value of the
      getGameStatus() in this winning case would be either 1 or 2 depending on which player won.

2. Test whether the CPU algorithms (minmax/minimax) in all four games are working correctly. Since
   minmax is a determined algorithm, not randomized, therefore we can manually calculate the most
   desirable move/place which the algorithm should give and then verify. Other than these general
   test cases, here is a specific scenario that also worth tested:

- When the CPU can make a winning move (for example, 5 pieces in a row in Gomoku; 4 pieces in a row
  in Connect Four), will the algorithm successfully identify that.

3. Testing Parsing Functionalities
It is important to test if a given file has the correct game type properties and correct file formats.
* CSVParser
  * Test whether the given file ends with ".csv". If not, throw "Not a CSV file" error!
  * Test whether the given CSV file has the correct Grid dimensions. If not, throw " Grid dimensions specified does not correspond to the number of elements!"
  * Test whether the given CSV file has non-numeric numbers. If yes, throw "An element is not a number!"
  * Test whether the given CSV file has non-represented numbers. If yes, throw "An element is a number not represented in the game!"
  * Test whether the number of rows and columns inputted are invalid. If yes, throw "Number of rows and columns inputted are invalid!"
  * Test whether the number of rows and number of columns are flipped around. If yes, throw "Number of rows and number of columns are flipped!"
* SIMParser
  * Test whether the given file ends with ".sim". If not, throw "Not a SIM file" error!
  * Test whether the given file is the correct game type. If not, throw "SIM file error: Incorrect game type file!"
  * Test whether the SIM file is missing attributes (e.g. game type). If yes, throw "Missing game type!"




