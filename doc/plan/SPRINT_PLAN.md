# OOGA Sprint Plan Discussion
### Team: 3
### Names: Steven Cheng, Nate Zelter, Norah Tan, Reya Magan, Justin Jang

## Project Progress

#### Sprint 1 (Test)
- Model: Complete all board games (mechanics, rules, process) for multiplayer.
  - Start with developing the Grid and Game logic, alongside how the Pieces will work.
- View: Get one or two games with multiplayer (2 people) working.
  - Start off with developing a simple UI of what the display will look like.
  - Then, develop the controller - view - parser pipeline.

#### Sprint 2 (Basic)
- Model: Complete automated opponents (with “smart” strategies) for at least 2 games.
  - Start developing and looking into CPU algorithms.
  - Then, try implementing a CPU algorithm into the model pipeline.
- Get the rest of the views implemented and add features for CPU
  - Finish up the remaining views.
  - Start error handling (show dialog errors).

#### Sprint 3 (Complete)
- Model: finish automated opponents in all games, incorporate high scores and save data/progress.
  - Clean up all the previous features.
  - Incorporate high scores.
- Add more complexity (more rules, more interactions with the view, etc)
  - Clean up save and load file pipeline, making sure that all error handling has been implemented.
  - Finish all functionality.
