package model.game;

import components.Grid;

public abstract class Game {
    private boolean empty;
    private int nextState;
    private int currentState;
    private Grid myGrid;
/*
  private List<Integer> myPlayers;
  private int myCurrPlayerID;
  private int PLAYER_HUMAN = 0;
  private int PLAYER_CPU = 1;

  private Grid myGrid;
  private int[] myPosToBeRemoved;
  private int[] myPosToBePlaced;
*/

    public void setUpModel(){}
   public void setUpGrid(){}

    public Grid getGrid() {
        return myGrid;
    }

    public void setUpPieces(){}

   public boolean isEmpty (int x, int y){return empty;}

  public boolean setPositionToBeRemoved(int x, int y) {return false;} // return false if the position is not valid

  public boolean setPositionToBePlaced(int x, int y) {return false;} // return false if the position is not valid

   public int getPieceNextState(int x, int y){return nextState;} // return -1 if empty; state in Piece will only be 0 and 1

   public int getPieceCurrentState(int x, int y){return currentState;}

   public void update(){};

   protected void addPiece(){}

  protected void movePiece(){}

   protected void smartMove(){}

}
