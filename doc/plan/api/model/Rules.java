package model;

import components.Piece;

public interface Rules {

  public Boolean isValidMove(Piece piece);

  public int getGameStatus();
  /**
   * returns 0 if game is NOT OVER
   * returns 1 if player 1 has won the game
   * returns 2 if player 2 has won the game
   */

}
