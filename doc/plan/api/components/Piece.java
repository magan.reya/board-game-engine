package components;

public class Piece {
  private int myX;
  private int myY;
  private int myValue;

  public Piece(int x, int y, int value) {
    this.myX = x;
    this.myY = y;
    this.myValue = value;
  }

  public int getX() {
    return myX;
  }

  public void setX(int myX) {
    this.myX = myX;
  }

  public int getY() {
    return myY;
  }

  public void setY(int myY) {
    this.myY = myY;
  }

  public int getValue() {
    return myValue;
  }

  public void setValue(int myValue) {
    this.myValue = myValue;
  }


}