package components;

import java.awt.*;
import java.util.Map;

public abstract class Grid {
  private Map<Point, Piece> myBoard;

  public void setMyBoard(Map<Point, Piece> myBoard) {
    this.myBoard = myBoard;
  }

  // let's improve this method by encapsulating it
  public Map<Point, Piece> getMyBoard(){
    return this.myBoard;
  }

}
