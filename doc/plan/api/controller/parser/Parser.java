package controller.parser;

import java.beans.PropertyChangeEvent;
import java.io.File;
import java.util.Map;

public interface Parser {
    // Function to be called for when loadFile button is called via the PropertyListener
    void loadFile(PropertyChangeEvent evt) throws Exception;

    // Function to load file given a string filepath
    Map<String, Object> loadFile(String filepath) throws Exception;

    // Function to be called for when saveFile button is called via the PropertyListener
    void saveFile(PropertyChangeEvent evt) throws Exception;

    // Function to save file given a string filepath
    File saveFile(String filepath) throws Exception;
}
