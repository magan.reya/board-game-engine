package controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import components.Grid;
import model.game.Game;
import view.game.GameView;

public class GameController extends PropertyObservable implements PropertyChangeListener {
    /*
    private GameView ooga.view;
    private GameModel game;
    private CSVParser csvParser;
    private SIMParser simParser;
    */

    // calls the next state in the model update() and displays the next state in the ooga.view.
    public void update(){ }

    // Getters
    public Game getModel(){ return this.getModel();}

    public GameView getView(){ return this.getView();}

    // Setters
    public void setModel(Game model){ }

    public void setView (GameView view){ }

    public void setViewGrid(Grid grid) { }

    // Parser interfaces
    public void saveFile(){ }

    public void loadFile(){ }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

    }
}
