package controller;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class PropertyObservable {
    /*
    private PropertyChangeSupport pcs;
    */

    // Connects an observable/observer edge between two classes
    // String name is a unique identifier for each observable/observer
    // PropertyChangeListener is a class that acts as a observer
    public void addObserver(String name, PropertyChangeListener l) {}

    // The observable class will notify the observer class, given an addObserver has been set up
    // String name is a unique identifier for each observable/observer
    // Object o is an object/class that is passed from observable to observer
    public void notifyObserver(String name, Object o) {}
}
