package view.grid;

import controller.PropertyObservable;
import javafx.scene.Group;
import javafx.scene.shape.Polygon;
import view.PieceView;

import java.awt.*;
import java.beans.PropertyChangeListener;

public abstract class GridView extends PropertyObservable implements PropertyChangeListener {

    /*
    private Map<Point, PieceView> pieces;
    private int numRows, numColumns;
    private Group pieceGroup;
    private Point startPosition;
    private double sideLength;
     */

    //initializes the GridView Group structure with the number of rows, columns, and the Piece's Polygon shape (exception occurs when no gridview object)
    public void initializeGrid(int rows, int columns, String polygonType, PropertyChangeListener listener) throws NullPointerException {}

    //creates the piece at each point in the Grid group (exceptions occur when Point accesses outside of bounds or no gridview object)
    public Polygon createPiece(Point point, PropertyChangeListener listener) throws IndexOutOfBoundsException, NullPointerException {return null;}

    //changes the color of all possible moves that can be made given a certain orientation at a specified point
    //exceptions occur when Point accesses outside of bounds or no gridview object
    public boolean lightUpPossibleCells(Point point, List possibleLights, Color color) throws IndexOutOfBoundsException, NullPointerException {return false;}

    //creates the border that separates the different Polygon Pieces (exception occurs when no gridview object)
    public void createBorder(Color color, List thicknessSpecifications) throws NullPointerException {}

    //returns the Group object representing the Pieces in a GridView object
    public Group getGrid() {return null;}

    //updates a specified Piece at a point with a new color for its new state (exceptions occur when Point accesses outside of bounds or no gridview object)
    public PieceView updatePiece(Point point, Color color) throws IndexOutOfBoundsException, NullPointerException {return null;}
}
