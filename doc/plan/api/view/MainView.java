package view;

import controller.PropertyObservable;
import javafx.scene.Node;
import javafx.scene.Scene;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class MainView extends PropertyObservable implements PropertyChangeListener {
    /*
    private Scene myScene;
    private ViewElements elements;
    private MenuBar games, languages, options;
     */

    //sets up the MainView scene with specified dimensions for the window size
    public Scene setUpMain(int width, int height) {return null;}

    //makes the dropdown menubars with the different options for each menubar
    private Node create() {return null;}

    //makes the button prompts to input values from the menubars or do other related actions
    private Node load() {return null;}

    //displays the error alert when an exception is caught
    public void errorDisplay(String message) {}

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

    }
}
