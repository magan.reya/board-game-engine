package view.game;

import java.beans.PropertyChangeEvent;
import javafx.scene.Node;
import javafx.scene.Scene;

public class GomokuGameView extends GameView {

  @Override
  public Scene setUpGameView() {
    return null;
  }

  @Override
  protected Node makeTopPanel() {
    return null;
  }

  @Override
  protected Node makeBottomPanel() {
    return null;
  }

  @Override
  protected Node makeLeftPanel() {
    return null;
  }

  @Override
  protected Node makeCenterPanel() {
    return null;
  }

  @Override
  protected void errorDisplay() {

  }

  @Override
  public void updatePieceView() {

  }

  @Override
  protected void start() {

  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {

  }
}
