package view.game;

import controller.PropertyObservable;
import java.awt.Point;

import java.beans.PropertyChangeListener;
import javafx.scene.Node;
import javafx.scene.Scene;
import ooga.view.GridView;

public abstract class GameView extends PropertyObservable implements PropertyChangeListener {

  // Sets up the Scene object (display) for the game ooga.view
  public abstract Scene setUpGameView();

  // Creates the top panel within the game ooga.view, (for now, we decided to show game name, player's turn, scoreboard)
  protected abstract Node makeTopPanel();

  // Creates the bottom panel within the game ooga.view (for now, we decided to have load file, save file buttons)
  protected abstract Node makeBottomPanel();

  // Creates the left panel (functionality TBD)
  protected abstract Node makeLeftPanel();

  // Create the center panel (will hold the Grid)
  protected abstract Node makeCenterPanel();

  // Shows an error dialog for exceptions
  protected abstract void errorDisplay();

  // Updates the pieces in the GridView
  public abstract void updatePieceView();

  // Starts the program
  protected abstract void start();

  private int myValue;
  public void selectPiece(){}
  public Point getPiece(){return new Point(0,0); }
  public int getValue(){return myValue;}
}
