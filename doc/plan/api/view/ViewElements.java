package view;

import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Polygon;
import javafx.stage.FileChooser;

public class ViewElements {

  // Creates a slider for changeable parameters
  public Slider makeSlider(){ return null; }

  // Display for a Piece object (pieces in a game)
  public Polygon makePieceShape(){return null; }

  // Makes the menubar for menuitems
  public MenuBar makeMenuBar(){ return null; }

  // Makes the individual menuitems within a menuBar
  public MenuItem makeMenuItem(){ return null; }

  // Creates a ComboBox object
  public ComboBox makeComboBox() { return null; }

  // For loading and saving files
  public FileChooser makeFileChooser() {return null; }

  // Creating a scoreboard ooga.view
  public Group makeScoreBoard() { return null; }

  // Creates a label object
  public Label makeLabel() { return null; }

  // Creates a text field input
  public TextField makeTextField(String name) { return null; }

  // Make a color picker for color choosing
  public ColorPicker makeColorPicker() { return null; }

  // Make a button
  public Button makeButton() { return null; }

  // Create an alert
  public Alert showError(String message) { return null; }

  // Helper methods for creating displays
  public VBox makeVBox() { return null; }

  // Helper methods for creating displays
  public HBox makeHBox() { return null; }
}
