import controller.GameController;
import components.Piece;
import model.Rules;
import model.game.ConnectFourModel;
import model.game.Game;
import view.game.ConnectFourGameView;
import view.game.GameView;

public class MakeANonWinningMove {

    public static void main(String[] args){
        Game model = new ConnectFourModel();
        GameView gameView = new ConnectFourGameView();
        GameController controller = new GameController();
        Rules rules = new Rules() {
            @Override
            public Boolean isValidMove(Piece piece) {
                return null;
            }

            @Override
            public int getGameStatus() {
                return 0;
            }
        };

        //the user selects the piece that is not yet on the grid and chooses where to place it
        gameView.selectPiece();

        //the controller gets the piece to be moved and its value
        controller.getView().getPiece();
        controller.getView().getValue();

        //the controller calls the model
        controller.getModel().setUpModel();

        //the model checks to make sure that the piece is being moved to a valid spot
        model.isEmpty(0,0);

        //if it is valid the piece is set up, in this case we will say it is (0,0,1)
        model.setUpPieces();

        //the rules are called to ensure that the move is valid
        rules.isValidMove(new Piece(0,0,1));

        //we check to see if the game is over
        rules.getGameStatus();

        //the open cell position to be moved to is set, this will also call the movePiece() method
        model.setPositionToBePlaced(0,0);

        //the model updates its changes
        model.update();

        //the controller updates and updates the ooga.view
        controller.update();
    }
}
