import controller.GameController;
import controller.PropertyObservable;

import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class LoadSIMFile extends PropertyObservable {
  private ResourceBundle myResources;
  /**
   * LoadFile is called when the Controller notifies the SIMParser that a file has been loaded.
   *
   * @param evt is a packet sent from the View class to SIMParser
   * @throws Exception is thrown when the file given is not a SIM file
   */
  private void loadFile(PropertyChangeEvent evt) throws Exception {
    String fileString = "" + evt.getNewValue();
    // Loads file information into a hashtable
    Map<String, Object> infoMap = loadFile(fileString);
    GameController controller = new GameController();
    // Pipelines information back to the controller
    notifyObserver("SIMParsing", controller);
  }


  /**
   * Helper loadFile function
   *
   * @param fileString takes in a string path for the file
   * @return a mapping of all the key-value pairs in the SIM file
   * @throws FileNotFoundException occurs if the file is not found
   */
  public Map<String, Object> loadFile(String fileString) throws Exception {
    // Checks valid file
    if (!fileString.endsWith(".sim")) {
      throw new Exception(myResources.getString("SIMErrorIncorrectFileType"));
    }

    // Instantiates hashtable and loads file
    Map<String, Object> infoMap = new HashMap<>();
    File file = new File(fileString);

    // Iterate through line by line
    Scanner in = new Scanner(file);
    while (in.hasNextLine()) {
      String nextLine = in.nextLine();

      // If the line is a comment, ignore
      if (nextLine.isEmpty() || nextLine.startsWith("#")) {
        continue;
      }

      // Split key value pairs, add to infoMap
      String[] lineSplitted = nextLine.split("=");
      infoMap.put(lineSplitted[0], lineSplitted[1]);
    }
    return infoMap;
  }

}