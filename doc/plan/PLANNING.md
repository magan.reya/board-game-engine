# OOGA Plan Discussion
### Team: 3
### Names: Steven Cheng, Nate Zelter, Norah Tan, Reya Magan, Justin Jang

## Game type: Mild Strategy Games (changed from Platformer)
- Checkers
- Connect4
- Gomoku (五子棋)
- Mancala


## Project goals
Utilize SOLID principles throughout our design
Built at least four well-designed board games
Try to implement multiplayer vs cpu implementation structure
Create computer players to simulate opponents
Improve our design concepts -> inheritance for pieces, structures of ooga.view, etc


## Project Emphasis
Start with plan by using Figma to design connections between classes and how that would look on the ooga.view
Focus on design more than functionality
SOLID!!!

## Project Extensions
- Game initialization:
    - Could be an empty board
    - Could load a previous saved board from data file (eg. csv)
- When a human player is playing, pop up “suggestion” for next step (based on our “smart” strategies)


## Project Progress

#### Sprint 1 (Test)
- Model: Complete all board games (mechanics, rules, process) for multiplayer. Add error handling
- View: Get one or two games with multiplayer (2 people) working.

#### Sprint 2 (Basic)
- Model: Complete automated opponents (with “smart” strategies) for at least 2 games
- Get the rest of the views implemented and add features for CPU

#### Sprint 3 (Complete)
- Model: finish automated opponents in all games, incorporate high scores and save data/progress. Add error handling.
- Add more complexity (more rules, more interactions with the ooga.view, etc)


## Project Roles
- Nate - backend
- Norah - backend
- Reya - backend initially, might change depending on needs later (like controller)
- Justin - frontend + controller
- Steven - frontend + controller
