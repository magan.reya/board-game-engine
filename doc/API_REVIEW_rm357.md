#API Review
## Names: Reya Magan (rm357) and Amr Tagel Din

##Part 1
###How does your API encapsulate your implementation decisions?
The model API has all of the movement details for the game. The controller
can access this API to deal with all the placing of pieces within the game.
This is all seen in the model as public methods.

###What abstractions is your API built on?
We have a lot of common functionality between our games, so the API shows
this as all of the extended game classes implement these APIs.

###What about your API's design is intended to be flexible?
As mentioned above, the API is really flexible for the implementation of all
of our games. We have a structure that works for similarities, and can be easily
overridden for any unique functionality.

###What exceptions (error cases) might occur in your API and how are they addressed?
If a user selects an invalid placement for a piece, a custom "Invalid Move" exception is
thrown. The invalid piece logic differs per game.

##Part 2
###How do you justify that your API is easy to learn?
The methods are really clear and their names illuminate their functionality.
It is easy to understand the step by step process of how the model makes a move.
###How do you justify that API leads to readable code?
We have good naming for the APIs.

###How do you justify that API is hard to misuse?
The APIs are only within our classes, so it is hard to find them. It would be good to extract them into
an interface for clarity so people know at first glance what they are.

###Why do you think your API design is good (also define what your measure of good is)?
Good API design is one that is easy to understand and organized in such a way that can be easily extracted.
The APIs we have are good in terms of readability, but should be organized into interfaces.