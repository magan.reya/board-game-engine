import controller.GameController;
import controller.MainController;
import components.Grid;
import model.game.CheckersModel;
import model.game.Game;
import ooga.view.MainView;
import ooga.view.grid.GridView;
import view.game.CheckersGameView;
import view.game.GameView;

import java.awt.*;

public class SeeAllPossibleMoves {

    public static void main(String[]args) {
        Point point = new Point(0,0);
        List possibleLights = new List();
        Color color = Color.BLACK;
        //here, we instantiate all of the objects necessary to execute this use case
        MainView mainView = new MainView();
        MainController mainController = new MainController();
        GameController gameController = new GameController();
        Game model = new CheckersModel();
        GameView gameView = new CheckersGameView();

        //this will start the window display with a width and height determined
        mainController.start(10, 10);

        //the mainController has this method call since it has the MainView instance to set up
        mainView.setUpMain(10, 10);

        //The idea here is that the data will be parsed from the SIM file input through the SIM Parser, which will then
        //contain the data necessary for future constructions of everything
        gameController.loadFile();

        //When we create our new instance of a gameController, the ooga.view will be set up for our GameView in order to
        //allow for error handling if we have inputted the wrong data.
        gameController.setView(gameView);

        //this is the extension in the gameView class that will receive the call from the gameController.setView method
        //call in order to set up the initial gameView
        gameView.setUpGameView();

        //here, we create an instance of our model specific to the game type in the controller
        gameController.setModel(model);

        //this is the extension in the gameModel class that will receive the call from the gameController.setModel method
        //call in order to set up the initial gameModel
        model.setUpModel();

        //this is the data for our Grid from the model after it has been set up with the right data
        Grid grid = model.getGrid();

        //this will set up the Grid in the View after having initialized the GameView for error handling
        gameController.setViewGrid(grid);

        //this is the data for our GridView with the game specifications for the type of Grid we want to use when
        //interacting with each Piece in the Grid
        GridView gridView = new CheckersGridView();

        //calls the step function in the gameController to execute a step of the game when there is user input
        gameController.update();

        //this will update the model with the changes implemented by its call in the step function of the gameController
        model.update();

        //this will be called by model.update to update the pieceView that was selected by the User
        gameView.updatePieceView();

        //from the pieceView that was selected, since we are specifically looking at Checkers, we will then be able to
        //see the cells that should be lit up/valid after clicking that individual Piece
        //even with variations, the update method would initially start different for other implementations
        gridView.lightUpPossibleCells(point, possibleLights, color);
    }
}
