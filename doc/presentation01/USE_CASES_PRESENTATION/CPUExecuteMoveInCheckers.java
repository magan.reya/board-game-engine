import model.game.Game;
import view.game.CheckersGameView;
import view.game.GameView;
import view.grid.CheckersGridView;
import view.grid.GridView;

public class CPUExecuteMoveInCheckers {

    public static void main (String[] agrs) {

        // simplify the frontend code to focus on how CPU execute smartMove()

        Game model = new CheckersModel();
        GameView gameView = new CheckersGameView();

        //this is the extension in the gameView class that will receive the call from the gameController.setView method
        //call in order to set up the initial gameView
        gameView.setUpGameView();

        //this is the extension in the gameModel class that will receive the call from the gameController.setModel method
        //call in order to set up the initial gameModel
        model.setUpModel();

        //this is the data for our GridView with the game specifications for the type of Grid we want to use when
        //interacting with each Piece in the Grid
        GridView gridView = new CheckersGridView();

        //this will update the model with the changes implemented by its call in the step function of the gameController
        model.update();

        //this will be called by model.update to update the pieceView that was selected by the User
        gameView.updatePieceView();

    }
}

class CheckersModel extends Game {

    private int myCurrPlayerID = 0;
    private int CPU = 1;

    public void update () {
        if (myCurrPlayerID == CPU) {
            smartMove();
        }
        else {  }
        myCurrPlayerID = 1 - myCurrPlayerID; // switch to the other player
    }

    protected void smartMove () {
        // implements an algorithm to upate the location of one piece on the board.
    }
}