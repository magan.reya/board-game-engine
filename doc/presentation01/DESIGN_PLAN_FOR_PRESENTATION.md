# OOGA Design Plan
### Team: 3
### Justin Jang, Steven Cheng, Nate Zelter, Norah Tan, Reya Magan

## Design Overview
For the backend: We will have an abstract model that is extended by each of the different game types (ConnectFourModel, OthelloModel, etc). Each of these models will implement the Rules interface in different ways (based on how the rules of the respective game works). We will have functionality for both human and CPU players, and depending on what player is playing, they will implement Model methods differently (Human player actions will be based on user inputs, while the CPU will always make the “smartest” decision). The game board will be a Grid object, and each individual Piece will be looked at to determine its state and position.

For the ooga.view: We will have an abstract game ooga.view class that is extended by each of the different game types (ConnectFourGameView, OthelloGameView, etc.). We will also have individual ooga.view classes to deal with the Grid as well as each Piece in the Grid. Finally, we will also have a MainView class for the starting game scene. All of the ooga.view elements will be created using our factory ViewElements class.
## CRC Cards
|GameModel| |
|---|---|
|public boolean isEmpty (int x, int y)         |GameController|
|public int getPieceNextState(int x, int y)         |Grid|
|public int getPieceCurrentState(int x, int y)         |Piece|
|public void setPositionToBeRemoved(int x, int y)         |Rules|
|public void setPositionToBePlaced(int x, int y)        |CheckersModel|
|public void update()         |ConnectFourModel|
|protected void addPiece()        |GomokuModel|
|protected void movePiece() |OthelloModel|
|protected void smartMove()||

|GameController| |
|---|---|
|public void update()         |GameModel|
|public GameModel getModel()         |GameView|
|public GameView getView()         |Piece|
|public void setModel(GameModel model)         |Rules|
|public void setView (GameView ooga.view)        |Parser|
|public void saveFile()         |Grid|
|public void loadFile()        ||

|MainController| |
|---|---|
|public Scene start(int width, int height)       |MainView|

|Parser| |
|---|---|
|void loadFile(PropertyChangeEvent evt)        |CSVParser, SIMParser|
|void saveFile(PropertyChangeEvent evt)      |Grid|
|        |Piece|

|Grid| |
|---|---|
|public Map<Point, Piece> getMyBoard()       |GameController|
| |Piece|
| |GridView|
| |PieceView|

|Piece| |
|---|---|
|public int getX()        |GameModel|
|public void setX(int myX)         |PieceView|
|public int getY()         |Piece|
|public void setY(int myY)         |Rules|
|public int getValue()        |Grid|
|public void setValue(int myValue)        ||

|Rules| |
|---|---|
|public Boolean isValidMove(Piece piece)       |Model|
|public int getGameStatus();     |Grid|
|        |Piece|

|GameView| |
|---|---|
|       |GameController|
|       |PieceView, GridView|
|        |Checkers,ConnectFour,Gomoku, Othello GameView|
| |ViewElements|

|GridView| |
|---|---|
|       |GameController|
|        |Checkers,ConnectFour,Gomoku, Othello GridView|

|MainView| |
|---|---|
|       |MainController|

|PieceView| |
|---|---|
|       |GameController|




## Design Details

Here is a graphical look at my design:

![image](oogadesignpresentation.jpg)
1. Each model should have a Grid construction
   The Grid will be abstract, and each game type will extend it (ConnectFourGrid, OthelloGrid, etc.)

2The grid will be changed by "clicking" on the ooga.view
- For all games except for Connect 4, a user can select a cell that it wants to add their piece to
- For Connect 4 then we would just choose a column to place a piece into
- We should have options where CPU vs human and human vs CPU are different in order to determine who starts first

## Design Considerations
* Deciding between whether a Cell class is necessary or not.
    * We were debating whether to use a Piece class for each individual piece within a game,
      or whether to keep track of the cells in which a piece exists.

#### Design Issue #1
Whether or not to have a Rules interface or abstract class


* Alernative #1

We thought at first to have an abstract rules class that would be extended for the rules of each of the different games. Within our game model classes, we would call the right rules class to make sure a proposed piece movement was within the scope of the rules, and also check to see if the game was over.

* Alernative #2

Our second idea was to have Rules be an interface instead, and it would have the isMoveValid() and isGameOver() methods to check if a move was valid or if the game had already been won. The interface would be implemented in each of the model classes, and we would just implement it differently for each game.

* Trade-offs

The first alternative seems quite redundant to us, we already have abstraction for our Model, and the abstraction for the rule seems almost forced. The second alternative allows us to implement rules different for each game, but have it be easier to understand and read. The second alternative may lead to more lines of code, but this can always be refactored. We think the second alternative is more reasonable and also readable.


#### Design Issue #2
Reflection to set up multiple Game Controllers versus reflection to call multiple Game Models.
* Alernative #1

Since we have multiple games within our application, we were debating whether to let each game have its own controller, such that once the game was started the respective game controller would be called. This controller would then deal with communication from the specific game ooga.view to the specific game model for this current game.

* Alernative #2

Our other idea was to have only one controller, but have it use reflection to call the correct game model class. This way, no matter what game was chosen the controller would be the same. It would simply read from the simulation file, and use reflection to generate the right model class based on the game name.
* Trade-offs

The first alternative may allow for more separation and readability, but it likely would lead to a lot of duplicated code. With this in mind, it makes more sense to just have one controller, and have it use reflection to call the relevant game model. Each of the games would likely use the controller in the same way, so there is no need to have that much abstraction for it.



## User Interface

![image](Display_Design_Presentation.jpeg)

## Team Responsibilities

* Team Member #1 Nate is on the backend.

* Team Member #2 Norah is on the backend.

* Team Member #3 Reya is on the backend initially, might change depending on needs later

* Team Member #4 Justin is on the frontend and working on controller

* Team Member #5 Steven is on the frontend and working on controller



#### Proposed Schedule
Finish all designs by Monday, Nov. 8

Complete all board games’ backend and one or two games’ frontend for multiple human players. Add error handling and testing. By Monday, Nov 15

Complete automated opponents (with “smart” strategies) for at least 2 games. Implement the rest of the views and add features for CPU. By Monday, Nov 22

Complete automated opponents in all games, incorporate high scores and save data/progress. Add more complexity (more rules, more interactions with the ooga.view, etc.). By Monday, Nov 29
